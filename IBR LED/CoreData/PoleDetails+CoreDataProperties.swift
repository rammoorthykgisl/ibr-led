//
//  PoleDetails+CoreDataProperties.swift
//  IBR LED
//
//  Created by BSSADM on 23/10/19.
//  Copyright © 2019 KGISL. All rights reserved.
//
//

import Foundation
import CoreData


extension PoleDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PoleDetails> {
        return NSFetchRequest<PoleDetails>(entityName: "PoleDetails")
    }

    @NSManaged public var account_no: String?
    @NSManaged public var city_id: Int32
    @NSManaged public var control_time_location: String?
    @NSManaged public var id: NSNumber?
    @NSManaged public var installed: Int16
    @NSManaged public var isNew: Bool
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var meter_no: String?
    @NSManaged public var name: String?
    @NSManaged public var new: Int16
    @NSManaged public var phase: Int16
    @NSManaged public var photo1: Data?
    @NSManaged public var photo2: Data?
    @NSManaged public var remarks: String?
    @NSManaged public var road_name: String?
    @NSManaged public var status: Int16
    @NSManaged public var supplier: String?
    @NSManaged public var time_stamp: Date?
    @NSManaged public var state_id: Int32

}
