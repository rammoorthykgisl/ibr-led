//
//  CoreDatabaseModel.swift
//  Board Vision
//
//  Created by Webykart Mac on 11/05/18.
//  Copyright © 2018 Webykart Inc. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
class CoreDatabaseModel: NSObject {
    
    class func getContext() -> NSManagedObjectContext
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    class func savePoleDetails(poleImage:UIImage,lampImage:UIImage,name:String,road_name:String,city_id:Int,latitude:Double,longitude:Double,remarks:String,installed:Int,status:Int,new:Int,control_time_location:String,meter_no:String,account_no:String,phase:Int? = nil,supplier:String,isNew:Bool,date:Date,recordID:NSNumber,state_id:Int,completionHandler: @escaping (_ success: Bool) -> Void) -> Bool{
        let context = getContext()
        
        let entity = NSEntityDescription.entity(forEntityName: "PoleDetails", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        let poleImageData = poleImage.jpegData(compressionQuality: 0)
        let lampImageData = lampImage.jpegData(compressionQuality: 0)
        manageObject.setValue(name, forKey: "name")
        manageObject.setValue(road_name, forKey: "road_name")
        manageObject.setValue(city_id, forKey: "city_id")
        manageObject.setValue(latitude, forKey: "latitude")
        manageObject.setValue(longitude, forKey: "longitude")
        manageObject.setValue(remarks, forKey: "remarks")
        manageObject.setValue(installed, forKey: "installed")
        manageObject.setValue(status, forKey: "status")
        manageObject.setValue(new, forKey: "new")
        manageObject.setValue(control_time_location, forKey: "control_time_location")
        manageObject.setValue(meter_no, forKey: "meter_no")
        manageObject.setValue(account_no, forKey: "account_no")
        manageObject.setValue(phase, forKey: "phase")
        manageObject.setValue(poleImageData, forKey: "photo1")
        manageObject.setValue(lampImageData, forKey: "photo2")
        manageObject.setValue(supplier, forKey: "supplier")
        manageObject.setValue(isNew, forKey: "isNew")
        manageObject.setValue(date, forKey: "time_stamp")
        manageObject.setValue(recordID, forKey: "id")
        manageObject.setValue(state_id, forKey: "state_id")
        do {
            try context.save()
            print("Saved")
            completionHandler(true)
            return true
        } catch {
            
            print("Failed saving")
            completionHandler(false)
            return false
        }
    }
    
    
    
    
    class func fetchPendingRecords()->[PoleDetails]{
        let context = getContext()
        var pendingRecords: [PoleDetails]? = nil
        
        do {
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
            request.returnsDistinctResults = true
            request.returnsObjectsAsFaults = false
            
            
            pendingRecords = try context.fetch(request) as? [PoleDetails]
            return pendingRecords!
        } catch {
            
            print("Failed Fetching")
            return pendingRecords!
        }
        
    }
    
    
    
    
    class func deletePendingRecord(name:String,roadName:String,control_time_location:String) -> Bool{
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
        //let recordPredicate = NSPredicate(format: "name == %@ AND road_name == %@", name,roadName)
        
        var predicate1 = NSPredicate()
        var predicate2 = NSPredicate()
        predicate1 = NSPredicate(format: "name == %@ AND road_name == %@", name,roadName)
        predicate2 = NSPredicate(format: "control_time_location == %@", control_time_location)
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
        request.predicate = andPredicate
        
        let delete = NSBatchDeleteRequest(fetchRequest:request)
        do{
            try self.getContext().execute(delete)
            print("Deleted")
            try self.getContext().save()
            return true
            
        }catch{
            print("Error")
            return false
        }
        
    }
        class func deleteAllData()  -> Bool
        {
    
            let managedContext =  self.getContext()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
            fetchRequest.returnsObjectsAsFaults = false
           
            do
            {
                let results = try managedContext.fetch(fetchRequest)
                for managedObject in results
                {
                    let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                    managedContext.delete(managedObjectData)
                    print("Deleted All Datas in CoreData..")
                }
                return true
            } catch let error as NSError {
                print(error)
                 return false
            }
        }
    
   
    class func deleteAllPendingRecords() -> Bool
    {
        let context = getContext()
        let delete = NSBatchDeleteRequest(fetchRequest:PoleDetails.fetchRequest())
        do
        {
            try context.execute(delete)
            try self.getContext().save()
            print("All Cleared")
            return true
        }
        catch
        {
            return false
        }
    }
    
    class func checkExistRecord(name:String,roadName:String) -> [PoleDetails]?{
        let context = getContext()
        let recordPredicate = NSPredicate(format: "name == %@ AND road_name == %@", name,roadName)
        var pendingRecords: [PoleDetails]? = nil
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
        request.returnsDistinctResults = true
        request.returnsObjectsAsFaults = false
        request.predicate = recordPredicate
        do{
            pendingRecords =  try context.fetch(request) as? [PoleDetails]
            
            return pendingRecords!
            
        }catch{
            print("Error")
            return nil
        }
        
    }
    class func checkExistPendingRecord(name:String,roadName:String,city_ID:Int,State_ID:Int) -> [PoleDetails]?{
           let context = getContext()
           let recordPredicate = NSPredicate(format: "name == %@ AND road_name == %@", name,roadName)
           var pendingRecords: [PoleDetails]? = nil
           let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
           request.returnsDistinctResults = true
           request.returnsObjectsAsFaults = false
           request.predicate = recordPredicate
        
        
                    var predicate1 = NSPredicate()
                    var predicate2 = NSPredicate()
                  
                    predicate1 =  NSPredicate(format: "name == %@ AND road_name == %@", name,roadName)
                    predicate2 = NSPredicate(format: "city_id == %i AND state_id == %i", city_ID,State_ID)
                  
                   let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
                   request.predicate = andPredicate
        
        
           do{
               pendingRecords =  try context.fetch(request) as? [PoleDetails]
               
               return pendingRecords!
               
           }catch{
               print("Error")
               return nil
           }
           
       }
    class func checkPoleExist(name:String) -> [PoleDetails]?{
        let context = getContext()
        let recordPredicate = NSPredicate(format: "name == %@", name)
        var pendingRecords: [PoleDetails]? = nil
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
        request.returnsDistinctResults = true
        request.returnsObjectsAsFaults = false
        request.predicate = recordPredicate
        do{
            pendingRecords =  try context.fetch(request) as? [PoleDetails]
            
            return pendingRecords!
            
        }catch{
            print("Error")
            return nil
        }
        
    }
    
    class func checkRoadExist(roadName:String) -> [PoleDetails]?{
           let context = getContext()
           let recordPredicate = NSPredicate(format: "road_name == %@", roadName)
           var pendingRecords: [PoleDetails]? = nil
           let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PoleDetails")
           request.returnsDistinctResults = true
           request.returnsObjectsAsFaults = false
           request.predicate = recordPredicate
           do{
               pendingRecords =  try context.fetch(request) as? [PoleDetails]
               
               return pendingRecords!
               
           }catch{
               print("Error")
               return nil
           }
           
       }
       
    //    class func fetchTenantIdForEvents()->[Events]{
    //        let context = getContext()
    //        var addToEvents: [Events]? = nil
    //
    //        do {
    //
    //            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EventDates")
    //            request.returnsDistinctResults = true
    //            request.returnsObjectsAsFaults = false
    //            request.propertiesToFetch = ["tenantId"]
    //
    //            addToEvents = try context.fetch(request) as? [Events]
    //            return addToEvents!
    //        } catch {
    //
    //            print("Failed Fetching")
    //            return addToEvents!
    //        }
    //
    //    }
    //
    //    class func fetchUserDetails() -> [UserEntity]?
    //    {
    //        let context = getContext()
    //        var addToCart: [UserEntity]? = nil
    //
    //        do {
    //
    //            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserEntity")
    //            request.returnsDistinctResults = true
    //            request.returnsObjectsAsFaults = false
    //
    //
    //            addToCart = try context.fetch(request) as? [UserEntity]
    //            return addToCart
    //        } catch {
    //
    //            print("Failed Fetching")
    //            return addToCart
    //        }
    //    }
    //
    //    class func fetchUserDetailsForLastModifiedDate() -> [UserEntity]?
    //    {
    //        let context = getContext()
    //        var addToCart: [UserEntity]? = nil
    //        let defaults = UserDefaults.standard
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        do {
    //
    //            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserEntity")
    //            let predicate = NSPredicate(format: "userId = %@ AND tenantId = %@", userID, tenantID)
    //            request.returnsDistinctResults = true
    //            request.returnsObjectsAsFaults = false
    //            request.predicate = predicate
    //
    //            addToCart = try context.fetch(request) as? [UserEntity]
    //            return addToCart
    //        } catch {
    //
    //            print("Failed Fetching")
    //            return addToCart
    //        }
    //    }
    //    class func getLastModifiedDate() -> String?
    //    {
    //        let context = getContext()
    //        var addToCart: [UserEntity]? = nil
    //        let defaults = UserDefaults.standard
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        do {
    //
    //            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserEntity")
    //            let predicate = NSPredicate(format: "userId = %@ AND tenantId = %@", userID, tenantID)
    //            request.returnsDistinctResults = true
    //            request.returnsObjectsAsFaults = false
    //            request.predicate = predicate
    //
    //            addToCart = try context.fetch(request) as? [UserEntity]
    //
    //            if (addToCart?.count)! > 0{
    //               return addToCart![0].lastModified
    //            }else{
    //              return "\(0)"
    //            }
    //
    //        } catch {
    //
    //            print("Failed Fetching")
    //            return ""
    //        }
    //    }
    //    class func updateLastModifiedDateWith(date:String){
    //        let userDetails = self.fetchUserDetailsForLastModifiedDate()
    //        let context =  CoreDatabaseModel.getContext()
    //        if (userDetails?.count)! > 0{
    //            let eventsEntity = userDetails![0]
    //             eventsEntity.lastModified = date
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //    }
    //
    //    class func fetchObject() -> [EventDates]?
    //    {
    //        let context = getContext()
    //        var addToCart: [EventDates]? = nil
    //
    //        do {
    //
    //            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EventDates")
    //            request.returnsDistinctResults = true
    //            request.returnsObjectsAsFaults = false
    //            let defaults = UserDefaults.standard
    //            let userID = defaults.string(forKey: "user_id")!
    //            let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //            let predicate = NSPredicate(format: "tenantId = %@ AND user_id = %@", tenantID,userID)
    //            request.predicate = predicate
    //            let sdSortDate = NSSortDescriptor.init(key: "eventDate", ascending: true)
    //            request.sortDescriptors = [sdSortDate]
    //
    //            addToCart = try context.fetch(request) as? [EventDates]
    //            return addToCart
    //        } catch {
    //
    //            print("Failed Fetching")
    //            return addToCart
    //        }
    //    }
    //
    //    class func fetchRequest(entityName:String) -> NSFetchRequest<NSFetchRequestResult>{
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
    //
    //        return fetchRequest
    //    }
    //
    //    class func getManagedObject(name:String)->NSManagedObject{
    //
    //        let managedObject = NSEntityDescription.insertNewObject(forEntityName: name, into:self.getContext())
    //
    //        return managedObject
    //    }
    //
    //    class func fetchAllEvents() -> Array<Any>?{
    //
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let defaults = UserDefaults.standard
    //        let userID = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let predicate = NSPredicate(format: "tenantId = %@ AND user_id = %@", tenantID,userID)
    //        fetchRequest.predicate = predicate
    //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //    class func fetchAllToDoList() -> Array<Any>?{
    //
    //        let fetchRequest = self.fetchRequest(entityName: "ToDoList")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let defaults = UserDefaults.standard
    //        let userID = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let predicate = NSPredicate(format: "tenantId = %@ AND user_id = %@", tenantID,userID)
    //        fetchRequest.predicate = predicate
    //        //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //    class func fetchAllEventDates() -> Array<Any>?{
    //
    //        let fetchRequest = self.fetchRequest(entityName: "EventDates")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let predicate = NSPredicate(format: "tenantId = %@ AND user_id = %@", tenantID,userID)
    //        fetchRequest.predicate = predicate
    //        fetchRequest.propertiesToFetch = ["eventDate"]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //
    //
    //    class func fetchMeetingsOnly() -> Array<Any>?{
    //        let item = "meeting"
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        let predicate = NSPredicate(format: "item_type = %@ AND tenantId = %@ AND user_id = %@", item, tenantID,userID)
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = predicate
    //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //            for result in result{
    //                print(result)
    //            }
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //    class func fetchResolutionsOnly() -> Array<Any>?{
    //        let item = "resolution"
    //        let item1 = "no_details_resolution"
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        predicate1 = NSPredicate(format: "item_type == %@ OR item_type == %@ AND participant_count != %i ", item,item1,1)
    //        predicate2 = NSPredicate(format: "tenantId = %@ AND user_id = %@",tenantID,userID)
    //         let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = andPredicate
    //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //            for result in result{
    //                print(result)
    //            }
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func fetchResolutionsParticipantOnly() -> Array<Any>?{
    //        let item1 = "no_details_resolution"
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //         let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        //        predicate1 = NSPredicate(format: "item_type == %@ AND tenantId = %@", item, tenantID)
    //        predicate2 = NSPredicate(format: "item_type == %@ AND participant_count != %i AND tenantId = %@ AND user_id = %@", item1,1,tenantID,userID)
    //
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [ predicate2])
    //
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = andPredicate
    //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //            for result in result{
    //                print(result)
    //            }
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func fetchNoDetailResolutionsOnly() -> Array<Any>?{
    //        let item = "no_details_resolution"
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        let predicate = NSPredicate(format: "item_type == %@ AND participant_count == %i AND tenantId = %@ AND user_id = %@", item,1,tenantID,userID)
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = predicate
    //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //            for result in result{
    //                print(result)
    //            }
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //    class func fetchVotesOnly() -> Array<Any>?{
    //        let item = "vote"
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        let predicate = NSPredicate(format: "item_type == %@ AND tenantId = %@ AND user_id = %@", item,tenantID,userID)
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = predicate
    //        let sdSortDate = NSSortDescriptor.init(key: "query_date", ascending: true)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //            for result in result{
    //                print(result)
    //            }
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //
    //
    //    class  func createEventsEntityFrom(dictionary: [String: AnyObject],tenantId:String) -> NSManagedObject? {
    //        let context =  CoreDatabaseModel.getContext()
    //        let userID = UserDefaults.standard.string(forKey: "user_id")!
    //        if let eventsEntity = NSEntityDescription.insertNewObject(forEntityName: "Events", into: context) as? Events  {
    //            eventsEntity.tenantId = tenantId
    //            eventsEntity.user_id = userID
    //            if let id = (dictionary["id"] as? Int64){
    //                eventsEntity.id = id
    //            }
    //            if let title = dictionary["title"] as? String{
    //                eventsEntity.title = title
    //            }
    //            if let ac = (dictionary["action_required"] as? Int64){
    //                eventsEntity.action_required = ac
    //            }
    //            if let aa = (dictionary["allow_abstain"] as? Int64){
    //                eventsEntity.allow_abstain = aa
    //            }
    //
    //            if let cr = (dictionary["closed_resolution"] as? Int64){
    //                eventsEntity.closed_resolution = cr
    //            }
    //
    //            if let cr = (dictionary["status_id"] as? Int64){
    //                eventsEntity.status_id = cr
    //            }
    //
    //            if let et = (dictionary["end_time"] as? NSNumber){
    //                eventsEntity.end_time = et
    //            }
    //
    //
    //            if let st = (dictionary["start_time"] as? NSNumber){
    //                eventsEntity.start_time = st
    //            }
    //            else
    //            {
    //                eventsEntity.start_time = 0
    //            }
    //
    //            if let fId = (dictionary["file_id"] as? String){
    //                eventsEntity.file_id =  fId
    //            }
    //            if let itemType = dictionary["item_type"] as? String{
    //                eventsEntity.item_type = itemType
    //            }
    //            if let oId = (dictionary["option_id"] as? Int64){
    //                eventsEntity.option_id = oId
    //            }
    //            if let pc = (dictionary["participant_count"] as? Int64){
    //                eventsEntity.participant_count = pc
    //            }
    //            if let pId = (dictionary["participant_id"] as? Int64){
    //                eventsEntity.participant_id = pId
    //            }
    //
    //            if let mId = (dictionary["modified_at"] as? Int64){
    //                eventsEntity.modified_at = mId
    //            }
    //            if let st = dictionary["sub_title"] as? String{
    //                eventsEntity.sub_title =   st
    //            }
    //            if let qD = dictionary["query_date"] as? String{
    //                eventsEntity.query_date =  qD
    //            }
    //            if let status = dictionary["status"] as? String{
    //                eventsEntity.status =  status
    //            }
    //            if let date = dictionary["date"] as? String{
    //                eventsEntity.date =  date
    //            }
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //            return eventsEntity
    //        }
    //        return nil
    //    }
    //
    //    class  func saveEventsInCoreDataWith(array: [[String: AnyObject]],tenantId:String) {
    //
    //        _ = array.map{self.createEventsEntityFrom(dictionary: $0, tenantId: tenantId)}
    //
    //    }
    //
    //    class func fetchEventsWith(id:Int,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //         let userID = UserDefaults.standard.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        let predicate = NSPredicate(format: "item_type = %@ AND tenantId = %@ ", itemType, tenantID)
    //        let predicate_three = NSPredicate(format: "user_id = %@ AND id = %i", userID,id)
    //
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate,predicate_three])
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = andPredicate
    //
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest)
    //            for event in result{
    //
    //            }
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //
    //    }
    //
    //    class func updateEventsWith(id:Int,itemType:String,dictionary: [String: AnyObject]){
    //        let event = self.fetchEventsWith(id: id, itemType: itemType) as! [Events]
    //        let context =  CoreDatabaseModel.getContext()
    //        let eventsEntity = event[0]
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        eventsEntity.tenantId = tenantID
    //
    //        if let id = (dictionary["id"] as? Int64){
    //            eventsEntity.id = id
    //        }
    //        if let title = dictionary["title"] as? String{
    //            eventsEntity.title = title
    //        }
    //        if let ac = (dictionary["action_required"] as? Int64){
    //            eventsEntity.action_required = ac
    //        }
    //        if let aa = (dictionary["allow_abstain"] as? Int64){
    //            eventsEntity.allow_abstain = aa
    //        }
    //
    //        if let cr = (dictionary["closed_resolution"] as? Int64){
    //            eventsEntity.closed_resolution = cr
    //        }
    //
    //        if let cr = (dictionary["status_id"] as? Int64){
    //            eventsEntity.status_id = cr
    //        }
    //
    //        if let et = (dictionary["end_time"] as? NSNumber){
    //            eventsEntity.end_time = et
    //        }
    //
    //
    //        if let st = (dictionary["start_time"] as? NSNumber){
    //            eventsEntity.start_time = st
    //        }
    //        else
    //        {
    //            eventsEntity.start_time = 0
    //        }
    //        if let fId = (dictionary["file_id"] as? String){
    //            eventsEntity.file_id =  fId
    //        }
    //        if let itemType = dictionary["item_type"] as? String{
    //            eventsEntity.item_type = itemType
    //        }
    //        if let oId = (dictionary["option_id"] as? Int64){
    //            eventsEntity.option_id = oId
    //        }
    //        if let pc = (dictionary["participant_count"] as? Int64){
    //            eventsEntity.participant_count = pc
    //        }
    //        if let pId = (dictionary["participant_id"] as? Int64){
    //            eventsEntity.participant_id = pId
    //        }
    //
    //        if let mId = (dictionary["modified_at"] as? Int64){
    //            eventsEntity.modified_at = mId
    //        }
    //        if let st = dictionary["sub_title"] as? String{
    //            eventsEntity.sub_title =   st
    //        }
    //        if let qD = dictionary["query_date"] as? String{
    //            eventsEntity.query_date =  qD
    //        }
    //        if let status = dictionary["status"] as? String{
    //            eventsEntity.status =  status
    //        }
    //        if let date = dictionary["date"] as? String{
    //            eventsEntity.date =  date
    //        }
    //        do{
    //            try context.save()
    //        }catch(let error){
    //            print(error)
    //        }
    //    }
    //    class  func createToDoListFrom(dictionary: [String: AnyObject],tenantId:String,date:NSNumber) -> NSManagedObject? {
    //        let context =  CoreDatabaseModel.getContext()
    //        let userID = UserDefaults.standard.string(forKey: "user_id")!
    //        if let toDoListEntity = NSEntityDescription.insertNewObject(forEntityName: "ToDoList", into: context) as? ToDoList  {
    //            toDoListEntity.tenantId = tenantId
    //            toDoListEntity.current_dt = date
    //            toDoListEntity.user_id = userID
    //            if let id = (dictionary["id"] as? Int64){
    //                toDoListEntity.id = id
    //            }
    //            if let title = dictionary["title"] as? String{
    //                toDoListEntity.title = title
    //            }
    //            if let ac = (dictionary["action_required"] as? Int64){
    //                toDoListEntity.action_required = ac
    //            }
    //            if let aa = (dictionary["allow_abstain"] as? Int64){
    //                toDoListEntity.allow_abstain = aa
    //            }
    //
    //            if let cr = (dictionary["closed_resolution"] as? Int64){
    //                toDoListEntity.closed_resolution = cr
    //            }
    //
    //            if let et = (dictionary["end_time"] as? NSNumber){
    //                toDoListEntity.end_time = et
    //            }
    //
    //
    //            if let st = (dictionary["start_time"] as? NSNumber){
    //                toDoListEntity.start_time = st
    //            }
    //            else
    //            {
    //                toDoListEntity.start_time = 0
    //            }
    //            if let fId = (dictionary["file_id"] as? String){
    //                toDoListEntity.file_id =  fId
    //            }
    //            if let itemType = dictionary["item_type"] as? String{
    //                toDoListEntity.item_type = itemType
    //            }
    //            if let oId = (dictionary["option_id"] as? Int64){
    //                toDoListEntity.option_id = oId
    //            }
    //            if let pc = (dictionary["participant_count"] as? Int64){
    //                toDoListEntity.participant_count = pc
    //            }
    //            if let pId = (dictionary["participant_id"] as? Int64){
    //                toDoListEntity.participant_id = pId
    //            }
    //            if let mId = (dictionary["modified_at"] as? Int64){
    //                toDoListEntity.modified_at = mId
    //            }
    //            if let st = dictionary["sub_title"] as? String{
    //                toDoListEntity.sub_title =   st
    //            }
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //            return toDoListEntity
    //        }
    //        return nil
    //    }
    //
    //    class  func saveToDoListInCoreDataWith(array: [[String: AnyObject]],tenantId:String,date:NSNumber) {
    //
    //        _ = array.map{self.createToDoListFrom(dictionary: $0, tenantId: tenantId,date:date)}
    //
    //    }
    //    class func deleteObject(addToCart:EventDates) -> Bool
    //    {
    //        let context = getContext()
    //        context.delete(addToCart)
    //
    //        do
    //        {
    //            try context.save()
    //            return true
    //        }
    //        catch
    //        {
    //            return false
    //        }
    //    }
    //
    //    class func cleanObject() -> Bool
    //    {
    //        let context = getContext()
    //        let delete = NSBatchDeleteRequest(fetchRequest:EventDates.fetchRequest())
    //        do
    //        {
    //            try context.execute(delete)
    //            return true
    //        }
    //        catch
    //        {
    //            return false
    //        }
    //    }
    //    class func cleanEventsObject() -> Bool
    //    {
    //        let context = getContext()
    //        let delete = NSBatchDeleteRequest(fetchRequest:Events.fetchRequest())
    //        do
    //        {
    //            try context.execute(delete)
    //            return true
    //        }
    //        catch
    //        {
    //            return false
    //        }
    //    }
    
    //    class func deleteFutures(){
    //        let date = BoardVisionConstants.getCurrentDate()
    //        let fetchRequest = self.fetchRequest(entityName: "Events")
    //        let datePredicate = NSPredicate(format: "query_date >= %@", date)
    //        fetchRequest.predicate = datePredicate
    //        do{
    //            // try self.getContext().execute(delete)
    //            let result = try self.getContext().fetch(fetchRequest) as! [Events]
    //
    //            for event in result{
    //                self.getContext().delete(event)
    //            }
    //            try self.getContext().save()
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //    class func deleteAllData(entity: String)
    //    {
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //
    //        let managedContext =  self.getContext()
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let predicate = NSPredicate(format: "tenantId = %@",tenantID)
    //        fetchRequest.predicate = predicate
    //        do
    //        {
    //            let results = try managedContext.fetch(fetchRequest)
    //            for managedObject in results
    //            {
    //                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
    //                managedContext.delete(managedObjectData)
    //                print("Deleted All Datas in CoreData..")
    //            }
    //        } catch let error as NSError {
    //            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
    //        }
    //    }
    //    class func deleteEventDatesObject(eventDates:EventDates)
    //    {
    //        let context = getContext()
    //        context.delete(eventDates)
    //
    //        do
    //        {
    //            try context.save()
    //            print("EventDate Object Deleted")
    //
    //        }
    //        catch
    //        {
    //            print("EventDate Object Not Deleted")
    //        }
    //    }
    //    class func deleteEventsObject(events:Events)
    //    {
    //        let context = getContext()
    //        context.delete(events)
    //
    //        do
    //        {
    //            try context.save()
    //            print("Events Object Deleted")
    //
    //        }
    //        catch
    //        {
    //            print("Events Object Not Deleted")
    //        }
    //    }
    //    class func deleteToDoListObject(lists:ToDoList)
    //    {
    //        let context = getContext()
    //        context.delete(lists)
    //
    //        do
    //        {
    //            try context.save()
    //            print("ToDoList Object Deleted")
    //
    //        }
    //        catch
    //        {
    //            print("ToDoList Object Not Deleted")
    //        }
    //    }
    //    // DOCUMENTS
    //
    //    class func downloadPDF(url:String,pdfName:String,completion: @escaping (Bool,URL) -> ()){
    //
    //        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
    //            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    //            documentsURL.appendPathComponent(pdfName)
    //            return (documentsURL, [.removePreviousFile])
    //        }
    //        let countBytes = ByteCountFormatter()
    //        countBytes.allowedUnits = [.useMB]
    //        countBytes.countStyle = .file
    //
    //        Alamofire.download(url, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { progress in
    //
    //            let fileSize = countBytes.string(fromByteCount: progress.totalUnitCount)
    //            let completedSize = countBytes.string(fromByteCount: progress.completedUnitCount)
    //            let userInfo = [ "totalSize" : fileSize , "completedSize" : completedSize ]
    //            NotificationCenter.default.post(name: Notification.Name(rawValue: "DownloadProgressNotification"), object: nil, userInfo: userInfo)
    //            }
    //            .responseData { response in
    //
    //            if let destinationUrl = response.destinationURL {
    //                print("destinationUrl \(destinationUrl.absoluteURL)")
    //                completion(true,destinationUrl)
    //            }
    //
    //        }
    //    }
    //
    //
    //    class  func createDocumentsEntityFrom(id:Int64,userid:String,startTime:NSNumber,name:String,document_type:String,isDownloaded:Bool,end_dt:NSNumber,tenantId:String,file: [String: AnyObject],type:String,pdfName:String,currentDate:String)  {
    //        let context =  CoreDatabaseModel.getContext()
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "Documents", into: context) as? Documents
    //        {
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.id = id
    //            documentsEntity.user_id = userid
    //            documentsEntity.start_time = startTime
    //            documentsEntity.name = name
    //            documentsEntity.end_dt = end_dt
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //            if type == "meeting"
    //            {
    //                let meetingFile = MeetingFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    meetingFile.name = name
    //                }
    //                if let file_type = (file["file_type"]!["description"] as? String){
    //                    meetingFile.file_type = file_type
    //                }
    //                if let description = (file["description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                    meetingFile.file_description = ""
    //                }
    //                if let type = (file["type"] as? String){
    //                    meetingFile.type = type
    //                }
    //                else
    //                {
    //                    meetingFile.type = "meeting"
    //                }
    //                if let id = (file["id"] as? String){
    //                    meetingFile.file_id = id
    //                }
    //                if let id = (file["email_self"] as? Bool){
    //                    meetingFile.email_self = id
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    meetingFile.created_at = created_at
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    meetingFile.read = id
    //                }else{
    //                    meetingFile.read = false
    //                }
    //
    //                meetingFile.pspdf_file_id = ""
    //                meetingFile.modified_at = currentDate
    //                meetingFile.id = id
    //                meetingFile.tenantId = tenantId
    //                meetingFile.pdfName = pdfName
    //                meetingFile.isDownloaded = isDownloaded
    //                meetingFile.user_id = userid
    //                documentsEntity.meeting = NSSet(object: meetingFile)
    //            }
    //            else if type == "agenda"
    //            {
    //                let agentaFile = AgentaFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    agentaFile.name = name
    //                }
    //                if let description = (file["description"] as? String)
    //                {
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    agentaFile.file_description = ""
    //                }
    //                if let type = (file["type"] as? String){
    //                    agentaFile.type = type
    //                }
    //                else
    //                {
    //                    agentaFile.type = "agenda"
    //                }
    //                if let id = (file["email_self"] as? Bool){
    //                    agentaFile.email_self = id
    //                }
    //                agentaFile.modified_at = currentDate
    //                agentaFile.id = id
    //                if let id = (file["id"] as? String){
    //                    agentaFile.file_id = id
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    agentaFile.created_at = created_at
    //                }
    //                if let agenda_id = (file["agenda_id"] as? Int64){
    //                    agentaFile.agenda_id = agenda_id
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    agentaFile.read = id
    //                }else{
    //                    agentaFile.read = false
    //                }
    //
    //                agentaFile.pspdf_file_id = ""
    //                agentaFile.pdfName = pdfName
    //                agentaFile.isDownloaded = isDownloaded
    //                agentaFile.tenantId = tenantId
    //                agentaFile.user_id = userid
    //                documentsEntity.agenta = NSSet(object: agentaFile)
    //            }
    //            else {
    //                let resolutionFile = ResolutionFile(context: context)
    //                resolutionFile.type = type
    //                if let name = (file["name"] as? String){
    //                    resolutionFile.name = name
    //                }
    //                if let description = (file["description"] as? String){
    //                    resolutionFile.file_description = description
    //                }
    //                else
    //                {
    //                    resolutionFile.file_description = ""
    //                }
    //                if let id = (file["email_self"] as? Bool){
    //                    resolutionFile.email_self = id
    //                }
    //                resolutionFile.modified_at = currentDate
    //                resolutionFile.id = id
    //                if let id = (file["id"] as? String){
    //                    resolutionFile.file_id = id
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    resolutionFile.created_at = created_at
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    resolutionFile.read = id
    //                }else{
    //                    resolutionFile.read = false
    //                }
    //
    //                resolutionFile.pspdf_file_id = ""
    //                resolutionFile.pdfName = pdfName
    //                resolutionFile.isDownloaded = isDownloaded
    //                documentsEntity.resolution = NSSet(object: resolutionFile)
    //                resolutionFile.tenantId = tenantId
    //                resolutionFile.user_id = userid
    //            }
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //
    //    }
    //
    //    class func LocalDocumentFileInsert(id:Int64,userid:String,startTime:NSNumber,name:String,document_type:String,isDownloaded:Bool,end_dt:NSNumber,tenantId:String,file: [String: AnyObject],type:String,pdfName:String,currentDate:String)
    //    {
    //        let context =  CoreDatabaseModel.getContext()
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "LocalDocuments", into: context) as? LocalDocuments
    //        {
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.id = id
    //            documentsEntity.user_id = userid
    //            documentsEntity.start_time = startTime
    //            documentsEntity.name = name
    //            documentsEntity.end_dt = end_dt
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //
    //            let resolutionFile = ResolutionFile(context: context)
    //            resolutionFile.type = type
    //            if let name = (file["name"] as? String){
    //                resolutionFile.name = name
    //            }
    //            if let description = (file["description"] as? String){
    //                resolutionFile.file_description = description
    //            }
    //            else
    //            {
    //                resolutionFile.file_description = ""
    //            }
    //            if let id = (file["email_self"] as? Bool){
    //                resolutionFile.email_self = id
    //            }
    //            resolutionFile.modified_at = currentDate
    //            resolutionFile.id = id
    //            if let id = (file["id"] as? String){
    //                resolutionFile.file_id = id
    //            }
    //            if let created_at = (file["created_at"] as? NSNumber){
    //                resolutionFile.created_at = created_at
    //            }
    //
    //            if let id = (file["read"] as? Bool){
    //                resolutionFile.read = id
    //            }else{
    //                resolutionFile.read = false
    //            }
    //
    //            resolutionFile.pspdf_file_id = ""
    //            resolutionFile.pdfName = ""
    //            resolutionFile.isDownloaded = isDownloaded
    //            resolutionFile.tenantId = tenantId
    //            resolutionFile.user_id = userid
    ////            documentsEntity.addToLocalResolutionfile((NSSet(object: resolutionFile)))
    //            documentsEntity.localResolutionfile = NSSet(object: resolutionFile)
    //
    //
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //    }
    //
    //
    //
    //    class func DocumentFileInsertQuery(file:[String:AnyObject],id:Int64,type:String,isDownloaded:Bool,pdfName:String,isDashboard:Bool,agenda_id:Int64)
    //    {
    //        let context =  CoreDatabaseModel.getContext()
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId == %@ AND user_id = %@", id,tenantID,userID)
    //        fetchRequest.predicate = meetingPredicate
    //        do
    //        {
    //        var documents = try context.fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            if type == "meeting"
    //            {
    //                let meetingFile = LocalMeetingFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    meetingFile.name = name
    //                }
    //                if let description = (file["description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                    meetingFile.file_description = ""
    //                }
    //                if let description = (file["file_description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                    meetingFile.file_description = ""
    //                }
    //                if let file_type = (file["file_type"] as? String){
    //                    meetingFile.file_type = file_type
    //                }
    //                else
    //                {
    //                    meetingFile.file_type = type
    //                }
    //                if let type = (file["type"] as? String){
    //                    meetingFile.type = type
    //                }
    //                else
    //                {
    //                    meetingFile.type = "meeting"
    //                }
    //                if let file_id = (file["file_id"] as? String){
    //                    meetingFile.file_id = file_id
    //                }
    //                if isDashboard{
    //                if let id = (file["id"] as? String){
    //                    meetingFile.file_id = id
    //                }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    meetingFile.created_at = created_at
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    meetingFile.read = id
    //                }else{
    //                    meetingFile.read = false
    //                }
    //
    //                meetingFile.pspdf_file_id = ""
    //                meetingFile.id = id
    //                meetingFile.modified_at = ""
    //                meetingFile.pdfName = pdfName
    //                meetingFile.isDownloaded = isDownloaded
    //                meetingFile.tenantId = tenantID
    //                meetingFile.user_id = userID
    //
    //                if documents.count > 0{
    //                documents[0].addToLocalMeetingfile(NSSet(object: meetingFile))
    //                }
    //            }
    //            else if type == "agenda"
    //            {
    //                let agentaFile = LocalAgendaFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    agentaFile.name = name
    //                }
    //                if let description = (file["description"] as? String)
    //                {
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    agentaFile.file_description = ""
    //                }
    //                if let description = (file["file_description"] as? String){
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    agentaFile.file_description = ""
    //                }
    //                if let type = (file["type"] as? String){
    //                    agentaFile.type = type
    //                }
    //                else
    //                {
    //                    agentaFile.type = "agenda"
    //                }
    //
    //                if let file_id = (file["file_id"] as? String){
    //                    agentaFile.file_id = file_id
    //                }
    //                if isDashboard{
    //                if let id = (file["id"] as? String){
    //                        agentaFile.file_id = id
    //                    }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    agentaFile.created_at = created_at
    //                }
    //                if let agenda_id = (file["agenda_id"] as? Int64){
    //                    agentaFile.agenda_id = agenda_id
    //                }
    //                if isDashboard{
    //                    agentaFile.agenda_id = agenda_id
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    agentaFile.read = id
    //                }else{
    //                    agentaFile.read = false
    //                }
    //
    //                agentaFile.pspdf_file_id = ""
    //                agentaFile.id = id
    //                agentaFile.modified_at = ""
    //                agentaFile.pdfName = pdfName
    //                agentaFile.isDownloaded = isDownloaded
    //                agentaFile.tenantId = tenantID
    //                agentaFile.user_id = userID
    //                 if documents.count > 0{
    //                documents[0].addToLocalAgendafile(NSSet(object: agentaFile))
    //                }
    //
    //            }
    //            else
    //            {
    //                var resolutionFile:LocalResolutionFile!
    //                var observerFile:LocalObserverFile!
    //                if type == "observer"{
    //                   observerFile = LocalObserverFile(context: context)
    //                    if let name = (file["name"] as? String){
    //                        observerFile.name = name
    //                    }
    //                    if let description = (file["description"] as? String){
    //                        observerFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        observerFile.file_description = ""
    //                    }
    //
    //                    if let file_id = (file["file_id"] as? String){
    //                        observerFile.file_id = file_id
    //                    }else{
    //                        if let id = (file["id"] as? String){
    //                            observerFile.file_id = id
    //                        }
    //                    }
    //                    if isDashboard{
    //                        if let id = (file["id"] as? String){
    //                            observerFile.file_id = id
    //                        }
    //                    }
    //                    if let created_at = (file["created_at"] as? NSNumber){
    //                        observerFile.created_at = created_at
    //                    }
    //                    if let description = (file["file_description"] as? String){
    //                        observerFile.file_description = description
    //                    }else{
    //                        if let description = (file["description"] as? String){
    //                            observerFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            observerFile.file_description = ""
    //                        }
    //                    }
    //                    if let parent_id = (file["parent_id"] as? Int64){
    //                        observerFile.parent_id = parent_id
    //                    }else{
    //                       observerFile.parent_id = agenda_id
    //                    }
    //                    if let parent_type = (file["parent_type"] as? String){
    //                        observerFile.parent_type = parent_type
    //                    }else{
    //                     observerFile.parent_type = type
    //                    }
    //                    observerFile.id = id
    //                    if type == "meeting_resolution"{
    //                        observerFile.type = "resolution"
    //                    }else{
    //                        observerFile.type = type
    //                    }
    //
    //                    if let id = (file["read"] as? Bool){
    //                        observerFile.read = id
    //                    }else{
    //                        observerFile.read = false
    //                    }
    //
    //
    //                    observerFile.pspdf_file_id = ""
    //                    observerFile.modified_at = ""
    //                    observerFile.pdfName = pdfName
    //                    observerFile.isDownloaded = isDownloaded
    //                    observerFile.tenantId = tenantID
    //                    observerFile.user_id = userID
    //                }else{
    //                  resolutionFile = LocalResolutionFile(context: context)
    //                    if let name = (file["name"] as? String){
    //                        resolutionFile.name = name
    //                    }
    //                    if let description = (file["description"] as? String){
    //                        resolutionFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.file_description = ""
    //                    }
    //
    //                    if let file_id = (file["file_id"] as? String){
    //                        resolutionFile.file_id = file_id
    //                    }
    //                    if isDashboard{
    //                        if let id = (file["id"] as? String){
    //                            resolutionFile.file_id = id
    //                        }
    //                    }
    //                    if let created_at = (file["created_at"] as? NSNumber){
    //                        resolutionFile.created_at = created_at
    //                    }
    //                    if let description = (file["file_description"] as? String){
    //                        resolutionFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.file_description = ""
    //                    }
    //                    if let parent_id = (file["parent_id"] as? Int64){
    //                        resolutionFile.parent_id = parent_id
    //                    }
    //                    if let parent_type = (file["parent_type"] as? String){
    //                        resolutionFile.parent_type = parent_type
    //                    }
    //                    resolutionFile.id = id
    //                    if type == "meeting_resolution"{
    //                        resolutionFile.type = (file["type"] as? String)
    //
    //                    }else{
    //                        resolutionFile.type = type
    //                    }
    //
    //
    //                    if let description = (file["file_type_description"] as? String){
    //
    //                        resolutionFile.file_type_description = description
    //                    }
    //                    else
    //                    {
    //                        if let description = (file["file_type"]!["description"] as? String){
    //
    //                            resolutionFile.file_type_description = description
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_description = ""
    //                        }
    //                    }
    //
    //
    //                    if let file_type_id = (file["file_type_id"] as? Int64){
    //
    //                        resolutionFile.file_type_id = file_type_id
    //                    }
    //                    else
    //                    {
    //                        if let file_type_id = (file["file_type"]!["id"] as? Int64){
    //
    //                            resolutionFile.file_type_id = file_type_id
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_id = 0
    //                        }
    //                    }
    //
    //
    //                    if let id = (file["read"] as? Bool){
    //                        resolutionFile.read = id
    //                    }else{
    //                        resolutionFile.read = false
    //                    }
    //
    //                    resolutionFile.pspdf_file_id = ""
    //                    resolutionFile.modified_at = ""
    //                    resolutionFile.pdfName = pdfName
    //                    resolutionFile.isDownloaded = isDownloaded
    //                    resolutionFile.tenantId = tenantID
    //                    resolutionFile.user_id = userID
    //                }
    //
    //
    //                 if documents.count > 0{
    //                    if type == "observer"{
    //                    documents[0].addToLocalObserverfile(NSSet(object: observerFile))
    //                    }else{
    //                     documents[0].addToLocalResolutionfile(NSSet(object: resolutionFile))
    //                    }
    //                }
    //            }
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //        catch
    //        {
    //
    //        }
    //
    //    }
    //
    //    class func DocumentFileInsertQueryForOnline(file:[String:AnyObject],id:Int64,type:String,parent_type:String,isDownloaded:Bool,pdfName:String,isDashboard:Bool)
    //    {
    //        let context =  CoreDatabaseModel.getContext()
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId == %@ AND user_id = %@ ", id,tenantID,userID)
    //        fetchRequest.predicate = meetingPredicate
    //        do
    //        {
    //            var documents = try context.fetch(fetchRequest) as! [Documents]
    //
    //            if type == "meeting"
    //            {
    //                let meetingFile = MeetingFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    meetingFile.name = name
    //                }
    //                if let description = (file["description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                    meetingFile.file_description = ""
    //                }
    //                if let description = (file["file_description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                }
    //                if let file_type = (file["file_type"]!["description"] as? String){
    //                    meetingFile.file_type = file_type
    //                }
    //                if let type = (file["type"] as? String){
    //                    meetingFile.type = type
    //                }
    //                else
    //                {
    //                    meetingFile.type = "meeting"
    //                }
    //                if let file_id = (file["file_id"] as? String){
    //                    meetingFile.file_id = file_id
    //                }
    //                if isDashboard{
    //                    if let id = (file["id"] as? String){
    //                        meetingFile.file_id = id
    //                    }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    meetingFile.created_at = created_at
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    meetingFile.read = id
    //                }else{
    //                    meetingFile.read = false
    //                }
    //
    //                if let download = (file["download"] as? Bool){
    //                    meetingFile.download = download
    //                }
    //                else
    //                {
    //                    meetingFile.download = false
    //                }
    //
    //
    //                meetingFile.pspdf_file_id = ""
    //                meetingFile.id = id
    //                meetingFile.modified_at = ""
    //                meetingFile.pdfName = pdfName
    //                meetingFile.isDownloaded = isDownloaded
    //                meetingFile.tenantId = tenantID
    //                meetingFile.user_id = userID
    //                if documents.count > 0{
    //                    documents[0].addToMeeting(NSSet(object: meetingFile))
    //                }
    //            }
    //            else if type == "agenda"
    //            {
    //                let agentaFile = AgentaFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    agentaFile.name = name
    //                }
    //                if let description = (file["description"] as? String)
    //                {
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    agentaFile.file_description = ""
    //                }
    //                if let description = (file["file_description"] as? String){
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    agentaFile.file_description = ""
    //                }
    //                if let type = (file["type"] as? String){
    //                    agentaFile.type = type
    //                }
    //                else
    //                {
    //                    agentaFile.type = "agenda"
    //                }
    //
    //                if let file_id = (file["file_id"] as? String){
    //                    agentaFile.file_id = file_id
    //                }
    //                if isDashboard{
    //                    if let id = (file["id"] as? String){
    //                        agentaFile.file_id = id
    //                    }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    agentaFile.created_at = created_at
    //                }
    //                if let agenda_id = (file["agenda_id"] as? Int64){
    //                    agentaFile.agenda_id = agenda_id
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    agentaFile.read = id
    //                }else{
    //                    agentaFile.read = false
    //                }
    //
    //                if let download = (file["download"] as? Bool){
    //                    agentaFile.download = download
    //                }
    //                else
    //                {
    //                    agentaFile.download = false
    //                }
    //
    //
    //                agentaFile.pspdf_file_id = ""
    //                agentaFile.id = id
    //                agentaFile.modified_at = ""
    //                agentaFile.pdfName = pdfName
    //                agentaFile.isDownloaded = isDownloaded
    //                agentaFile.tenantId = tenantID
    //                agentaFile.user_id = userID
    //                if documents.count > 0{
    //                    documents[0].addToAgenta(NSSet(object: agentaFile))
    //                }
    //            }
    //            else if type == "meeting_resolution" || type == "resolution" || type == "approval" || type == "observer"
    //            {
    //                var resolutionFile:ResolutionFile!
    //                var observerFile:ObserverFile!
    //                if type == "observer"{
    //                observerFile = ObserverFile(context: context)
    //                    if let name = (file["name"] as? String){
    //                        observerFile.name = name
    //                    }
    //                    if let description = (file["description"] as? String){
    //                        observerFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        observerFile.file_description = ""
    //                    }
    //
    //                    if let file_id = (file["file_id"] as? String){
    //                        observerFile.file_id = file_id
    //                    }
    //                    if isDashboard{
    //                        if let id = (file["id"] as? String){
    //                            observerFile.file_id = id
    //                        }
    //                    }
    //                    if let created_at = (file["created_at"] as? NSNumber){
    //                        observerFile.created_at = created_at
    //                    }
    //                    if let description = (file["file_description"] as? String){
    //                        observerFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        observerFile.file_description = ""
    //                    }
    //
    //                    if let id = (file["read"] as? Bool){
    //                        observerFile.read = id
    //                    }else{
    //                        observerFile.read = false
    //                    }
    //
    //                    if let id = (file["resolution_id"] as? Int64){
    //                        observerFile.parent_id = id
    //                    }
    //                    else
    //                    {
    //                        observerFile.parent_id = id
    //                    }
    //
    //                    if let download = (file["download"] as? Bool){
    //                        observerFile.download = download
    //                    }
    //                    else
    //                    {
    //                        observerFile.download = false
    //                    }
    //
    //
    //                    observerFile.pspdf_file_id = ""
    //                    observerFile.id = id
    //                    observerFile.type = type
    //                    observerFile.parent_type = "meeting_resolution"
    //                    observerFile.modified_at = ""
    //                    observerFile.pdfName = pdfName
    //                    observerFile.isDownloaded = isDownloaded
    //                    observerFile.tenantId = tenantID
    //                    observerFile.user_id = userID
    //                    if documents.count > 0{
    //
    //                        documents[0].addToObserver(NSSet(object: observerFile))
    //
    //                    }
    //                }else{
    //                  resolutionFile = ResolutionFile(context: context)
    //                    if let name = (file["name"] as? String){
    //                        resolutionFile.name = name
    //                    }
    //                    if let description = (file["description"] as? String){
    //                        resolutionFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.file_description = ""
    //                    }
    //
    //                    if let file_id = (file["file_id"] as? String){
    //                        resolutionFile.file_id = file_id
    //                    }
    //                    if isDashboard{
    //                        if let id = (file["id"] as? String){
    //                            resolutionFile.file_id = id
    //                        }
    //                    }
    //                    if let created_at = (file["created_at"] as? NSNumber){
    //                        resolutionFile.created_at = created_at
    //                    }
    //                    if let description = (file["file_description"] as? String){
    //                        resolutionFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.file_description = ""
    //                    }
    //                    if let id = (file["read"] as? Bool){
    //                        resolutionFile.read = id
    //                    }else{
    //                        resolutionFile.read = false
    //                    }
    //
    //                    if let id = (file["resolution_id"] as? Int64){
    //                        resolutionFile.parent_id = id
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.parent_id = id
    //                    }
    //
    //                    if let type = (file["type"] as? String){
    //                        resolutionFile.type = type
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.type = type
    //                    }
    //
    //                    if let download = (file["download"] as? Bool){
    //                        resolutionFile.download = download
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.download = false
    //                    }
    //
    //                    resolutionFile.pspdf_file_id = ""
    //                    resolutionFile.id = id
    //                    resolutionFile.parent_type = parent_type
    //                    resolutionFile.modified_at = ""
    //                    resolutionFile.pdfName = pdfName
    //                    resolutionFile.isDownloaded = isDownloaded
    //                    resolutionFile.tenantId = tenantID
    //                    resolutionFile.user_id = userID
    //                    if documents.count > 0{
    //
    //                        documents[0].addToResolution(NSSet(object: resolutionFile))
    //
    //                    }
    //                }
    //            }
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //        catch
    //        {
    //
    //        }
    //
    //    }
    //
    //
    //    class func DocumentFileInsertAgentaForOnline(file:[String:AnyObject],id:Int64,agenda_id:Int64,type:String,isDownloaded:Bool,pdfName:String)
    //    {
    //        let context =  CoreDatabaseModel.getContext()
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId == %@ AND user_id = %@ ", id,tenantID,userID)
    //        fetchRequest.predicate = meetingPredicate
    //        do
    //        {
    //            var documents = try context.fetch(fetchRequest) as! [Documents]
    //
    //            if type == "agenda"
    //            {
    //                let agentaFile = AgentaFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    agentaFile.name = name
    //                }
    //                if let description = (file["description"] as? String)
    //                {
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    agentaFile.file_description = ""
    //                }
    //
    //                if let id = (file["id"] as? String){
    //                    agentaFile.file_id = id
    //                }
    //
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    agentaFile.created_at = created_at
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    agentaFile.read = id
    //                }else{
    //                    agentaFile.read = false
    //                }
    //
    //                agentaFile.type = "agenda"
    //                agentaFile.agenda_id = agenda_id
    //
    //                agentaFile.pspdf_file_id = ""
    //                agentaFile.id = id
    //                agentaFile.modified_at = ""
    //                agentaFile.pdfName = pdfName
    //                agentaFile.isDownloaded = isDownloaded
    //                agentaFile.tenantId = tenantID
    //                agentaFile.user_id = userID
    //                if documents.count > 0{
    //                    documents[0].addToAgenta(NSSet(object: agentaFile))
    //                }
    //            }
    //
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //        catch
    //        {
    //
    //        }
    //
    //    }
    //
    //
    //    class  func createDocumentsFrom(dictionary: [String: AnyObject],tenantId:String,document_type:String,isDownloaded:Bool) -> NSManagedObject? {
    //        let context =  CoreDatabaseModel.getContext()
    //        let userID: String  = UserDefaults.standard.string(forKey: "user_id")!
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "Documents", into: context) as? Documents  {
    //
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //            if let meeting = (dictionary["meeting"] as? [String:AnyObject]){
    //                if let id = (meeting["id"] as? Int64){
    //                    documentsEntity.id = id
    //                }
    //
    //                if let title = meeting["name"] as? String{
    //                    documentsEntity.name = title
    //                }
    //                if let ac = (meeting["start_time"] as? NSNumber){
    //                    documentsEntity.start_time = ac
    //                }
    //                else
    //                {
    //                    documentsEntity.start_time = 0
    //                }
    //                if let cr = (meeting["end_dt"] as? NSNumber){
    //                    documentsEntity.end_dt = cr
    //                }
    //                else
    //                {
    //                    documentsEntity.end_dt = 0
    //                }
    //                if let file = (meeting["meeting_file"] as? [[String:AnyObject]]){
    //
    //                    for doc in file
    //                    {
    //                         let meetingFile = MeetingFile(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            meetingFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String){
    //                            meetingFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            meetingFile.file_description = ""
    //                        }
    //                        if let file_type = (doc["file_type"]!["description"] as? String){
    //                            meetingFile.file_type = file_type
    //                        }
    //                        if let type = (doc["type"] as? String){
    //                            meetingFile.type = type
    //                        }
    //                        else
    //                        {
    //                            meetingFile.type = "meeting"
    //                        }
    //                        if let id = (doc["id"] as? String){
    //                            meetingFile.file_id = id
    //                        }
    //                        if let id = (doc["email_self"] as? Bool){
    //                            meetingFile.email_self = id
    //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            meetingFile.created_at = created_at
    //                        }
    //
    //                        meetingFile.modified_at = ""
    //                        if let id = (meeting["id"] as? Int64){
    //                            meetingFile.id = id
    //                        }
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            meetingFile.read = id
    //                        }else{
    //                            meetingFile.read = false
    //                        }
    //
    //                        if let sequence_no = (doc["agenda_sequence_no"] as? Int64){
    //                            meetingFile.sequence_no = sequence_no
    //                        }
    //
    //                        if let download = (doc["download"] as? Bool){
    //                            meetingFile.download = download
    //                        }
    //                        else
    //                        {
    //                            meetingFile.download = false
    //                        }
    //
    //                        meetingFile.pspdf_file_id = ""
    //
    //                        meetingFile.pdfName = ""
    //                        meetingFile.isDownloaded = isDownloaded
    //                        meetingFile.tenantId = tenantId
    //                        meetingFile.user_id = userID
    //                   documentsEntity.addMeetingFile(values: NSSet(object: meetingFile))
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //            }
    //
    //                if let file = (meeting["agenda_file"] as? [[String:AnyObject]]){
    //
    //
    //                    for doc in file{
    //                         let agentaFile = AgentaFile(context: context)
    //                    if let name = (doc["name"] as? String){
    //                        agentaFile.name = name
    //                    }
    //                    if let description = (doc["description"] as? String)
    //                    {
    //                        agentaFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        agentaFile.file_description = ""
    //                    }
    //                    if let type = (doc["type"] as? String){
    //                        agentaFile.type = type
    //                    }
    //                    else
    //                    {
    //                        agentaFile.type = "agenda"
    //                    }
    //                        if let id = (doc["email_self"] as? Bool){
    //                            agentaFile.email_self = id
    //                        }
    //                    agentaFile.modified_at = ""
    //                    if let id = (meeting["id"] as? Int64){
    //                        agentaFile.id = id
    //                    }
    //
    //                    if let id = (doc["id"] as? String){
    //                        agentaFile.file_id = id
    //                    }
    //                    if let created_at = (doc["created_at"] as? NSNumber){
    //                        agentaFile.created_at = created_at
    //                    }
    //                    if let agenda_id = (doc["agenda_id"] as? Int64){
    //                        agentaFile.agenda_id = agenda_id
    //                    }
    //
    //                        if let sequence_no = (doc["agenda_sequence_no"] as? Int64){
    //                            agentaFile.sequence_no = sequence_no
    //                        }
    //
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            agentaFile.read = id
    //                        }else{
    //                            agentaFile.read = false
    //                        }
    //
    //
    //                        if let download = (doc["download"] as? Bool){
    //                            agentaFile.download = download
    //                        }
    //                        else
    //                        {
    //                            agentaFile.download = false
    //                        }
    //
    //                        agentaFile.pspdf_file_id = ""
    //
    //                        agentaFile.pdfName = "'"
    //                        agentaFile.isDownloaded = isDownloaded
    //                        agentaFile.tenantId = tenantId
    //                        agentaFile.user_id = userID
    //                        documentsEntity.addAgendaFile(values: NSSet(object: agentaFile))
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //                    if let file = (meeting["resolution_file"] as? [[String:AnyObject]]){
    //                        var resolutionFile:ResolutionFile!
    //                        var observerFile:ObserverFile!
    //                        for doc in file{
    //                            if let type = (doc["type"] as? String){
    //                                if type == "observer"{
    //                                 observerFile = ObserverFile(context: context)
    //                                    if let name = (doc["name"] as? String){
    //                                        observerFile.name = name
    //                                    }
    //                                    if let description = (doc["description"] as? String)
    //                                    {
    //                                        observerFile.file_description = description
    //                                    }
    //                                    else
    //                                    {
    //                                        observerFile.file_description = ""
    //                                    }
    //
    //                                    if let type = (doc["type"] as? String){
    //                                        observerFile.type = type
    //                                         observerFile.parent_type = type
    //                                    }
    //
    //                                    if let id = (meeting["id"] as? Int64){
    //                                        observerFile.id = id
    //                                    }
    //
    //                                    observerFile.modified_at = ""
    //                                    if let id = (doc["resolution_id"] as? Int64){
    //                                        observerFile.parent_id = id
    //                                    }
    //                                    if let end_d = (doc["resolution_end_dt"] as? NSNumber){
    //                                        documentsEntity.end_dt = end_d
    //                                    }
    //                                    else
    //                                    {
    //                                        documentsEntity.end_dt = 0
    //                                    }
    //                                    if let id = (doc["email_self"] as? Bool){
    //                                        observerFile.email_self = id
    //                                    }
    //                                    if let id = (doc["id"] as? String){
    //                                        observerFile.file_id = id
    //                                    }
    //                                    if let created_at = (doc["created_at"] as? NSNumber){
    //                                        observerFile.created_at = created_at
    //                                    }
    //
    //                                    if let id = (doc["read"] as? Bool){
    //                                        observerFile.read = id
    //                                    }else{
    //                                        observerFile.read = false
    //                                    }
    //
    //                                    if let sequence_no = (doc["agenda_sequence_no"] as? Int64){
    //                                        observerFile.sequence_no = sequence_no
    //                                    }
    //
    //                                    if let download = (doc["download"] as? Bool){
    //                                        observerFile.download = download
    //                                    }
    //                                    else
    //                                    {
    //                                        observerFile.download = false
    //                                    }
    //
    //                                    observerFile.pspdf_file_id = ""
    //
    //                                    observerFile.pdfName = ""
    //                                    observerFile.isDownloaded = isDownloaded
    //                                    observerFile.tenantId = tenantId
    //                                    observerFile.user_id = userID
    //                                    documentsEntity.addObserverFile(values: NSSet(object: observerFile))
    //                                }else{
    //                                   resolutionFile = ResolutionFile(context: context)
    //                                    if let name = (doc["name"] as? String){
    //                                        resolutionFile.name = name
    //                                    }
    //                                    if let description = (doc["description"] as? String)
    //                                    {
    //                                        resolutionFile.file_description = description
    //                                    }
    //                                    else
    //                                    {
    //                                        resolutionFile.file_description = ""
    //                                    }
    //
    //                                    if let type = (doc["type"] as? String){
    //                                        resolutionFile.type = type
    //                                    }
    //                                    resolutionFile.parent_type = "meeting_resolution"
    //                                    if let id = (meeting["id"] as? Int64){
    //                                        resolutionFile.id = id
    //                                    }
    //
    //                                    resolutionFile.modified_at = ""
    //                                    if let id = (doc["resolution_id"] as? Int64){
    //                                        resolutionFile.parent_id = id
    //                                    }
    //                                    if let end_d = (doc["resolution_end_dt"] as? NSNumber){
    //                                        documentsEntity.end_dt = end_d
    //                                    }
    //                                    else
    //                                    {
    //                                        documentsEntity.end_dt = 0
    //                                    }
    //                                    if let id = (doc["email_self"] as? Bool){
    //                                        resolutionFile.email_self = id
    //                                    }
    //                                    if let id = (doc["id"] as? String){
    //                                        resolutionFile.file_id = id
    //                                    }
    //                                    if let created_at = (doc["created_at"] as? NSNumber){
    //                                        resolutionFile.created_at = created_at
    //                                    }
    //
    //                                    if let id = (doc["read"] as? Bool){
    //                                        resolutionFile.read = id
    //                                    }else{
    //                                        resolutionFile.read = false
    //                                    }
    //
    //
    //                                    if let description = (doc["file_type"]?["description"] as? String){
    //                                        resolutionFile.file_type_description = description
    //                                    }
    //                                    else
    //                                    {
    //                                        resolutionFile.file_type_description = ""
    //                                    }
    //
    //
    //                                    if let file_type_id = (doc["file_type"]?["id"] as? Int64){
    //                                        resolutionFile.file_type_id = file_type_id
    //                                    }
    //                                    else
    //                                    {
    //                                        resolutionFile.file_type_id = 0
    //                                    }
    //
    //                                    if let sequence_no = (doc["agenda_sequence_no"] as? Int64){
    //                                        resolutionFile.sequence_no = sequence_no
    //                                    }
    //
    //                                    if let download = (doc["download"] as? Bool){
    //                                        resolutionFile.download = download
    //                                    }
    //                                    else
    //                                    {
    //                                        resolutionFile.download = false
    //                                    }
    //
    //
    //                                    resolutionFile.pspdf_file_id = ""
    //
    //                                    resolutionFile.pdfName = ""
    //                                    resolutionFile.isDownloaded = isDownloaded
    //                                    resolutionFile.tenantId = tenantId
    //                                    resolutionFile.user_id = userID
    //                                    documentsEntity.addResolutionFile(values: NSSet(object: resolutionFile!))
    //                                }}
    //
    //
    //                            do{
    //                                try context.save()
    //                            }catch(let error){
    //                                print(error)
    //                            }
    //                        }
    //                    }
    //               }
    //            if let resolution = (dictionary["resolution"] as? [String:AnyObject]){
    //                if let id = (resolution["id"] as? Int64){
    //                    documentsEntity.id = id
    //                }
    //                if let title = resolution["name"] as? String{
    //                    documentsEntity.name = title
    //                }
    //                if let ac = (resolution["start_time"] as? NSNumber){
    //                    documentsEntity.start_time = ac
    //                }
    //                else
    //                {
    //                    documentsEntity.start_time = 0
    //                }
    //                if let cr = (resolution["end_dt"] as? NSNumber){
    //                    documentsEntity.end_dt = cr
    //                }
    //                else
    //                {
    //                    documentsEntity.end_dt = 0
    //                }
    //                if let file = (resolution["resolution_file"] as? [[String:AnyObject]]){
    //
    //                    for doc in file{
    //                        let resolutionFile = ResolutionFile(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            resolutionFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String)
    //                        {
    //                            resolutionFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_description = ""
    //                        }
    //
    //                        resolutionFile.type = document_type
    //                        resolutionFile.parent_type = document_type
    //
    //                        resolutionFile.modified_at = ""
    //                        if let id = (resolution["id"] as? Int64){
    //                            resolutionFile.id = id
    //                            resolutionFile.parent_id = id
    //                        }
    //
    //                        if let id = (doc["email_self"] as? Bool){
    //                            resolutionFile.email_self = id
    //                        }
    //                        if let id = (doc["id"] as? String){
    //                            resolutionFile.file_id = id
    //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            resolutionFile.created_at = created_at
    //                        }
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            resolutionFile.read = id
    //                        }else{
    //                            resolutionFile.read = false
    //                        }
    //
    //                        if let description = (doc["file_type"]?["description"] as? String){
    //
    //                            resolutionFile.file_type_description = description
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_description = ""
    //                        }
    //
    //
    //                        if let file_type_id = (doc["file_type"]?["id"] as? Int64){
    //
    //                            resolutionFile.file_type_id = file_type_id
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_id = 0
    //                        }
    //
    //                        if let download = (doc["download"] as? Bool){
    //                            resolutionFile.download = download
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.download = false
    //                        }
    //
    //                        resolutionFile.pspdf_file_id = ""
    //
    //                        resolutionFile.pdfName = ""
    //                        resolutionFile.isDownloaded = isDownloaded
    //                        resolutionFile.tenantId = tenantId
    //                        resolutionFile.user_id = userID
    //                        documentsEntity.addResolutionFile(values: NSSet(object: resolutionFile))
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //            }
    //            if let aa = (dictionary["user_id"] as? String){
    //                documentsEntity.user_id = aa
    //            }
    //
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //            return documentsEntity
    //        }
    //        return nil
    //    }
    //
    //    class  func saveDocumentsDataWith(array: [[String: AnyObject]],tenantId:String,document_type:String,isDownloaded:Bool) {
    //
    //        _ = array.map{self.createDocumentsFrom(dictionary: $0, tenantId: tenantId, document_type: document_type,isDownloaded:isDownloaded)}
    //
    //    }
    //    class  func createDirectoryEntityFrom(designation:String,first_name:String,id:String,image:String,last_name:String,middle_name:String,tenantId:String,role: [String: AnyObject],primary_email:String)  {
    //        let context =  CoreDatabaseModel.getContext()
    //        if let directoryEntity = NSEntityDescription.insertNewObject(forEntityName: "Directory", into: context) as? Directory
    //        {
    //            directoryEntity.tenantId = tenantId
    //            directoryEntity.id = id
    //            directoryEntity.designation = designation
    //            directoryEntity.first_name = first_name
    //            directoryEntity.image = image
    //            directoryEntity.last_name = last_name
    //            directoryEntity.middle_name = middle_name
    //            directoryEntity.primary_email = primary_email
    //
    //
    //
    //        do{
    //            try context.save()
    //        }catch(let error){
    //            print(error)
    //        }
    //    }
    //    }
    //    class func fetchAllDirectory() -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        //let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Directory")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let sdSortDate = NSSortDescriptor(key: "first_name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //
    //        let predicate = NSPredicate(format: "tenantId == %@ ",tenantID)
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Directory]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //    class func fetchAllDirectoryWithUserGroupFilter(id:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        //let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Directory")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let sdSortDate = NSSortDescriptor(key: "first_name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //
    //        let predicate = NSPredicate(format: "tenantId == %@ AND id = %@",tenantID,id)
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Directory]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchBusinessUserOnly() -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //
    //        let fetchRequest = self.fetchRequest(entityName: "Directory")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let sdSortDate = NSSortDescriptor(key: "first_name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        let predicate = NSPredicate(format: "ANY role.role_description = %@ AND tenantId == %@ ", "Business User",tenantID)
    //        fetchRequest.predicate = predicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Directory]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchAdministratorOnly() -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //
    //        let fetchRequest = self.fetchRequest(entityName: "Directory")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let sdSortDate = NSSortDescriptor(key: "first_name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //       let predicate = NSPredicate(format: "ANY role.role_description = %@ AND tenantId == %@ ", "Administrator",tenantID)
    //        fetchRequest.predicate = predicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Directory]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //    class func fetchFile(id:String,type:String,name:String) -> Int{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        var count = 0
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        var predicate3 = NSPredicate()
    //        var predicate4 = NSPredicate()
    //        if type == "meeting"{
    //            predicate1 = NSPredicate(format: "ANY meeting.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY meeting.file_id = %@ AND user_id = %@",id,userID)
    //            predicate3 = NSPredicate(format:"ANY meeting.isDownloaded == %@",NSNumber(value: true))
    //            predicate4 = NSPredicate(format:"ANY meeting.name = %@",name)
    //        }else if type == "agenda"{
    //            predicate1 = NSPredicate(format: "ANY agenta.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY agenta.file_id = %@ AND user_id = %@",id,userID)
    //             predicate3 = NSPredicate(format:"ANY agenta.isDownloaded == %@",NSNumber(value: true))
    //             predicate4 = NSPredicate(format:"ANY agenta.name = %@",name)
    //        }else{
    //            predicate1 = NSPredicate(format: "ANY resolution.type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY resolution.file_id = %@F AND user_id = %@",id,userID)
    //            predicate3 = NSPredicate(format:"ANY resolution.isDownloaded == %@ ",NSNumber(value: true))
    //             predicate4 = NSPredicate(format:"ANY resolution.name = %@",name)
    //        }
    //        let andPredicate1 = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [ predicate1,predicate2])
    //         let andPredicate2 = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [ predicate2,predicate3])
    //         let andPredicate3 = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [ andPredicate1,andPredicate2])
    //        fetchRequest.predicate = andPredicate2
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //            for data in result{
    //                for fileInfo in data.meeting?.allObjects as! [MeetingFile]{
    //                    if fileInfo.file_id == id && fileInfo.isDownloaded == true{
    //                        count = count + 1
    //                    }
    //                }
    //                for fileInfo in data.agenta?.allObjects as! [AgentaFile]{
    //                    if fileInfo.file_id == id && fileInfo.isDownloaded == true{
    //                        count = count + 1
    //                    }
    //                }
    //                for fileInfo in data.resolution?.allObjects as! [ResolutionFile]{
    //                    if fileInfo.file_id == id && fileInfo.isDownloaded == true{
    //                        count = count + 1
    //                    }
    //                }
    //            }
    //
    //            return count
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return 0
    //        }
    //    }
    //
    //
    //
    //    class func fetchMeetingsFilters(type:String) -> Array<Any>?{
    //
    //    let defaults = UserDefaults.standard
    //    let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //    let userID = defaults.string(forKey: "user_id")!
    //    let fetchRequest = self.fetchRequest(entityName: "Documents")
    //    fetchRequest.returnsDistinctResults = true
    //    fetchRequest.returnsObjectsAsFaults = false
    //    var predicate_one = NSPredicate()
    //    var predicate_two = NSPredicate()
    //    var predicate_three = NSPredicate()
    //
    //
    //    predicate_one = NSPredicate(format: "ANY meeting.isDownloaded = %@ AND isDownloaded = %@ AND tenantId = %@",  NSNumber(value: true),NSNumber(value: true), tenantID)
    //    predicate_two = NSPredicate(format: "ANY agenta.isDownloaded = %@ AND isDownloaded = %@ AND tenantId = %@",  NSNumber(value: true),NSNumber(value: true), tenantID)
    //    predicate_three = NSPredicate(format: "user_id = %@", userID)
    //
    //
    //    let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [predicate_one,predicate_two,predicate_three])
    //
    //    fetchRequest.predicate = andPredicate
    //    do {
    //    let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //
    //    return result
    //    }
    //    catch {
    //
    //    print("Failed")
    //    return nil
    //    }
    //    }
    //
    //
    //
    //    class func fetchDownloadedDocumentsFilters(type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //         let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        var predicate_one = NSPredicate()
    //        var predicate_two = NSPredicate()
    //        var predicate_three = NSPredicate()
    //            predicate_one = NSPredicate(format: "ANY resolution.isDownloaded = %@ AND isDownloaded = %@ ",  NSNumber(value: true),NSNumber(value: true))
    //            predicate_two =  NSPredicate(format: "ANY resolution.type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate_three = NSPredicate(format: "user_id = %@", userID)
    //
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate_one,predicate_two,predicate_three])
    //
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //
    //    class func fetchFilesOnly(id:Int64,type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let meetingPredicate:NSPredicate!
    //        let meetingTypePredicate:NSPredicate!
    //        let meetingResultPredicate:NSPredicate!
    //        let agendaPredicate:NSPredicate!
    //        let finalPredicate:NSPredicate!
    //        let meetingResolutionPredicate:NSPredicate!
    //        let meetingResolutionPredicate2:NSPredicate!
    //        let meetingResolutionTypePredicate:NSPredicate!
    //        let meetingResolutionFinalPredicate:NSPredicate!
    //        let resolutionPredicate:NSPredicate!
    //        if type == "meeting"{
    //            meetingPredicate = NSPredicate(format: "ANY meeting.id = %i AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //            meetingTypePredicate = NSPredicate(format: "ANY meeting.type = %@ AND document_type = %@ AND id = %i", type,type,id)
    //            meetingResultPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, meetingTypePredicate])
    //            agendaPredicate = NSPredicate(format: "ANY agenta.id = %i AND tenantId = %@ AND user_id = %@ AND id = %i", id,tenantID,userID,id)
    //            meetingResolutionPredicate = NSPredicate(format: "ANY resolution.parent_id = %i AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //            meetingResolutionPredicate2 = NSPredicate(format: "ANY resolution.id = %i AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //             meetingResolutionTypePredicate = NSPredicate(format: "ANY resolution.parent_type = %@ AND document_type = %@", "meeting_resolution",type)
    //            meetingResolutionFinalPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingResolutionPredicate, meetingResolutionTypePredicate])
    //             let observerPredicate = NSPredicate(format: "ANY observer.id = %i AND tenantId = %@ AND user_id = %@ ", id,tenantID,userID)
    //             finalPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [meetingResultPredicate,agendaPredicate,meetingResolutionFinalPredicate,observerPredicate,meetingResolutionPredicate2])
    //        }
    //        else
    //        {
    //             resolutionPredicate = NSPredicate(format: "ANY resolution.id = %i AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //             meetingResolutionPredicate = NSPredicate(format: "ANY resolution.parent_id = %i AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //             finalPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [resolutionPredicate, meetingResolutionPredicate])
    //        }
    //
    //
    //      fetchRequest.predicate = finalPredicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchDocuments(type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let meetingPredicate = NSPredicate(format: "document_type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //        let sdSortDate = NSSortDescriptor.init(key: "start_time", ascending: false)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        fetchRequest.predicate = meetingPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchDocumentsWithID(type:String,meetingID:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let meetingPredicate = NSPredicate(format: "id = %i AND document_type = %@ AND tenantId = %@ AND user_id = %@",meetingID, type,tenantID,userID)
    //        let sdSortDate = NSSortDescriptor.init(key: "start_time", ascending: false)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        fetchRequest.predicate = meetingPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchDocumentsWithFilters(type:String,isAssending:Bool) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let context =  self.getContext()
    //        context.stalenessInterval = 0.0
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let meetingPredicate = NSPredicate(format: "document_type = %@ AND tenantId = %@ ", type,tenantID)
    //        let predicate_three = NSPredicate(format: "user_id = %@", userID)
    //
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate,predicate_three])
    //        let sdSortDate:NSSortDescriptor!
    //        if type == "meeting"{
    //          sdSortDate  = NSSortDescriptor.init(key: "start_time", ascending: isAssending)
    //        }else{
    //          sdSortDate  = NSSortDescriptor.init(key: "end_dt", ascending: isAssending)
    //        }
    //
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try context.fetch(fetchRequest) as! [Documents]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func UpadteCurrentDate(pdfname:String,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        var predicate = NSPredicate()
    //        if itemType == "meeting"
    //        {
    //            predicate = NSPredicate(format: "ANY meeting.pdfName = %@ AND tenantId = %@",pdfname,tenantID)
    //        }
    //        else if itemType == "agenda"
    //        {
    //            predicate = NSPredicate(format: "ANY agenta.pdfName = %@ AND tenantId = %@",pdfname,tenantID)
    //        }
    //        else
    //        {
    //            predicate = NSPredicate(format: "ANY resolution.pdfName = %@ AND tenantId = %@",pdfname,tenantID)
    //        }
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func CheckFileExist(type:String,fileId:String,id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let type1 = "meeting"
    //        let type2 = "agenda"
    //        let type3 = "observer"
    //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        if type == type1{
    //            predicate1 = NSPredicate(format: "ANY meeting.type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //            predicate2 = NSPredicate(format:"ANY meeting.file_id = %@ AND id = %i ",fileId,id)
    //        }else if type == type2{
    //            predicate1 = NSPredicate(format: "ANY agenta.type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //            predicate2 = NSPredicate(format:"ANY agenta.file_id = %@ AND id = %i ",fileId,id)
    //        }else if type == type3{
    //            predicate1 = NSPredicate(format: "ANY observer.type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //            predicate2 = NSPredicate(format:"ANY observer.file_id = %@ ",fileId)
    //        }
    //        else{
    //            predicate1 = NSPredicate(format: "ANY resolution.type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //            predicate2 = NSPredicate(format:"ANY resolution.file_id = %@ AND id = %i ",fileId,id)
    //        }
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func fetchLocalDocuments() -> Array<Any>?{
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //    class  func createDocumentsForLocal(id:Int64,userid:String,startTime:NSNumber,name:String,document_type:String,isDownloaded:Bool,end_dt:NSNumber,tenantId:String,file: [String: AnyObject],type:String,pdfName:String,currentDate:String,isDashboard:Bool,localPdfName:String,agendaId:Int64)  {
    //        let context =  CoreDatabaseModel.getContext()
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "LocalDocuments", into: context) as? LocalDocuments
    //        {
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.id = id
    //            documentsEntity.user_id = userid
    //            documentsEntity.start_time = startTime
    //            documentsEntity.name = name
    //            documentsEntity.end_dt = end_dt
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //            if type == "meeting"
    //            {
    //                let meetingFile = LocalMeetingFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    meetingFile.name = name
    //                }
    //                if let description = (file["description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                    if let description = (file["file_description"] as? String){
    //                        meetingFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        meetingFile.file_description = ""
    //                    }
    //                }
    //                if let file_type = (file["file_type"] as? String){
    //                    meetingFile.file_type = file_type
    //                }
    //                if let type = (file["type"] as? String){
    //                    meetingFile.type = type
    //                }
    //                else
    //                {
    //                    meetingFile.type = "meeting"
    //                }
    //                if let id = (file["file_id"] as? String){
    //                    meetingFile.file_id = id
    //                }
    //                if isDashboard{
    //                if let id = (file["id"] as? String){
    //                    meetingFile.file_id = id
    //                }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    meetingFile.created_at = created_at
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    meetingFile.read = id
    //                }else{
    //                    meetingFile.read = false
    //                }
    //
    //                meetingFile.pspdf_file_id = ""
    //                meetingFile.modified_at = currentDate
    //                meetingFile.id = id
    //                meetingFile.tenantId = tenantId
    //                meetingFile.user_id = userid
    //                meetingFile.pdfName = pdfName
    //                meetingFile.isDownloaded = isDownloaded
    //                meetingFile.localPdfname = localPdfName
    //                documentsEntity.localMeetingfile = NSSet(object: meetingFile)
    //            }
    //            else if type == "agenda"
    //            {
    //                let agentaFile = LocalAgendaFile(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    agentaFile.name = name
    //                }
    //                if let description = (file["description"] as? String)
    //                {
    //                    agentaFile.file_description = description
    //                }
    //                else
    //                {
    //                    if let description = (file["file_description"] as? String){
    //                        agentaFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        agentaFile.file_description = ""
    //                    }
    //
    //                }
    //
    //                if let type = (file["type"] as? String){
    //                    agentaFile.type = type
    //                }
    //                else
    //                {
    //                    agentaFile.type = "agenda"
    //                }
    //
    //                agentaFile.modified_at = currentDate
    //                agentaFile.id = id
    //                if let id = (file["file_id"] as? String){
    //                    agentaFile.file_id = id
    //                }
    //                if isDashboard{
    //                if let id = (file["id"] as? String){
    //                    agentaFile.file_id = id
    //                }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    agentaFile.created_at = created_at
    //                }
    //                if let agenda_id = (file["agenda_id"] as? Int64){
    //                    agentaFile.agenda_id = agenda_id
    //                }
    //                if isDashboard{
    //                     agentaFile.agenda_id = agendaId
    //                }
    //
    //                if let id = (file["read"] as? Bool){
    //                    agentaFile.read = id
    //                }else{
    //                    agentaFile.read = false
    //                }
    //
    //                agentaFile.pspdf_file_id = ""
    //                agentaFile.pdfName = pdfName
    //                agentaFile.isDownloaded = isDownloaded
    //                agentaFile.tenantId = tenantId
    //                agentaFile.user_id = userid
    //                agentaFile.localPdfname = localPdfName
    //                documentsEntity.localAgendafile = NSSet(object: agentaFile)
    //            }
    //            else {
    //                var resolutionFile:LocalResolutionFile!
    //                var observerFile:LocalObserverFile!
    //                if type == "observer"{
    //                   observerFile = LocalObserverFile(context: context)
    //
    //                    if let name = (file["name"] as? String){
    //                        observerFile.name = name
    //                    }
    //                    if let description = (file["description"] as? String){
    //                        observerFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        observerFile.file_description = ""
    //                    }
    //                    if let description = (file["file_description"] as? String){
    //                        observerFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        observerFile.file_description = ""
    //                    }
    //                    if let parent_id = (file["parent_id"] as? Int64){
    //                        observerFile.parent_id = parent_id
    //                    }else{
    //                        observerFile.parent_id = agendaId
    //                    }
    //                    if let parent_type = (file["parent_type"] as? String){
    //
    //                        observerFile.parent_type = parent_type
    //                    }else{
    //                        observerFile.parent_type = type
    //                    }
    //
    //                    if let id = (file["read"] as? Bool){
    //                        observerFile.read = id
    //                    }else{
    //                        observerFile.read = false
    //                    }
    //
    //                    observerFile.modified_at = currentDate
    //                    observerFile.id = id
    //                    observerFile.type = type
    //                    if let id = (file["file_id"] as? String){
    //                        observerFile.file_id = id
    //                    }else{
    //                        if let id = (file["id"] as? String){
    //                            observerFile.file_id = id
    //                        }
    //                    }
    //                    if isDashboard{
    //                        if let id = (file["id"] as? String){
    //                            observerFile.file_id = id
    //                        }
    //                    }
    //                    if let created_at = (file["created_at"] as? NSNumber){
    //                        observerFile.created_at = created_at
    //                    }
    //
    //                    observerFile.pspdf_file_id = ""
    //                    observerFile.pdfName = pdfName
    //                    observerFile.user_id = userid
    //                    observerFile.isDownloaded = isDownloaded
    //                    observerFile.localPdfname = localPdfName
    //                    observerFile.tenantId = tenantId
    //                    documentsEntity.localObserverfile = NSSet(object: observerFile)
    //                }else{
    //                   resolutionFile =  LocalResolutionFile(context: context)
    //                    if type == "meeting_resolution"{
    //
    //                        if (file["type"] as? String) == "approval"
    //                        {
    //                            resolutionFile.type = "approval"
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.type = "resolution"
    //                        }
    //
    //                    }else{
    //                        resolutionFile.type = type
    //                    }
    //
    //                    if let name = (file["name"] as? String){
    //                        resolutionFile.name = name
    //                    }
    //                    if let description = (file["description"] as? String){
    //                        resolutionFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.file_description = ""
    //                    }
    //                    if let description = (file["file_description"] as? String){
    //                        resolutionFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        resolutionFile.file_description = ""
    //                    }
    //                    if let parent_id = (file["parent_id"] as? Int64){
    //                        resolutionFile.parent_id = parent_id
    //                    }else{
    //                        resolutionFile.parent_id = id
    //                    }
    //                    if let parent_type = (file["parent_type"] as? String){
    //
    //                        resolutionFile.parent_type = parent_type
    //                    }else{
    //                        resolutionFile.parent_type = type
    //                    }
    //                    resolutionFile.modified_at = currentDate
    //                    resolutionFile.id = id
    //                    if let id = (file["file_id"] as? String){
    //                        resolutionFile.file_id = id
    //                    }
    //                    if isDashboard{
    //                        if let id = (file["id"] as? String){
    //                            resolutionFile.file_id = id
    //                        }
    //                    }
    //                    if let created_at = (file["created_at"] as? NSNumber){
    //                        resolutionFile.created_at = created_at
    //                    }
    //
    //                    if let id = (file["read"] as? Bool){
    //                        resolutionFile.read = id
    //                    }else{
    //                        resolutionFile.read = false
    //                    }
    //
    //                    if let description = (file["file_type"]?["description"] as? String){
    //
    //                        resolutionFile.file_type_description = description
    //                    }
    //                    else
    //                    {
    //                        if let description = (file["file_type_description"] as? String){
    //                            resolutionFile.file_type_description = description
    //                        }
    //                        else{
    //                            resolutionFile.file_type_description = ""
    //                        }
    //                    }
    //
    //
    //                    if let file_type_id = (file["file_type"]?["id"] as? Int64){
    //
    //                        resolutionFile.file_type_id = file_type_id
    //                    }
    //                    else
    //                    {
    //                        if let file_type_id = (file["file_type_id"] as? Int64){
    //                            resolutionFile.file_type_id = file_type_id
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_id = 0
    //                        }
    //                    }
    //
    //
    //                    resolutionFile.pspdf_file_id = ""
    //                    resolutionFile.pdfName = pdfName
    //                    resolutionFile.user_id = userid
    //                    resolutionFile.isDownloaded = isDownloaded
    //                    resolutionFile.localPdfname = localPdfName
    //                    resolutionFile.tenantId = tenantId
    //                    documentsEntity.localResolutionfile = NSSet(object: resolutionFile)
    //                }
    //
    //            }
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //
    //    }
    //
    //    class func fetchLocalMeetingsFilters(type:String,isAssending:Bool) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let context = self.getContext()
    //        context.stalenessInterval = 0.0
    //        context.refreshAllObjects()
    //         let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        var predicate_one = NSPredicate()
    //        var predicate_two = NSPredicate()
    //        var predicate_three = NSPredicate()
    //         var orPredicate = NSPredicate()
    //
    //        if type == "meeting"{
    //            predicate_one = NSPredicate(format: "ANY document_type = %@ AND tenantId = %@",  type, tenantID)
    //            predicate_two = NSPredicate(format: "isDownloaded = %@ AND user_id = %@" ,  NSNumber(value: true),userID)
    //            let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate_one, predicate_two])
    //            let meetingResolution =  NSPredicate(format: "ANY localResolutionfile.parent_type = %@ AND tenantId = %@ ","meeting_resolution" ,tenantID)
    //            let meetingResolution2 =  NSPredicate(format: "ANY localResolutionfile.isDownloaded = %@ AND user_id = %@",NSNumber(value: true),userID)
    //
    //            let andPredicate2 = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingResolution, meetingResolution2])
    //             orPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [andPredicate, andPredicate2])
    //              fetchRequest.predicate = orPredicate
    //        }else{
    //            predicate_one = NSPredicate(format: "ANY document_type = %@ AND tenantId = %@",  type, tenantID)
    //            predicate_two = NSPredicate(format: "isDownloaded = %@ AND user_id = %@" ,  NSNumber(value: true),userID)
    //            let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate_one, predicate_two])
    //             fetchRequest.predicate = andPredicate
    //        }
    //
    //        let sdSortDate = NSSortDescriptor.init(key: "start_time", ascending: isAssending)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        do {
    //            let result = try context.fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalResolutionDocumentsFilters(type:String,isAssending:Bool) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let context = self.getContext()
    //        context.stalenessInterval = 0.0
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        var predicate_one = NSPredicate()
    //        var predicate_two = NSPredicate()
    //        var predicate_three = NSPredicate()
    //         predicate_one = NSPredicate(format: "ANY document_type = %@ AND tenantId = %@",  type, tenantID)
    //         predicate_two =  NSPredicate(format: "ANY localResolutionfile.parent_type = %@ AND localResolutionfile.isDownloaded = %@", type,NSNumber(value: true))
    //         predicate_three = NSPredicate(format: "isDownloaded = %@ AND user_id = %@" ,  NSNumber(value: true),  userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate_one,predicate_three])
    //        let sdSortDate = NSSortDescriptor.init(key: "end_dt", ascending: isAssending)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try context.fetch(fetchRequest) as! [LocalDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalFile(id:String,type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        //fetchRequest.fetchLimit = 1
    //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        var predicate3 = NSPredicate()
    //        if type == "meeting"{
    //            predicate1 = NSPredicate(format: "ANY localMeetingfile.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localMeetingfile.file_id = %@ ",id)
    //            predicate3 = NSPredicate(format:"ANY localMeetingfile.user_id = %@ AND user_id = %@",userID,userID)
    //        }else if type == "agenda"{
    //            predicate1 = NSPredicate(format: "ANY localAgendafile.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localAgendafile.file_id = %@ ",id)
    //             predicate3 = NSPredicate(format:"ANY localAgendafile.user_id = %@ AND user_id = %@",userID,userID)
    //        }else if type == "observer"{
    //            predicate1 = NSPredicate(format: "ANY localObserverfile.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localObserverfile.file_id = %@ ",id)
    //            predicate3 = NSPredicate(format:"ANY localObserverfile.user_id = %@ AND user_id = %@",userID,userID)
    //        }
    //        else if type == "meeting_resolution"{
    //            predicate1 = NSPredicate(format: "ANY localResolutionfile.parent_type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localResolutionfile.file_id = %@",id)
    //            predicate3 = NSPredicate(format:"ANY localResolutionfile.user_id = %@ AND user_id = %@",userID,userID)
    //        }else{
    //            predicate1 = NSPredicate(format: "ANY localResolutionfile.type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localResolutionfile.file_id = %@",id)
    //            predicate3 = NSPredicate(format:"ANY localResolutionfile.user_id = %@ AND user_id = %@",userID,userID)
    //        }
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2,predicate3])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalMeetingResolutionFile(id:Int64,type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    ////        fetchRequest.fetchLimit = 1
    //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        var predicate3 = NSPredicate()
    //
    //            predicate1 = NSPredicate(format: "ANY localResolutionfile.parent_id = %i AND tenantId = %@", id,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localResolutionfile.parent_type = %@",type)
    //            predicate3 = NSPredicate(format:"ANY localResolutionfile.user_id = %@ AND user_id = %@",userID,userID)
    //
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1,predicate2,predicate3])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalDocumentsWith(id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId = %@ ", id,tenantID)
    //        let userPredicate = NSPredicate(format: " user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalDocumentsForAllDelete(id:Int64,type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        var meetingPredicate:NSPredicate!
    //        let meetingResolution:NSPredicate!
    //        let userPredicate:NSPredicate!
    //        let andPredicate:NSPredicate!
    //        let orPredicate:NSPredicate!
    //        if type == "meeting" ||  type == "resolution" || type == "approval"{
    //            meetingPredicate = NSPredicate(format: " id = %i AND tenantId = %@ ", id,tenantID)
    //            userPredicate = NSPredicate(format: " user_id = %@ ",userID)
    //            andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //            meetingResolution = NSPredicate(format: "ANY localResolutionfile.parent_id = %i", id)
    //             let meetingResolutionType = NSPredicate(format: "ANY localResolutionfile.parent_type = %@","meeting_resolution")
    //            let meetingandPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingResolution, meetingResolutionType])
    //            orPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [andPredicate, meetingandPredicate])
    //
    //          fetchRequest.predicate = orPredicate
    //
    //        }else{
    //          meetingPredicate = NSPredicate(format: " id = %i AND tenantId = %@ ", id,tenantID)
    //          userPredicate = NSPredicate(format: " user_id = %@ ",userID)
    //          andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //          fetchRequest.predicate = andPredicate
    //        }
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func deleteLocalDocumentsWith(id:Int64,type:String){
    //        let defaults = UserDefaults.standard
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let predicate = NSPredicate(format: "id = %i AND document_type = %@", id,type)
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userPredicate = NSPredicate(format: " user_id = %@ AND tenantId = %@  ",userID,tenantID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate, userPredicate])
    //        fetchRequest.predicate = andPredicate
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            for document in result
    //            {
    //                    for file in document.localMeetingfile?.allObjects as! [LocalMeetingFile]{
    //                        if file.user_id == userID {
    //                            self.getContext().delete(file)
    //                        }
    //                    }
    //
    //                    for file in document.localAgendafile?.allObjects as! [LocalAgendaFile]{
    //
    //                        if file.user_id == userID {
    //                            self.getContext().delete(file)
    //                        }
    //                    }
    //                for file in document.localObserverfile?.allObjects as! [LocalObserverFile]{
    //
    //                    if file.user_id == userID {
    //                        self.getContext().delete(file)
    //                    }
    //                }
    //                for file in document.localResolutionfile?.allObjects as! [LocalResolutionFile]{
    //
    //                    if file.user_id == userID {
    //                        self.getContext().delete(file)
    //                    }
    //                }
    //
    //                self.getContext().delete(document)
    //                self.getContext().refreshAllObjects()
    //            }
    //
    //            try self.getContext().save()
    //             print("File Removed From DB...")
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //    class func deleteLocalMeetingResolutionDocumentsWith(id:Int64,type:String,resolutionId:Int64){
    //        let defaults = UserDefaults.standard
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let predicate = NSPredicate(format: "id = %i AND document_type = %@", id,type)
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userPredicate = NSPredicate(format: " user_id = %@ AND tenantId = %@  ",userID,tenantID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate, userPredicate])
    //        var fileCount = 0
    //        fetchRequest.predicate = andPredicate
    //
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            for document in result{
    //                if type == "meeting"{
    //                    for _ in document.localMeetingfile?.allObjects as! [LocalMeetingFile]{
    //                    fileCount = fileCount + 1
    //                    }
    //
    //                    for _ in document.localAgendafile?.allObjects as! [LocalAgendaFile]{
    //
    //                     fileCount = fileCount + 1
    //                }
    //                    for _ in document.localObserverfile?.allObjects as! [LocalObserverFile]{
    //
    //                     fileCount = fileCount + 1
    //                    }
    //                    for file in document.localResolutionfile?.allObjects as! [LocalResolutionFile]{
    //                       fileCount = fileCount + 1
    //                        if file.user_id == userID && file.parent_id == resolutionId{
    //                            self.getContext().delete(file)
    //                            fileCount = fileCount - 1
    //                        }
    //
    //                    }
    //                    if fileCount == 0{
    //                     self.getContext().delete(document)
    //                    }
    //
    //                }
    //                else{
    //                    for file in document.localResolutionfile?.allObjects as! [LocalResolutionFile]{
    //
    //                        if file.user_id == userID {
    //                            self.getContext().delete(file)
    //                        }
    //
    //                    }
    //                     self.getContext().delete(document)
    //                }
    //
    //
    //
    //                self.getContext().refreshAllObjects()
    //            }
    //
    //            try self.getContext().save()
    //            print("File Removed From DB...")
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //
    //
    //    class func updateLocalDocument(id:Int,type:String){
    //        let defaults = UserDefaults.standard
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let predicate = NSPredicate(format: "id = %i AND document_type = %@", id,type)
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userPredicate = NSPredicate(format: " user_id = %@ AND tenantId = %@  ",userID,tenantID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate, userPredicate])
    //        var fileCount = 0
    //        fetchRequest.predicate = andPredicate
    //
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            for document in result{
    //                if type == "meeting"{
    //                    for file in document.localMeetingfile?.allObjects as! [LocalMeetingFile]{
    //
    //                        if file.isDownloaded == true
    //                        {
    //                            fileCount = fileCount + 1
    //                        }
    //                    }
    //
    //                    for file in document.localAgendafile?.allObjects as! [LocalAgendaFile]{
    //
    //                        if file.isDownloaded == true
    //                        {
    //                            fileCount = fileCount + 1
    //                        }
    //                    }
    //                    for file in document.localObserverfile?.allObjects as! [LocalObserverFile]{
    //
    //                        if file.isDownloaded == true
    //                        {
    //                            fileCount = fileCount + 1
    //                        }
    //                    }
    //                    for file in document.localResolutionfile?.allObjects as! [LocalResolutionFile]{
    ////                        fileCount = fileCount + 1
    ////                        if file.user_id == userID && file.parent_id == resolutionId{
    ////                            self.getContext().delete(file)
    ////                            fileCount = fileCount - 1
    ////                        }
    //                        if file.isDownloaded == true
    //                        {
    //                            fileCount = fileCount + 1
    //                        }
    //                    }
    //
    //                    if fileCount == 0{
    ////                        self.getContext().delete(document)
    //                        document.isDownloaded = false
    //                    }
    //                    else
    //                    {
    //                    }
    //
    //                }
    //                else
    //                {
    //                    for file in document.localResolutionfile?.allObjects as! [LocalResolutionFile]{
    //
    //                        if file.isDownloaded == true
    //                        {
    //                            fileCount = fileCount + 1
    //                        }
    //                    }
    //
    //                    if fileCount == 0{
    ////                        self.getContext().delete(document)
    //                        document.isDownloaded = false
    //
    //                    }
    //                    else
    //                    {
    //                    }
    //                }
    //
    //                self.getContext().refreshAllObjects()
    //            }
    //
    //            try self.getContext().save()
    //            print("File Removed From DB...")
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //
    //
    //    class func deleteLocalFileWith(id:String,type:String){
    //
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        let defaults = UserDefaults.standard
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        fetchRequest.returnsObjectsAsFaults = false
    //        var predicate = NSPredicate()
    //        if type == "meeting"{
    //            predicate = NSPredicate(format: "ANY localMeetingfile.file_id = %@ AND user_id = %@", id,userID)
    //        }else if type == "agenda"{
    //            predicate = NSPredicate(format: "ANY localAgendafile.file_id = %@ AND user_id = %@", id,userID)
    //        }else if type == "observer"{
    //            predicate = NSPredicate(format: "ANY localObserverfile.file_id = %@ AND user_id = %@", id,userID)
    //        }else{
    //            predicate = NSPredicate(format: "ANY localResolutionfile.file_id = %@ AND user_id = %@", id,userID)
    //        }
    //
    //        fetchRequest.predicate = predicate
    //
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            for document in result{
    //                if type == "meeting"{
    //                    for file in document.localMeetingfile?.allObjects as! [LocalMeetingFile]{
    //                        if file.file_id == id && file.user_id == userID && file.tenantId == tenantID{
    //                        self.getContext().delete(file)
    //                        }
    //                    }
    //                }else if type == "observer"{
    //
    //                    for file in document.localObserverfile?.allObjects as! [LocalObserverFile]{
    //                         if file.file_id == id && file.user_id == userID && file.tenantId == tenantID{
    //                        self.getContext().delete(file)
    //                        }
    //                    }
    //
    //                }
    //                else if type == "agenda"{
    //
    //                    for file in document.localAgendafile?.allObjects as! [LocalAgendaFile]{
    //                        if file.file_id == id && file.user_id == userID && file.tenantId == tenantID{
    //                            self.getContext().delete(file)
    //                        }
    //                    }
    //
    //                }else{
    //                    for file in document.localResolutionfile?.allObjects as! [LocalResolutionFile]{
    //                         if file.file_id == id && file.user_id == userID && file.tenantId == tenantID{
    //                        self.getContext().delete(file)
    //                        }
    //                    }
    //                }
    //            }
    //            try self.getContext().save()
    //            self.getContext().refreshAllObjects()
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //    class func fetchLocalFilesOnly(id:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: "ANY localMeetingfile.file_id = %@ AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //        let agendaPredicate = NSPredicate(format: "ANY localAgendafile.file_id = %@ AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //        let observerPredicate = NSPredicate(format: "ANY localObserverfile.file_id = %@ AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //        let resolutionPredicate = NSPredicate(format: "ANY localResolutionfile.file_id = %@ AND tenantId = %@ AND user_id = %@", id,tenantID,userID)
    //
    //        let orPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [meetingPredicate, agendaPredicate,observerPredicate,resolutionPredicate])
    //
    //        fetchRequest.predicate = orPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func CheckLocalFileExist(type:String,fileId:String,id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let type1 = "meeting"
    //        let type2 = "agenda"
    //        let type3 = "meeting_resolution"
    //        let type4 = "observer"
    //        var predicate1 = NSPredicate()
    //        var predicate2 = NSPredicate()
    //        var predicate3 = NSPredicate()
    //        if type == type1{
    //            predicate1 = NSPredicate(format: "ANY localMeetingfile.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localMeetingfile.file_id = %@ AND id = %i ",fileId,id)
    //            predicate3 = NSPredicate(format:"ANY localMeetingfile.user_id = %@  AND user_id = %@",userID,userID)
    //
    //        }else if type == type2{
    //            predicate1 = NSPredicate(format: "ANY localAgendafile.type = %@ AND tenantId = %@", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localAgendafile.file_id = %@ AND id = %i ",fileId,id)
    //            predicate3 = NSPredicate(format:"ANY localAgendafile.user_id = %@  AND user_id = %@",userID,userID)
    //        }else if type == type3{
    //            predicate1 = NSPredicate(format: "ANY localResolutionfile.parent_type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localResolutionfile.file_id = %@ AND id = %i ",fileId,id)
    //            predicate3 = NSPredicate(format:"ANY localResolutionfile.user_id = %@  AND user_id = %@",userID,userID)
    //        }
    //        else if type == type4{
    //            predicate1 = NSPredicate(format: "ANY localObserverfile.parent_type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localObserverfile.file_id = %@ AND id = %i ",fileId,id)
    //            predicate3 = NSPredicate(format:"ANY localObserverfile.user_id = %@  AND user_id = %@",userID,userID)
    //        }else{
    //            predicate1 = NSPredicate(format: "ANY localResolutionfile.type = %@ AND tenantId = %@ ", type,tenantID)
    //            predicate2 = NSPredicate(format:"ANY localResolutionfile.file_id = %@ AND id = %i ",fileId,id)
    //            predicate3 = NSPredicate(format:"ANY localResolutionfile.user_id = %@  AND user_id = %@",userID,userID)
    //        }
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2,predicate3])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func onlinePdfNameUpdate(pdfname:String,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        var predicate = NSPredicate()
    //        if itemType == "meeting"
    //        {
    //            predicate = NSPredicate(format: "ANY localMeetingfile.pdfName = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }
    //        else if itemType == "agenda"
    //        {
    //            predicate = NSPredicate(format: "ANY localAgendafile.pdfName = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }
    //        else if itemType == "observer"
    //        {
    //            predicate = NSPredicate(format: "ANY localObserverfile.pdfName = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }
    //        else
    //        {
    //            predicate = NSPredicate(format: "ANY localResolutionfile.pdfName = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func localPdfNameUpdate(pdfname:String,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        var predicate = NSPredicate()
    //        if itemType == "meeting"
    //        {
    //            predicate = NSPredicate(format: "ANY localMeetingfile.localPdfname = %@ AND tenantId = %@ AND user_id = %@ ",pdfname,tenantID,userID)
    //        }
    //        else if itemType == "agenda"
    //        {
    //            predicate = NSPredicate(format: "ANY localAgendafile.localPdfname = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }else if itemType == "observer"
    //        {
    //            predicate = NSPredicate(format: "ANY localObserverfile.localPdfname = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }
    //        else
    //        {
    //            predicate = NSPredicate(format: "ANY localResolutionfile.localPdfname= %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //        }
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func updateEmailActionAndDocumentTitle(fileId:String,type:String)->(Bool,String){
    //
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsObjectsAsFaults = false
    //        var predicate = NSPredicate()
    //        var filename = ""
    //        var emailSelf =  false
    //         let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        if type == "meeting"{
    //            predicate = NSPredicate(format: "ANY meeting.file_id = %@ AND tenantId = %@ AND user_id = %@", fileId,tenantID,userID)
    //
    //        }else if type == "agenda"{
    //            predicate = NSPredicate(format: "ANY agenta.file_id = %@ AND tenantId = %@ AND user_id = %@", fileId,tenantID,userID)
    //
    //        }
    //        else if type == "observer"{
    //            predicate = NSPredicate(format: "ANY observer.file_id = %@ AND tenantId = %@ AND user_id = %@", fileId,tenantID,userID)
    //
    //        }else{
    //            predicate = NSPredicate(format: "ANY resolution.file_id = %@ AND tenantId = %@ AND user_id = %@",fileId ,tenantID,userID)
    //
    //        }
    //
    //        fetchRequest.predicate = predicate
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //            for document in result{
    //                for file in document.meeting?.allObjects as! [MeetingFile]{
    //                    if file.file_id == fileId && file.type == type{
    //                    filename = file.name!
    //                    emailSelf =  file.email_self
    //                    }
    //                }
    //
    //                for file in document.agenta?.allObjects as! [AgentaFile]{
    //                     if file.file_id == fileId && file.type == type{
    //                    filename = file.name!
    //                    emailSelf =  file.email_self
    //                    }
    //                }
    //
    //                for file in document.resolution?.allObjects as! [ResolutionFile]{
    //                     if file.file_id == fileId && file.type == type{
    //                    filename = file.name!
    //                    emailSelf =  file.email_self
    //                    }
    //                }
    //
    //            }
    //
    //
    //            return (emailSelf,filename)
    //        }catch{
    //            print("Error")
    //
    //            return (emailSelf,filename)
    //        }
    //
    //    }
    //
    //   class func updateOnlineDocumentFile(meeting_type:String,meeting_file_id:String,meeting_id:Int,filename:String,fileDescription:String){
    //        let document = CoreDatabaseModel.CheckFileExist(type: meeting_type, fileId: meeting_file_id, id: Int64(meeting_id)) as! [Documents]
    //        let context = CoreDatabaseModel.getContext()
    //        for documents in document{
    //            if meeting_type == "meeting"{
    //                for file in documents.meeting?.allObjects as! [MeetingFile]{
    //                    if file.file_id == meeting_file_id{
    //                        file.name = filename
    //                        file.file_description = fileDescription
    //                    }
    //                }
    //            }else if meeting_type == "agenda"{
    //
    //                for file in documents.agenta?.allObjects as! [AgentaFile]{
    //                    if file.file_id == meeting_file_id{
    //                        file.name = filename
    //                        file.file_description = fileDescription
    //                    }
    //                }
    //            }
    //
    //            do{
    //                try context.save()
    //            }catch{
    //
    //            }
    //        }
    //    }
    //
    //
    //    class func updateOfflineDocumentFile(meeting_type:String,meeting_file_id:String,meeting_id:Int,filename:String,fileDescription:String){
    //
    //        let document = CoreDatabaseModel.fetchLocalFile(id:meeting_file_id, type: meeting_type) as! [LocalDocuments]
    //        let context = CoreDatabaseModel.getContext()
    //        for documents in document
    //        {
    //            documents.isDownloaded = true
    //            if meeting_type == "meeting"{
    //                for file in documents.localMeetingfile?.allObjects as! [LocalMeetingFile]{
    //                    if file.file_id == meeting_file_id{
    //                        file.name = filename
    //                        file.file_description = fileDescription
    //                    }
    //                }
    //            }else if meeting_type == "agenda"{
    //
    //                for file in documents.localAgendafile?.allObjects as! [LocalAgendaFile]{
    //                    if file.file_id == meeting_file_id{
    //                        file.name = filename
    //                        file.file_description = fileDescription
    //                    }
    //                }
    //            }
    //
    //
    //            do{
    //                try context.save()
    //            }catch{
    //
    //            }
    //
    //        }
    //    }
    //
    //    // COMMITTEE DOCUMENTS FUNCTIONS
    //
    //    // CREATE COMMITEE DOCUMENTS
    //
    //    class  func createCommitteeDocumentsFrom(dictionary: [String: AnyObject],tenantId:String,document_type:String,isDownloaded:Bool) -> NSManagedObject? {
    //        let context =  CoreDatabaseModel.getContext()
    //        let userID: String  = UserDefaults.standard.string(forKey: "user_id")!
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "CommitteeDocuments", into: context) as? CommitteeDocuments  {
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //
    //
    //            if let committee = (dictionary["committee_detail"] as? [String:AnyObject]){
    //                if let id = (committee["id"] as? Int64){
    //                    documentsEntity.id = id
    //                }
    //                if let forming_dt = (committee["forming_dt"] as? NSNumber){
    //                    documentsEntity.forming_dt = forming_dt
    //                }
    //                if let title = committee["name"] as? String{
    //                    documentsEntity.name = title
    //                }
    //
    //                    documentsEntity.user_id = userID
    //
    //
    //                documentsEntity.chair = (dictionary["chair"] as? Bool)!
    //                if let file = (committee["committee_file"] as? [[String:AnyObject]]){
    //
    //                    for doc in file
    //                    {
    //                        let committeeFile = CommitteeFile(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            committeeFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String){
    //                            committeeFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            committeeFile.file_description = ""
    //                        }
    //
    //                        if let file_id = (doc["id"] as? String){
    //                            committeeFile.file_id = file_id
    //                        }
    //                        if let created_by = (doc["created_by"] as? String){
    //                            committeeFile.created_by = created_by
    //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            committeeFile.created_at = created_at
    //                        }
    //                        if let id = (doc["email_self"] as? Bool){
    //                            committeeFile.email_self = id
    //                        }
    //                       committeeFile.email_self = false
    //
    //                        if let id = (committee["id"] as? Int64){
    //                            committeeFile.id = id
    //                        }
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            committeeFile.read = id
    //                        }else{
    //                            committeeFile.read = false
    //                        }
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            committeeFile.read = id
    //                        }else{
    //                            committeeFile.read = false
    //                        }
    //
    //                        if let id = (doc["download"] as? Bool){
    //                            committeeFile.download = id
    //                        }else{
    //                            committeeFile.download = false
    //                        }
    //
    //
    //                        committeeFile.pdfName = ""
    //                        committeeFile.isDownloaded = isDownloaded
    //                        committeeFile.tenantId = tenantId
    //                        committeeFile.type = "userGroup"
    //                        committeeFile.user_id = userID
    //                        committeeFile.pspdf_file_id = ""
    //                        documentsEntity.addToCommitteeFile(NSSet(object: committeeFile))
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //
    //
    //            return documentsEntity
    //        }
    //
    //    }
    //          return nil
    //    }
    //
    //    class  func saveCommitteeDocumentsDataWith(array: [[String: AnyObject]],tenantId:String,document_type:String,isDownloaded:Bool) {
    //
    //        _ = array.map{self.createCommitteeDocumentsFrom(dictionary: $0, tenantId: tenantId, document_type: document_type,isDownloaded:isDownloaded)}
    //
    //    }
    //
    //
    //    class func CommitteeDocumentFileInsertQueryForOnline(file:[String:AnyObject],id:Int64,type:String,isDownloaded:Bool,pdfName:String,isDashboard:Bool)
    //    {
    //        let context =  CoreDatabaseModel.getContext()
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId = %@ AND user_id = %@ ", id,tenantID,userID)
    //        fetchRequest.predicate = meetingPredicate
    //        do
    //        {
    //            var documents = try context.fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //
    //
    //            if type == "userGroup"
    //            {
    //                let committeeFile = LocalCommitteeFiles(context: context)
    //                if let name = (file["name"] as? String){
    //                    committeeFile.name = name
    //                }
    //                if let description = (file["file_description"] as? String){
    //                    committeeFile.file_description = description
    //                }
    //                else
    //                {
    //                    committeeFile.file_description = ""
    //                }
    //
    //                if let file_id = (file["file_id"] as? String){
    //                    committeeFile.file_id = file_id
    //                }
    //                if let created_by = (file["created_by"] as? String){
    //                    committeeFile.created_by = created_by
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    committeeFile.created_at = created_at
    //                }
    //                if let id = (file["email_self"] as? Bool){
    //                    committeeFile.email_self = id
    //                }
    //                 committeeFile.email_self = false
    //                if let id = (file["id"] as? Int64){
    //                    committeeFile.id = id
    //                }
    //                if let id = (file["type"] as? String){
    //                    committeeFile.type = id
    //                }else{
    //                     committeeFile.type = "userGroup"
    //                }
    //                if let id = (file["read"] as? Bool){
    //                    committeeFile.read = id
    //                }else{
    //                    committeeFile.read = false
    //                }
    //
    //                if let id = (file["download"] as? Bool){
    //                    committeeFile.download = id
    //                }else{
    //                    committeeFile.download = false
    //                }
    //
    //                committeeFile.pspdf_file_id = ""
    //
    //                committeeFile.pdfName = ""
    //                committeeFile.isDownloaded = isDownloaded
    //                committeeFile.tenantId = tenantID
    //                committeeFile.user_id = userID
    //
    //
    //                if documents.count > 0{
    //                  documents[0].addToLocalCommitteeFile(NSSet(object: committeeFile))
    //                }
    //            }
    //
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //        catch
    //        {
    //
    //        }
    //
    //    }
    //
    //    class func fetchCommiteeDocuments(type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //         let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: "document_type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //
    //        fetchRequest.predicate = meetingPredicate
    //        let sdSortDate = NSSortDescriptor.init(key: "forming_dt", ascending: false)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchCommiteeDocumentsWithFilters(type:String,isAscending:Bool) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: "document_type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //
    //        fetchRequest.predicate = meetingPredicate
    //        let sdSortDate = NSSortDescriptor.init(key: "forming_dt", ascending: isAscending)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalCommitteeDocumentsWith(id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId = %@ ", id,tenantID)
    //        let userPredicate = NSPredicate(format: " user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchCommitteeFilesOnly(id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //         let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: "ANY committeeFile.id = %i AND tenantId = %@ ", id,tenantID)
    //        let userPredicate =   NSPredicate(format: "ANY committeeFile.user_id = %@",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchCommitteeFilesOnlyForReadAndUnread(id:String) -> Int{
    //
    //        let defaults = UserDefaults.standard
    //        var count = 0
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: "ANY committeeFile.file_id= %@ AND tenantId = %@ ", id,tenantID)
    //        let userPredicate =   NSPredicate(format: "ANY committeeFile.isDownloaded= %@ AND user_id = %@",NSNumber(value: true),userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //        fetchRequest.predicate = andPredicate
    //        fetchRequest.predicate = meetingPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //            for data in result{
    //                for fileInfo in data.committeeFile?.allObjects as! [CommitteeFile]{
    //                    if fileInfo.file_id == id && fileInfo.isDownloaded == true{
    //                        count = count + 1
    //                    }
    //                }
    //
    //            }
    //
    //
    //            return count
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return 0
    //        }
    //    }
    //
    //    class func onlinePdfNameUpdateForCommittee(pdfname:String,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //
    //         let   predicate = NSPredicate(format: "ANY committeeFile.pdfName = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func onlineUpdateForCommitteeFile(pdfname:String,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //
    //        let   predicate = NSPredicate(format: "ANY localCommitteeFile.pdfName = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func offlinePdfNameUpdateForCommittee(pdfname:String,itemType:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //
    //        let   predicate = NSPredicate(format: "ANY localCommitteeFile.localPdfname = %@ AND tenantId = %@ AND user_id = %@",pdfname,tenantID,userID)
    //
    //        fetchRequest.predicate = predicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class  func createCommitteeDocumentsForLocal(id:Int64,formingDate:NSNumber,userid:String,name:String,document_type:String,isDownloaded:Bool,tenantId:String,file: [String: AnyObject],type:String,pdfName:String,isDashboard:Bool,localPdfName:String,chair:Bool) {
    //        let context =  CoreDatabaseModel.getContext()
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "LocalCommitteDocuments", into: context) as? LocalCommitteDocuments
    //        {
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.id = id
    //            documentsEntity.user_id = userid
    //            documentsEntity.name = name
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //            documentsEntity.chair = chair
    //           documentsEntity.forming_dt = formingDate
    //
    //            if type == "userGroup"
    //            {
    //                let meetingFile = LocalCommitteeFiles(context: context)
    //
    //                if let name = (file["name"] as? String){
    //                    meetingFile.name = name
    //                }
    //                if let description = (file["description"] as? String){
    //                    meetingFile.file_description = description
    //                }
    //                else
    //                {
    //                    if let description = (file["file_description"] as? String){
    //                        meetingFile.file_description = description
    //                    }
    //                    else
    //                    {
    //                        meetingFile.file_description = ""
    //                    }
    //                }
    //
    //                if let type = (file["type"] as? String){
    //                    meetingFile.type = type
    //                }
    //                else
    //                {
    //                    meetingFile.type = "userGroup"
    //                }
    //                if let id = (file["file_id"] as? String){
    //                    meetingFile.file_id = id
    //                }
    //                if isDashboard{
    //                    if let id = (file["id"] as? String){
    //                        meetingFile.file_id = id
    //                    }
    //                }
    //                if let created_at = (file["created_at"] as? NSNumber){
    //                    meetingFile.created_at = created_at
    //                }
    //                if let created_by = (file["created_by"] as? String){
    //                    meetingFile.created_by = created_by
    //                }
    //
    //                if let id = (file["email_self"] as? Bool){
    //                    meetingFile.email_self = id
    //                }
    //
    //
    //                meetingFile.read = true
    //                meetingFile.email_self = false
    //                meetingFile.id = id
    //                meetingFile.tenantId = tenantId
    //                meetingFile.pdfName = pdfName
    //                meetingFile.isDownloaded = isDownloaded
    //                meetingFile.localPdfname = localPdfName
    //                meetingFile.user_id = userid
    //                meetingFile.pspdf_file_id = ""
    //
    //                documentsEntity.localCommitteeFile = NSSet(object: meetingFile)
    //            }
    //
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //
    //    }
    //    class func CheckOnlineCommitteeFileExist(type:String,fileId:String,id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        let  predicate1 = NSPredicate(format: "ANY committeeFile.type = %@ AND tenantId = %@", type,tenantID)
    //        let  predicate2 = NSPredicate(format:"ANY committeeFile.file_id = %@ AND id = %i ",fileId,id)
    //        let predicate3 = NSPredicate(format:"ANY committeeFile.user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate2,predicate3])
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func CheckLocalCommitteeFileExist(type:String,fileId:String,id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        let  predicate1 = NSPredicate(format: "ANY localCommitteeFile.type = %@ AND tenantId = %@", type,tenantID)
    //        let  predicate2 = NSPredicate(format:"ANY localCommitteeFile.file_id = %@ AND id = %i ",fileId,id)
    //        let predicate3 = NSPredicate(format:"ANY localCommitteeFile.user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2,predicate3])
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func CheckLocalCommitteeFileDownloaded(fileId:String,id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //
    //        let  predicate1 = NSPredicate(format: "ANY localCommitteeFile.user_id = %@ AND tenantId = %@",  userID,tenantID)
    //        let  predicate2 = NSPredicate(format:"ANY localCommitteeFile.file_id = %@ ",fileId)
    //
    ////        let  predicate1 = NSPredicate(format: "ANY localCommitteeFile.isDownloaded = %@ AND tenantId = %@",  NSNumber(value: true),tenantID)
    ////        let  predicate2 = NSPredicate(format:"ANY localCommitteeFile.file_id = %@ AND isDownloaded = %@ ",fileId, NSNumber(value: true))
    ////        let predicate3 = NSPredicate(format:"ANY localCommitteeFile.user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchCommiteeFilters(type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //       let predicate_one = NSPredicate(format: "ANY committeeFile.isDownloaded = %@ AND isDownloaded = %@ AND tenantId = %@ AND user_id = %@",  NSNumber(value: true),NSNumber(value: true), tenantID,userID)
    //
    //
    //
    //        fetchRequest.predicate = predicate_one
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //    class func fetchOnlineCommitteeFile(id:String,type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = true
    //        fetchRequest.fetchLimit = 1
    //        let  predicate2 = NSPredicate(format: "ANY committeeFile.type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //        let  predicate1 = NSPredicate(format:"ANY committeeFile.file_id = %@ ", id)
    //        let  predicate3 = NSPredicate(format:"ANY committeeFile.user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2,predicate3])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //
    //    class func fetchLocalCommitteeFile(id:String,type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = true
    //        fetchRequest.fetchLimit = 1
    //        let  predicate2 = NSPredicate(format: "ANY localCommitteeFile.type = %@ AND tenantId = %@ AND user_id = %@", type,tenantID,userID)
    //        let  predicate1 = NSPredicate(format:"ANY localCommitteeFile.file_id = %@ ", id)
    //        let  predicate3 = NSPredicate(format:"ANY localCommitteeFile.user_id = %@ ",userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2,predicate3])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //    class func checkIsMeetingResolution(id:String,parent_type:String) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = true
    //        fetchRequest.fetchLimit = 1
    //        let  predicate1 = NSPredicate(format: "ANY resolution.parent_type = %@ AND tenantId = %@ AND user_id = %@", parent_type,tenantID,userID)
    //        let  predicate2 = NSPredicate(format: "ANY resolution.file_id = %@", id)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1, predicate2])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchMeetingResolutionDocument(id:Int64) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = true
    //        fetchRequest.fetchLimit = 1
    //        let  predicate1 = NSPredicate(format: "ANY resolution.parent_id = %i AND id = %i AND tenantId = %@ AND user_id = %@", id,id,tenantID,userID)
    ////        let  predicate2 = NSPredicate(format: "ANY resolution.parent_id = %i AND id = %i", id,id)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.or, subpredicates: [predicate1])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //    class func fetchLocalCommitteeDocumentsFilters(type:String,isAscending:Bool) -> Array<Any>?{
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //        var predicate_one = NSPredicate()
    //        var predicate_two = NSPredicate()
    //        var predicate_three = NSPredicate()
    //
    //        predicate_one =  NSPredicate(format: "ANY document_type = %@ AND tenantId = %@ ", type,tenantID)
    //        predicate_two = NSPredicate(format: "isDownloaded = %@ AND user_id = %@" ,  NSNumber(value: true),  userID)
    //
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate_one, predicate_two])
    //        let sdSortDate = NSSortDescriptor.init(key: "forming_dt", ascending: isAscending)
    //        fetchRequest.sortDescriptors = [sdSortDate]
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //
    //            return result
    //        }
    //        catch {
    //
    //            print("Failed")
    //            return nil
    //        }
    //    }
    //
    //    class func updateCommiteeEmailActionAndDocumentTitle(fileId:String,type:String)->(Bool,String){
    //
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        var filename = ""
    //        var emailSelf =  false
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //
    //        let  predicate = NSPredicate(format: "ANY committeeFile.file_id = %@ AND tenantId = %@ AND user_id = %@", fileId,tenantID,userID)
    //
    //
    //
    //        fetchRequest.predicate = predicate
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //            for document in result{
    //                for file in document.committeeFile?.allObjects as! [CommitteeFile]{
    //                    if file.file_id == fileId && file.type == type{
    //                        filename = file.name!
    //                        emailSelf =  file.email_self
    //                    }
    //                }
    //
    //
    //
    //            }
    //
    //
    //            return (emailSelf,filename)
    //        }catch{
    //            print("Error")
    //
    //            return (emailSelf,filename)
    //        }
    //
    //
    //
    //    }
    //
    //    class func deleteLocalCommiteeFileWith(id:String,type:String){
    //
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //
    //        let predicate = NSPredicate(format: "ANY localCommitteeFile.file_id = %@", id)
    //
    //
    //        fetchRequest.predicate = predicate
    //
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //            for document in result{
    //
    //                    for file in document.localCommitteeFile?.allObjects as! [LocalCommitteeFiles]{
    //                        if file.file_id == id && file.user_id == userID && file.tenantId == tenantID{
    //                            self.getContext().delete(file)
    //                        }
    //                    }
    //
    //            }
    //            try self.getContext().save()
    //            self.getContext().refreshAllObjects()
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //    class func deleteLocalCommitteeDocumentsWith(id:Int64,type:String){
    //
    //        let fetchRequest = self.fetchRequest(entityName: "LocalCommitteDocuments")
    //        fetchRequest.returnsObjectsAsFaults = false
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID: String  = defaults.string(forKey: "user_id")!
    //
    //        let predicate = NSPredicate(format: "id = %i AND document_type = %@", id,type)
    //        fetchRequest.predicate = predicate
    //        do{
    //
    //            let result = try self.getContext().fetch(fetchRequest) as! [LocalCommitteDocuments]
    //
    //            for document in result{
    //                for file in document.localCommitteeFile?.allObjects as! [LocalCommitteeFiles]{
    //                    if file.user_id == userID && file.tenantId == tenantID{
    //                        self.getContext().delete(file)
    //                    }
    //                }
    //
    //                self.getContext().delete(document)
    ////                self.getContext().refreshAllObjects()
    //            }
    //
    //            try self.getContext().save()
    //            self.getContext().refreshAllObjects()
    //            print("File Removed From DB...")
    //        }catch{
    //            print("Error")
    //        }
    //    }
    //
    //    //OFFLINE Documents Update Ater UninstallApp
    //
    //    class func updateOnlinePSPDFLId(id:Int64,fileId:String,pspdfFileId:String){
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "CommitteeDocuments")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        let meetingPredicate = NSPredicate(format: " id = %i AND tenantId = %@ ", id,tenantID)
    //        let userPredicate = NSPredicate(format: "ANY committeeFile.file_id = %@ AND user_id = %@ ",fileId,userID)
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [meetingPredicate, userPredicate])
    //        fetchRequest.predicate = andPredicate
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [CommitteeDocuments]
    //
    //            for document in result{
    //                for file in document.committeeFile?.allObjects as! [CommitteeFile]{
    //                    if file.file_id == fileId {
    //                        file.pspdf_file_id = pspdfFileId
    //                        file.isDownloaded = true
    //                        file.read = true
    //                        document.isDownloaded = true
    //                    }
    //                }
    //            }
    //            do{
    //                try getContext().save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //        catch {
    //
    //            print("Failed")
    //        }
    //    }
    //
    //    class func UpadteOfflinePSPDFIdForExceptUserGroup(id:Int64,fileId:String,pspdfFileId:String,itemType:String){
    //
    //        let defaults = UserDefaults.standard
    //        let tenantID: String  = defaults.string(forKey: "tenant_id")!
    //        let userID = defaults.string(forKey: "user_id")!
    //        let fetchRequest = self.fetchRequest(entityName: "Documents")
    //        fetchRequest.returnsDistinctResults = true
    //        fetchRequest.returnsObjectsAsFaults = false
    //
    //        var predicate = NSPredicate()
    //        var predicateTwo = NSPredicate()
    //
    //        if itemType == "meeting"
    //        {
    //            predicate = NSPredicate(format: "ANY meeting.file_id = %@ AND tenantId = %@",fileId,tenantID)
    //            predicateTwo = NSPredicate(format: " id = %i AND user_id = %@ ", id,userID)
    //        }
    //        else if itemType == "agenda"
    //        {
    //            predicate = NSPredicate(format: "ANY agenta.file_id = %@ AND tenantId = %@",fileId,tenantID)
    //            predicateTwo = NSPredicate(format: " id = %i AND user_id = %@ ", id,userID)
    //        }
    //        else if itemType == "observer"{
    //            predicate = NSPredicate(format: "ANY observer.file_id = %@ AND tenantId = %@",fileId,tenantID)
    //            predicateTwo = NSPredicate(format: " id = %i AND user_id = %@ ", id,userID)
    //        }
    //        else
    //        {
    //            predicate = NSPredicate(format: "ANY resolution.file_id = %@ AND tenantId = %@",fileId,tenantID)
    //            predicateTwo = NSPredicate(format: "ANY resolution.parent_id = %i AND user_id = %@ ", id,userID)
    //        }
    //        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate, predicateTwo])
    //        fetchRequest.predicate = andPredicate
    //
    //        do {
    //            let result = try self.getContext().fetch(fetchRequest) as! [Documents]
    //            for document in result{
    //
    //                for file in document.meeting?.allObjects as! [MeetingFile]{
    //                    if file.file_id == fileId {
    //                        file.pspdf_file_id = pspdfFileId
    //                        file.isDownloaded = true
    //                        file.read = true
    //                        document.isDownloaded = true
    //                    }
    //                }
    //
    //                for file in document.agenta?.allObjects as! [AgentaFile]{
    //                    if file.file_id == fileId {
    //                        file.pspdf_file_id = pspdfFileId
    //                        file.isDownloaded = true
    //                        file.read = true
    //                        document.isDownloaded = true
    //                    }
    //                }
    //
    //
    //                for file in document.observer?.allObjects as! [ObserverFile]{
    //                    if file.file_id == fileId {
    //                        file.pspdf_file_id = pspdfFileId
    //                        file.isDownloaded = true
    //                        file.read = true
    //                        document.isDownloaded = true
    //                    }
    //                }
    //
    //
    //                for file in document.resolution?.allObjects as! [ResolutionFile]{
    //                    if file.file_id == fileId {
    //                        file.pspdf_file_id = pspdfFileId
    //                        file.isDownloaded = true
    //                        file.read = true
    //                        document.isDownloaded = true
    //                    }
    //                }
    //
    //            }
    //            do{
    //                try getContext().save()
    //            }catch(let error){
    //                print(error)
    //            }
    //        }
    //        catch {
    //
    //            print("Failed")
    //
    //        }
    //    }
    //
    //
    //
    //
    //    class  func createOfflineCommitteeDocumentsFrom(dictionary: [String: AnyObject],tenantId:String,document_type:String) -> NSManagedObject? {
    //        let context =  CoreDatabaseModel.getContext()
    //        let userID: String  = UserDefaults.standard.string(forKey: "user_id")!
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "LocalCommitteDocuments", into: context) as? LocalCommitteDocuments  {
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = true
    //
    //
    //            if let committee = (dictionary["committee_detail"] as? [String:AnyObject]){
    //                if let id = (committee["id"] as? Int64){
    //                    documentsEntity.id = id
    //                }
    //                if let forming_dt = (committee["forming_dt"] as? NSNumber){
    //                    documentsEntity.forming_dt = forming_dt
    //                }
    //                if let title = committee["name"] as? String{
    //                    documentsEntity.name = title
    //                }
    //
    //                documentsEntity.user_id = userID
    //
    //
    //                documentsEntity.chair = (dictionary["chair"] as? Bool)!
    //                if let file = (committee["committee_file"] as? [[String:AnyObject]]){
    //
    //                    for doc in file
    //                    {
    //                        let committeeFile = LocalCommitteeFiles(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            committeeFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String){
    //                            committeeFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            committeeFile.file_description = ""
    //                        }
    //
    //                        if let file_id = (doc["id"] as? String){
    //                            committeeFile.file_id = file_id
    //                        }
    //                        if let created_by = (doc["created_by"] as? String){
    //                            committeeFile.created_by = created_by
    //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            committeeFile.created_at = created_at
    //                        }
    //                        if let id = (doc["email_self"] as? Bool){
    //                            committeeFile.email_self = id
    //                        }
    //                        committeeFile.email_self = false
    //
    //                        if let id = (committee["id"] as? Int64){
    //                            committeeFile.id = id
    //                        }
    //
    //                        if let id = (committee["download"] as? Bool){
    //                            committeeFile.download = id
    //                        }else{
    //                            committeeFile.download = false
    //                        }
    //
    //                        if let id = (doc["user_offline_file_id"] as? String){
    //                            committeeFile.pspdf_file_id = id
    //                            self.updateOnlinePSPDFLId(id: (committee["id"] as! Int64), fileId: (doc["id"] as! String), pspdfFileId: id)
    //                        }
    //                        committeeFile.read = true
    //                        committeeFile.pdfName = ""
    //                        committeeFile.isDownloaded = true
    //                        committeeFile.tenantId = tenantId
    //                        committeeFile.type = "userGroup"
    //                        committeeFile.user_id = userID
    //
    //                        documentsEntity.addToLocalCommitteeFile(NSSet(object: committeeFile))
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //
    //
    //                return documentsEntity
    //            }
    //
    //        }
    //        return nil
    //    }
    //
    //    class  func saveOfflineCommitteeDocumentsDataWith(array: [[String: AnyObject]],tenantId:String,document_type:String) {
    //
    //        _ = array.map{self.createOfflineCommitteeDocumentsFrom(dictionary: $0, tenantId: tenantId, document_type: document_type)}
    //
    //    }
    //
    //
    //    class  func createOfflineDocumentsForLocal(dictionary: [String: AnyObject],tenantId:String,document_type:String,isDownloaded:Bool) -> NSManagedObject? {
    //        let context =  CoreDatabaseModel.getContext()
    //        let userID: String  = UserDefaults.standard.string(forKey: "user_id")!
    //        if let documentsEntity = NSEntityDescription.insertNewObject(forEntityName: "LocalDocuments", into: context) as? LocalDocuments  {
    //
    //            documentsEntity.tenantId = tenantId
    //            documentsEntity.document_type = document_type
    //            documentsEntity.isDownloaded = isDownloaded
    //            if let meeting = (dictionary["meeting"] as? [String:AnyObject])
    //            {
    //                if let id = (meeting["id"] as? Int64){
    //                    documentsEntity.id = id
    //                }
    //
    //                if let title = meeting["name"] as? String{
    //                    documentsEntity.name = title
    //                }
    //                if let ac = (meeting["start_time"] as? NSNumber){
    //                    documentsEntity.start_time = ac
    //                }
    //                else
    //                {
    //                    documentsEntity.start_time = 0
    //                }
    //                if let cr = (meeting["end_dt"] as? NSNumber){
    //                    documentsEntity.end_dt = cr
    //                }
    //                else
    //                {
    //                    documentsEntity.end_dt = 0
    //                }
    //                if let file = (meeting["meeting_file"] as? [[String:AnyObject]]){
    //
    //                    for doc in file
    //                    {
    //                        let meetingFile = LocalMeetingFile(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            meetingFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String){
    //                            meetingFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            meetingFile.file_description = ""
    //                        }
    //                        if let file_type = (doc["file_type"]!["description"] as? String){
    //                            meetingFile.file_type = file_type
    //                        }
    //                        if let type = (doc["type"] as? String){
    //                            meetingFile.type = type
    //                        }
    //                        else
    //                        {
    //                            meetingFile.type = "meeting"
    //                        }
    //                        if let id = (doc["id"] as? String){
    //                            meetingFile.file_id = id
    //                        }
    //                        //                        if let id = (doc["email_self"] as? Bool){
    //                        //                            meetingFile.email_self = id
    //                        //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            meetingFile.created_at = created_at
    //                        }
    //
    //                        meetingFile.modified_at = ""
    //                        if let id = (meeting["id"] as? Int64){
    //                            meetingFile.id = id
    //                        }
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            meetingFile.read = id
    //                        }else{
    //                            meetingFile.read = false
    //                        }
    //
    //                        if let id = (doc["download"] as? Bool){
    //                            meetingFile.download = id
    //                        }else{
    //                            meetingFile.download = false
    //                        }
    //
    //                        if let id = (doc["user_offline_file_id"] as? String){
    //                            meetingFile.pspdf_file_id = id
    //                            self.UpadteOfflinePSPDFIdForExceptUserGroup(id: (meeting["id"] as! Int64), fileId: (doc["id"] as! String), pspdfFileId: id, itemType: document_type)
    //                        }
    //
    //                        meetingFile.pdfName = ""
    //                        meetingFile.isDownloaded = isDownloaded
    //                        meetingFile.tenantId = tenantId
    //                        meetingFile.user_id = userID
    //                        documentsEntity.addToLocalMeetingfile(NSSet(object: meetingFile))
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //
    //                if let file = (meeting["agenda_file"] as? [[String:AnyObject]]){
    //
    //
    //                    for doc in file
    //                    {
    //                        let agentaFile = LocalAgendaFile(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            agentaFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String)
    //                        {
    //                            agentaFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            agentaFile.file_description = ""
    //                        }
    //                        if let type = (doc["type"] as? String){
    //                            agentaFile.type = type
    //                        }
    //                        else
    //                        {
    //                            agentaFile.type = "agenda"
    //                        }
    //                        //                        if let id = (doc["email_self"] as? Bool){
    //                        //                            agentaFile.email_self = id
    //                        //                        }
    //                        agentaFile.modified_at = ""
    //                        if let id = (meeting["id"] as? Int64){
    //                            agentaFile.id = id
    //                        }
    //
    //                        if let id = (doc["id"] as? String){
    //                            agentaFile.file_id = id
    //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            agentaFile.created_at = created_at
    //                        }
    //                        if let agenda_id = (doc["agenda_id"] as? Int64){
    //                            agentaFile.agenda_id = agenda_id
    //                        }
    //
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            agentaFile.read = id
    //                        }else{
    //                            agentaFile.read = false
    //                        }
    //
    //                        if let id = (doc["download"] as? Bool){
    //                            agentaFile.download = id
    //                        }else{
    //                            agentaFile.download = false
    //                        }
    //
    //                        if let id = (doc["user_offline_file_id"] as? String){
    //                            agentaFile.pspdf_file_id = id
    //                            self.UpadteOfflinePSPDFIdForExceptUserGroup(id: (meeting["id"] as! Int64), fileId: (doc["id"] as! String), pspdfFileId: id, itemType: "agenda")
    //                        }
    //
    //                        agentaFile.pdfName = "'"
    //                        agentaFile.isDownloaded = isDownloaded
    //                        agentaFile.tenantId = tenantId
    //                        agentaFile.user_id = userID
    //                        documentsEntity.addToLocalAgendafile((NSSet(object: agentaFile)))
    ////                        documentsEntity.localAgendafile =  NSSet(object: agentaFile)
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //
    //                }
    //
    //                if let file = (meeting["resolution_file"] as? [[String:AnyObject]])
    //                {
    //                    var resolutionFile:LocalResolutionFile!
    //                    var observerFile:LocalObserverFile!
    //                    for doc in file
    //                    {
    //                        if let type = (doc["type"] as? String)
    //                        {
    //                            if type == "observer"
    //                            {
    //                                observerFile = LocalObserverFile(context: context)
    //                                if let name = (doc["name"] as? String){
    //                                    observerFile.name = name
    //                                }
    //                                if let description = (doc["description"] as? String)
    //                                {
    //                                    observerFile.file_description = description
    //                                }
    //                                else
    //                                {
    //                                    observerFile.file_description = ""
    //                                }
    //
    //                                if let type = (doc["type"] as? String){
    //                                    observerFile.type = type
    //                                    observerFile.parent_type = type
    //                                }
    //
    //                                if let id = (meeting["id"] as? Int64){
    //                                    observerFile.id = id
    //                                }
    //
    //                                observerFile.modified_at = ""
    //                                if let id = (doc["resolution_id"] as? Int64){
    //                                    observerFile.parent_id = id
    //                                }
    //                                if let end_d = (doc["resolution_end_dt"] as? NSNumber){
    //                                    documentsEntity.end_dt = end_d
    //                                }
    //                                else
    //                                {
    //                                    documentsEntity.end_dt = 0
    //                                }
    //                                //                                if let id = (doc["email_self"] as? Bool){
    //                                //                                    observerFile.email_self = id
    //                                //                                }
    //                                if let id = (doc["id"] as? String){
    //                                    observerFile.file_id = id
    //                                }
    //                                if let created_at = (doc["created_at"] as? NSNumber){
    //                                    observerFile.created_at = created_at
    //                                }
    //
    //                                if let id = (doc["read"] as? Bool){
    //                                    observerFile.read = id
    //                                }else{
    //                                    observerFile.read = false
    //                                }
    //
    //                                if let id = (doc["download"] as? Bool){
    //                                    observerFile.download = id
    //                                }else{
    //                                    observerFile.download = false
    //                                }
    //
    //                                if let id = (doc["user_offline_file_id"] as? String){
    //                                    observerFile.pspdf_file_id = id
    //                                    self.UpadteOfflinePSPDFIdForExceptUserGroup(id: (meeting["id"] as! Int64), fileId: (doc["id"] as! String), pspdfFileId: id, itemType: "observer")
    //                                }
    //
    //                                observerFile.pdfName = ""
    //                                observerFile.isDownloaded = isDownloaded
    //                                observerFile.tenantId = tenantId
    //                                observerFile.user_id = userID
    //                                documentsEntity.addToLocalObserverfile((NSSet(object: observerFile)))
    ////                                documentsEntity.localObserverfile = NSSet(object: observerFile)
    //                            }
    //                            else
    //                            {
    //                                resolutionFile = LocalResolutionFile(context: context)
    //                                if let name = (doc["name"] as? String){
    //                                    resolutionFile.name = name
    //                                }
    //                                if let description = (doc["description"] as? String)
    //                                {
    //                                    resolutionFile.file_description = description
    //                                }
    //                                else
    //                                {
    //                                    resolutionFile.file_description = ""
    //                                }
    //
    //                                if let type = (doc["type"] as? String){
    //                                    resolutionFile.type = type
    //                                }
    //                                resolutionFile.parent_type = "meeting_resolution"
    //                                if let id = (meeting["id"] as? Int64){
    //                                    resolutionFile.id = id
    //                                }
    //
    //                                resolutionFile.modified_at = ""
    //                                if let id = (doc["resolution_id"] as? Int64){
    //                                    resolutionFile.parent_id = id
    //                                }
    //                                if let end_d = (doc["resolution_end_dt"] as? NSNumber){
    //                                    documentsEntity.end_dt = end_d
    //                                }
    //                                else
    //                                {
    //                                    documentsEntity.end_dt = 0
    //                                }
    //                                //                                if let id = (doc["email_self"] as? Bool){
    //                                //                                    resolutionFile.email_self = id
    //                                //                                }
    //                                if let id = (doc["id"] as? String){
    //                                    resolutionFile.file_id = id
    //                                }
    //                                if let created_at = (doc["created_at"] as? NSNumber){
    //                                    resolutionFile.created_at = created_at
    //                                }
    //
    //                                if let id = (doc["read"] as? Bool){
    //                                    resolutionFile.read = id
    //                                }else{
    //                                    resolutionFile.read = false
    //                                }
    //
    //                                if let id = (doc["download"] as? Bool){
    //                                    resolutionFile.download = id
    //                                }else{
    //                                    resolutionFile.download = false
    //                                }
    //
    //
    //                                if let description = (doc["file_type"]?["description"] as? String){
    //
    //                                    resolutionFile.file_type_description = description
    //                                }
    //                                else
    //                                {
    //                                    resolutionFile.file_type_description = ""
    //                                }
    //
    //
    //                                if let file_type_id = (doc["file_type"]?["id"] as? Int64){
    //
    //                                    resolutionFile.file_type_id = file_type_id
    //                                }
    //                                else
    //                                {
    //                                    resolutionFile.file_type_id = 0
    //                                }
    //
    //
    //                                if let id = (doc["user_offline_file_id"] as? String){
    //                                    resolutionFile.pspdf_file_id = id
    //                                    self.UpadteOfflinePSPDFIdForExceptUserGroup(id: (doc["resolution_id"] as! Int64), fileId: (doc["id"] as! String), pspdfFileId: id, itemType: (doc["type"] as? String)!)
    //                                }
    //
    //                                resolutionFile.pdfName = ""
    //                                resolutionFile.isDownloaded = isDownloaded
    //                                resolutionFile.tenantId = tenantId
    //                                resolutionFile.user_id = userID
    //
    //                                documentsEntity.addToLocalResolutionfile((NSSet(object: resolutionFile)))
    ////                                documentsEntity.localResolutionfile = NSSet(object: resolutionFile)
    //                            }
    //                        }
    //
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //            }
    //            if let resolution = (dictionary["resolution"] as? [String:AnyObject]){
    //                if let id = (resolution["id"] as? Int64){
    //                    documentsEntity.id = id
    //                }
    //                if let title = resolution["name"] as? String{
    //                    documentsEntity.name = title
    //                }
    //                if let ac = (resolution["start_time"] as? NSNumber){
    //                    documentsEntity.start_time = ac
    //                }
    //                else
    //                {
    //                    documentsEntity.start_time = 0
    //                }
    //                if let cr = (resolution["end_dt"] as? NSNumber){
    //                    documentsEntity.end_dt = cr
    //                }
    //                else
    //                {
    //                    documentsEntity.end_dt = 0
    //                }
    //                if let file = (resolution["resolution_file"] as? [[String:AnyObject]]){
    //
    //
    //                    for doc in file
    //                    {
    //                        let resolutionFile = LocalResolutionFile(context: context)
    //                        if let name = (doc["name"] as? String){
    //                            resolutionFile.name = name
    //                        }
    //                        if let description = (doc["description"] as? String)
    //                        {
    //                            resolutionFile.file_description = description
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_description = ""
    //                        }
    //
    //                        resolutionFile.type = document_type
    //                        resolutionFile.parent_type = document_type
    //
    //                        resolutionFile.modified_at = ""
    //                        if let id = (resolution["id"] as? Int64){
    //                            resolutionFile.id = id
    //                            resolutionFile.parent_id = id
    //                        }
    //
    //                        //                        if let id = (doc["email_self"] as? Bool){
    //                        //                            resolutionFile.email_self = id
    //                        //                        }
    //                        if let id = (doc["id"] as? String){
    //                            resolutionFile.file_id = id
    //                        }
    //                        if let created_at = (doc["created_at"] as? NSNumber){
    //                            resolutionFile.created_at = created_at
    //                        }
    //
    //                        if let id = (doc["read"] as? Bool){
    //                            resolutionFile.read = id
    //                        }else{
    //                            resolutionFile.read = false
    //                        }
    //
    //                        if let description = (doc["file_type"]?["description"] as? String){
    //
    //                            resolutionFile.file_type_description = description
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_description = ""
    //                        }
    //
    //
    //                        if let file_type_id = (doc["file_type"]?["id"] as? Int64){
    //
    //                            resolutionFile.file_type_id = file_type_id
    //                        }
    //                        else
    //                        {
    //                            resolutionFile.file_type_id = 0
    //                        }
    //
    //                        if let id = (doc["download"] as? Bool){
    //                            resolutionFile.download = id
    //                        }else{
    //                            resolutionFile.download = false
    //                        }
    //
    //
    //                        if let id = (doc["user_offline_file_id"] as? String){
    //                            resolutionFile.pspdf_file_id = id
    //                            self.UpadteOfflinePSPDFIdForExceptUserGroup(id: (resolution["id"] as! Int64), fileId: (doc["id"] as! String), pspdfFileId: id, itemType: document_type)
    //                        }
    //
    //                        resolutionFile.pdfName = ""
    //                        resolutionFile.isDownloaded = isDownloaded
    //                        resolutionFile.tenantId = tenantId
    //                        resolutionFile.user_id = userID
    //
    //                        documentsEntity.addToLocalResolutionfile((NSSet(object: resolutionFile)))
    //
    ////                        documentsEntity.localResolutionfile = NSSet(object: resolutionFile)
    //                        do{
    //                            try context.save()
    //                        }catch(let error){
    //                            print(error)
    //                        }
    //                    }
    //                }
    //            }
    //            if let aa = (dictionary["user_id"] as? String){
    //                documentsEntity.user_id = aa
    //            }
    //
    //            do{
    //                try context.save()
    //            }catch(let error){
    //                print(error)
    //            }
    //            return documentsEntity
    //        }
    //        return nil
    //    }
    //
    //    class  func saveOfflineDocumentsDataWith(array: [[String: AnyObject]],tenantId:String,document_type:String,isDownloaded:Bool) {
    //
    //        _ = array.map{self.createOfflineDocumentsForLocal(dictionary: $0, tenantId: tenantId, document_type: document_type,isDownloaded:isDownloaded)}
    //
    //    }
    
}
