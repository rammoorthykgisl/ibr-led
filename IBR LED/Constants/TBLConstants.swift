//
//  BoardVisionConstants.swift
//  boardVision
//
//  Created by Webykart Mac 2 on 23/01/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import SwiftyJSON
import Alamofire
import CoreData
public class TBLConstants  {
    // Defaults
    static let TBL_Local_Data = UserDefaults.standard
    // Local KEYS
    
    struct Keys{
        
        static let USER_NAME = "user_name"
        static let EMAIL = "email"
        static let COMPANY = "company"
        static let ACCESS_TOKEN = "access_token"
        static let POLE_NO = "pole_no"
        static let REMARKS = "remarks"
        static let ROAD_NAME = "road_name"
        static let CITY = "city"
        static let CITY_ID = "city_id"
        static let STATE_ID = "state_id"
        static let REPORT_ID = "report_id"
        static let STATE = "state"
        static let ISNOTE = "note"
        static let SUPPLIER = "supplier"
        static let SUPPLIERS = "suppliers"
        static let RECORDS  = "records"
        static let STATES = "states"
        static let CITIES = "cities"
        static let RELESE_NOTES = "relese_notes"
        static let ISUPDATE_AVAIL = "is_update"
        static let UPDATE_URL = "update_url"
    }
    
    struct DateAndTime {
        
        static func getMonthAndYearString() -> String{
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM yyyy"
            let result = formatter.string(from: date)
            return result
        }
        
        
        
        
        
        static func getCurrentDateString() -> String{
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd"
            let result = formatter.string(from: date)
            return result
        }
        
        static func dateValidation(date:String) ->(Date,Date){
            let crtdate = Date()
            let crtformatter = DateFormatter()
            crtformatter.dateFormat = "yyyy-MM-dd"
            let today = crtformatter.string(from: crtdate)
            
            
            let start: String = date
            let f = DateFormatter()
            f.dateFormat = "yyyy-MM-dd"
            let startDate: Date? = f.date(from: today)
            let endDate: Date? = f.date(from: start)
            return (startDate!,endDate!)
            
        }
        
        
        static func dateFormatConvertion(date:String)->(String,String,String){
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your date format
            dateFormatter.timeZone = TimeZone(abbreviation: "SGT") //Current time zone
            let date = dateFormatter.date(from: (date ))
            
            dateFormatter.dateFormat = "EEE" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date!) //pass Date here
            
            dateFormatter.dateFormat = "MMM yyyy" //Your New Date format as per requirement change it own
            let newDate1 = dateFormatter.string(from: date!) //pass Date here
            
            dateFormatter.dateFormat = "dd" //Your New Date format as per requirement change it own
            let newDate2 = dateFormatter.string(from: date!) //pass Date here
            
            return (newDate2,newDate1,newDate)
            
        }
        //
        //        static func dateConvertionForFinalizedDate(date:NSNumber)->(String,String,String){
        //
        //            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        //            formatter1.timeZone = TimeZone(abbreviation: "SGT")
        //            formatter1.dateFormat = "dd MMM yyyy"
        //
        //            formatter1.dateFormat = "EEE" //Your New Date format as per requirement change it own
        //            let newDate = formatter1.string(from: date1 as Date) //pass Date here
        //
        //            formatter1.dateFormat = "MMM yyyy" //Your New Date format as per requirement change it own
        //            let newDate1 = formatter1.string(from: date1 as Date) //pass Date here
        //
        //            formatter1.dateFormat = "dd" //Your New Date format as per requirement change it own
        //            let newDate2 = formatter1.string(from: date1 as Date) //pass Date here
        //
        //            return (newDate2,newDate1,newDate)
        //        }
        //        static func dateConvertionForEndDate(date:NSNumber)->(String,String,String){
        //
        //            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        //            formatter1.timeZone = TimeZone(abbreviation: "SGT")
        //            formatter1.dateFormat = "dd MMM yyyy"
        //
        //            formatter1.dateFormat = "EEEE" //Your New Date format as per requirement change it own
        //            let newDate = formatter1.string(from: date1 as Date) //pass Date here
        //
        //            formatter1.dateFormat = "MMM yyyy" //Your New Date format as per requirement change it own
        //            let newDate1 = formatter1.string(from: date1 as Date) //pass Date here
        //
        //            formatter1.dateFormat = "d" //Your New Date format as per requirement change it own
        //            let newDate2 = formatter1.string(from: date1 as Date) //pass Date here
        //
        //            formatter1.dateFormat = "dd" //Your New Date format as per requirement change it own
        //
        //            return (newDate2,newDate1,newDate)
        //        }
        //        static func dateConvertionForFilters(date:NSNumber)->Date{
        //
        //            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        ////            formatter1.timeZone = TimeZone(abbreviation: "UTC")
        //            formatter1.dateFormat = "yyyy-MM-dd"
        //            let dateString = formatter1.string(from: date1 as Date)
        //            let date = formatter1.date(from: dateString)!
        //            return date
        //        }
        //        static func timeConvertion(date:NSNumber)->String{
        //
        //            var time = ""
        //            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        //            formatter1.timeZone = TimeZone(abbreviation: "SGT")
        //            formatter1.dateFormat = "hh:mm a"
        //            time = formatter1.string(from: date1 as Date)
        //            return time
        //        }
        //
        //        static func dateConvertion(date:NSNumber)->String{
        //            var dateString = ""
        //            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        //            formatter1.timeZone = TimeZone(abbreviation: "SGT")
        //            formatter1.dateFormat = "dd MMM yyyy"
        //            dateString = formatter1.string(from: date1 as Date)
        //            return dateString
        //        }
        //
        //
        static func dateConvertionForOffline(date:NSNumber)->String{
            var dateString = ""
            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
            let formatter1 = DateFormatter()
            formatter1.timeZone = TimeZone(abbreviation: "SGT")
            formatter1.dateFormat = "dd MMM yyyy/\nHH:mm:ss a"
            dateString = formatter1.string(from: date1 as Date)
            return dateString
        }
        //        static func dateConvertionForRemarks(date:NSNumber)->String{
        //            var dateString = ""
        //            let date1 =  NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        //            formatter1.timeZone = TimeZone(abbreviation: "SGT")
        //            formatter1.dateFormat = "dd/MM/YYYY"
        //            dateString = formatter1.string(from: date1 as Date)
        //            return dateString
        //        }
        //        static func timeConvertionForRemarks(date:NSNumber)->String{
        //            var dateString = ""
        //            let date1 = NSDate(timeIntervalSince1970: Double(exactly: date)!)
        //            let formatter1 = DateFormatter()
        //            formatter1.timeZone = TimeZone(abbreviation: "SGT+1")
        //            formatter1.dateFormat = "HH:mm"
        //            dateString = formatter1.string(from: date1 as Date)
        //            return dateString
        //        }
        static func getCurrentDate() -> String{
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMMM-yyyy"
            let datePassing = formatter.string(from: date)
            return datePassing
        }
        
        static  func dateFromString(dateString:String)->Date{
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let date = formatter.date(from: dateString)
            return date!
        }
        static func getCurrentDateForOffline() -> Date{
            let date1 = Date()
            let formatter1 = DateFormatter()
            //formatter1.timeZone = TimeZone(abbreviation: "SGT")
            formatter1.dateFormat = "dd MMM yyyy/\nHH:mm:ss a"
            let dateString = formatter1.string(from: date1 as Date)
            let currentDate = formatter1.date(from: dateString)!
            return currentDate
        }
        
    }
    
    static func setImageFromUrl(url:URL) -> UIImage?{
        
        if let data = try? Data(contentsOf: url){
            return UIImage(data: data)!
        }
        return nil
    }
    
    static func showActivityIndicator(){
        let size = CGSize(width: 48, height: 48)
        let data = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), messageSpacing: 1.0, type:  NVActivityIndicatorType(rawValue: 5)!, color:#colorLiteral(red: 0.7450980392, green: 0.2784313725, blue: 0.2156862745, alpha: 1), padding: 1.0, displayTimeThreshold: 3, minimumDisplayTime: 3, backgroundColor: #colorLiteral(red: 0.137254902, green: 0.137254902, blue: 0.137254902, alpha: 0.9), textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(data, NVActivityIndicatorView.DEFAULT_FADE_IN_ANIMATION)    }
    
    static func hideActivityIndicator(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(NVActivityIndicatorView.DEFAULT_FADE_OUT_ANIMATION)
    }
    
    static  func alertMessageForApiErrorTypes(viewController:UIViewController,storyBoardName:String, ErrorTypeResponse:DataResponse<Any>? = nil,ErrorResponse:DataResponse<Data>? = nil,ErrorType:String? = nil){
        
        let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
        let vc:AlertErrorTypeMessageView = storyboard.instantiateViewController(withIdentifier: "AlertErrorTypeMessageView") as! AlertErrorTypeMessageView
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        if ErrorTypeResponse == nil{
            if ErrorResponse!.response?.statusCode == 400 || ErrorResponse!.response?.statusCode == 404 ||  ErrorResponse!.response?.statusCode == 409 || ErrorResponse!.response?.statusCode == 422 || ErrorResponse!.response?.statusCode == 401 || ErrorResponse!.response?.statusCode == 503 || ErrorResponse!.response?.statusCode == 403
            {
                let swiftyJsonVar = JSON(ErrorResponse!.data!)
                vc.err_msg = swiftyJsonVar["error"].string!
                viewController.present(vc, animated: true)
            }
            else{
                vc.title = ErrorType
                if Reachability.isConnectedToNetwork(){
                    vc.err_msg = "Server connection Error.\nPlease contact your admin!"
                }else{
                    vc.err_msg = "Please check your internet connectivity."
                }
                
                viewController.present(vc, animated: true)
            }
        }else{
            if ErrorTypeResponse!.response?.statusCode == 400 || ErrorTypeResponse!.response?.statusCode == 404 ||  ErrorTypeResponse!.response?.statusCode == 409 || ErrorTypeResponse!.response?.statusCode == 422  || ErrorTypeResponse!.response?.statusCode == 503 || ErrorTypeResponse!.response?.statusCode == 401 || ErrorTypeResponse!.response?.statusCode == 403
            {
                let swiftyJsonVar = JSON(ErrorTypeResponse!.data!)
                vc.err_msg = swiftyJsonVar["error"].string!
                viewController.present(vc, animated: true)
            }
            else{
                vc.title = ErrorType
                if Reachability.isConnectedToNetwork(){
                    vc.err_msg = "Server connection Error.\nPlease contact your admin!"
                }else{
                    vc.err_msg = "Please check your internet connectivity."
                }
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = UIModalPresentationStyle.popover
                viewController.present(vc, animated: true)
            }
        }
        
        
    }
    static func showErrorPopup(message:String,storyBoardName:String,viewController:UIViewController,title:String){
        let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
        let vc:AlertErrorTypeMessageView = storyboard.instantiateViewController(withIdentifier: "AlertErrorTypeMessageView") as! AlertErrorTypeMessageView
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.title = title
        vc.err_msg = message
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        viewController.present(vc, animated: true)
    }
    
    static func offlineAppAlert(){
        if (UIApplication.topViewController() as?  UIAlertController ) == nil{
            let alertController = UIAlertController(title: "NO INTERNET CONNECTION", message: "Please check your internet connectivity.", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                UIAlertAction in
                alertController.dismiss(animated: true, completion: nil)
            }
            
            // Add the actions
            alertController.addAction(okAction)
            // Present the controller
            UIApplication.topViewController()!.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    static func saveImage(image: UIImage,name:String) -> Bool {
        guard let data = image.jpegData(compressionQuality: 0.5) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("\(name).jpg")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    static func getSavedImage(named: String) -> (UIImage? , path:String?){
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return (UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path),URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return (nil,nil)
    }
    
    public static func reset() {
        let _persistentContainer = NSPersistentContainer(name: "IBR_LED")
        let coordinator = _persistentContainer.persistentStoreCoordinator
        for store in coordinator.persistentStores where store.url != nil {
            try? coordinator.remove(store)
            try? FileManager.default.removeItem(atPath: store.url!.path)
        }
    }
    static func navigationBarSetUp(vc:UIViewController){
        vc.navigationController?.navigationBar.tintColor = UIColor.white
        vc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    static func resizeImage(with image: UIImage?, scaledTo newSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(newSize, _: false,_ : 0.0)
        image?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
    
    
}

