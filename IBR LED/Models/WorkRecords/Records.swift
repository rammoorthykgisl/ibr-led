/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct Records : Mappable {
    
    
    
    
	var id : Int?
	var name : String?
	var name_old : String?
	var account_name : String?
	var account_no : String?
	var company_id : Int?
	var user_id : Int?
	var state_id : Int?
	var city_id : Int?
	var station_id : String?
	var account_id : String?
	var supplier_id : Int?
	var dun_id : String?
	var parliament_id : String?
	var district_id : String?
	var control_time_location : String?
	var meter_no : String?
	var subdistrict_id : String?
	var road_name : String?
	var village : String?
	var type : String?
	var remarks : String?
	var installed : Int?
	var installed_at : String?
	var phase : Int?
	var new : Int?
	var voltage_current : String?
	var voltage_previous : String?
	var status : String?
	var latitude : Double?
	var longitude : Double?
	var photo : [Any]?
	var created_at : String?
	var updated_at : String?
	var deleted_at : String?
	var verified_at : String?
	var updatedby_id : String?
	var deletedby_id : String?
	var task_asignment : String?
	var duplicate_flag : Int?
	var action_at : String?
	var supplier : Supplier?
    var pole_count:Int?
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		name <- map["name"]
		name_old <- map["name_old"]
		account_name <- map["account_name"]
		account_no <- map["account_no"]
		company_id <- map["company_id"]
		user_id <- map["user_id"]
		state_id <- map["state_id"]
		city_id <- map["city_id"]
		station_id <- map["station_id"]
		account_id <- map["account_id"]
		supplier_id <- map["supplier_id"]
		dun_id <- map["dun_id"]
		parliament_id <- map["parliament_id"]
		district_id <- map["district_id"]
		control_time_location <- map["control_time_location"]
		meter_no <- map["meter_no"]
		subdistrict_id <- map["subdistrict_id"]
		road_name <- map["road_name"]
		village <- map["village"]
		type <- map["type"]
		remarks <- map["remarks"]
		installed <- map["installed"]
		installed_at <- map["installed_at"]
		phase <- map["phase"]
		new <- map["new"]
		voltage_current <- map["voltage_current"]
		voltage_previous <- map["voltage_previous"]
		status <- map["status"]
		latitude <- map["latitude"]
		longitude <- map["longitude"]
		photo <- map["photo"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		deleted_at <- map["deleted_at"]
		verified_at <- map["verified_at"]
		updatedby_id <- map["updatedby_id"]
		deletedby_id <- map["deletedby_id"]
		task_asignment <- map["task_asignment"]
		duplicate_flag <- map["duplicate_flag"]
		action_at <- map["action_at"]
		supplier <- map["supplier"]
        pole_count <- map["pole_count"]
	}
    
    
    func transformFromJSON(_ value: Any?) -> [Records]? {
           guard let records = value as? [[String: Any]] else { return nil }
           return records.compactMap { dictionary -> Records? in
               if let record = Records(JSON: dictionary) { return record }
    
               return nil
           }
       }
    
}
