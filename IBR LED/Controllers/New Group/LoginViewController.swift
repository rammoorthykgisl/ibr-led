//
//  ViewController.swift
//  IBR LED
//
//  Created by BSSADM on 27/08/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import Alamofire
import Toaster
class LoginViewController: UIViewController,UITextFieldDelegate {
    
    fileprivate let apiConnector = LogInAPIManager()
    
    
    @IBOutlet weak var projectNameLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var showPassword: UIButton!
    
    var isShowPswd = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.setNavigationBarHidden(true, animated: true)
    }
    //BUTTONS_ACTIONS
    
    @IBAction func logInAction(_ sender: Any) {
        
        if emailTextField.text != "" && passwordTextField.text != ""{
            TBLConstants.showActivityIndicator()
            
            getUserToken(email: emailTextField.text! , password: passwordTextField.text!  ) { (sucess) in
                if sucess{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.getUsersDetails(completion: { (sucess,user)  in
                            
                            if user?.user?.roles?.first?.name == "staff"{
                                TBLConstants.hideActivityIndicator()
                                TBLConstants.TBL_Local_Data.set(user?.user?.company?.name!, forKey: TBLConstants.Keys.COMPANY)
                                TBLConstants.TBL_Local_Data.set(user?.user?.email!, forKey: TBLConstants.Keys.EMAIL)
                                Toast(text: "Login Sucessfully", delay: 0, duration: 1).show()
                                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                                let MainView = storyboard.instantiateViewController(withIdentifier: "SideMenu")
                                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                appdelegate.window!.rootViewController = MainView
                            }else{
                                TBLConstants.hideActivityIndicator()
                                TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.ACCESS_TOKEN)
                                TBLConstants.showErrorPopup(message: "Please use Staff email and Password", storyBoardName: "Main", viewController: self, title: "Access Denied")
                            }
                            
                            //self.present(MainView, animated: true, completion: nil)
                            
                        })
                        
                    }
                }else{
                    
                    TBLConstants.hideActivityIndicator()
                }
            }
        }else{
            Toast(text: "Please enter required credentials to proceed", delay: 0, duration: 1).show()
        }
        
        
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Change_Password_VC") as! ChangePasswordVC
        self.navigationController?.pushViewController(loginVC, animated: true)
        
    }
    
    @IBAction func showAndHidePassword(_ sender: Any) {
        
        if !isShowPswd{
            isShowPswd = !isShowPswd
            passwordTextField.isSecureTextEntry = false
            showPassword.setImage(#imageLiteral(resourceName: "hide-password"), for: .normal)
        }else{
            showPassword.setImage(#imageLiteral(resourceName: "show-password"), for: .normal)
            passwordTextField.isSecureTextEntry = true
            isShowPswd = !isShowPswd
        }
    }
    
    
    
    
    // GET ACCESS_TOKEN FROM USER CREDENTIALS
    
    func getUserToken (email:String,password:String,completion:@escaping (_ success:Bool ) -> Void){
        
        let _ =   apiConnector.LoginUserWithToken(email: email, password: password) { (users, error) in
            if users == nil{
                TBLConstants.alertMessageForApiErrorTypes(viewController: self, storyBoardName: "Main", ErrorTypeResponse: error)
                completion(false)
                
            }else{
                // SAVE ACCESS_TOKEN TO USER_DEFAULTS
                TBLConstants.TBL_Local_Data.set(users?.token!, forKey: TBLConstants.Keys.ACCESS_TOKEN)
                TBLConstants.TBL_Local_Data.set(users?.user?.name!, forKey: TBLConstants.Keys.USER_NAME)
                TBLConstants.TBL_Local_Data.synchronize()
                completion(true)
                
            }
            
        }
    }
    
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    //GET USERS DETAILS
    
    func getUsersDetails(completion:@escaping(_ success:Bool,_ user:Users?) -> Void){
        
        let _ = apiConnector.getUserID { (users, error) in
            if users == nil{
                TBLConstants.alertMessageForApiErrorTypes(viewController: self, storyBoardName: "Main", ErrorTypeResponse: error)
                completion(false,nil)
            }else{
                TBLConstants.TBL_Local_Data.set(users?.user?.name!, forKey: TBLConstants.Keys.USER_NAME)
                TBLConstants.TBL_Local_Data.synchronize()
                completion(true,users!)
            }
        }
    }
    
    
    
}

