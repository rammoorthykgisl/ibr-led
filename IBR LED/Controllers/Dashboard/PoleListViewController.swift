//
//  PoleListViewController.swift
//  IBR LED
//
//  Created by BSSADM on 03/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit

class PoleListViewController: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var poleListName: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet var emptyRecordLabel: UILabel!
    var record:[Records]?
    var poleNames = [Records]()
    var filteredRecords = [Records]()
    var names = [String]()
    var filteredNames = [String]()
    var street_name = ""
    
    var searching  = false
    override func viewDidLoad() {
        super.viewDidLoad()
        streetName.text = street_name
        searchBar.delegate = self
        filterRecord { (sucess) in
            
        }
        // Do any additional setup after loading the view.
        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.white]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes
    }
    
    
    
    @IBAction func addnewPole(_ sender: Any) {
        let reportVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "NewPoleVC") as! AddNewPoleViewController
        self.navigationController?.pushViewController(reportVC, animated: true)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.endEditing(true)
        searching = false
        poleNames.removeAll()
        filterRecord { (sucess) in
            
        }
        searchBar.isHidden = false
        poleListName.reloadData()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        poleNames.removeAll()
        filteredNames.removeAll()
        filterRecord { (sucess) in
            if !searchBar.text!.isEmpty{
                
                self.filteredNames = self.names.filter({($0.range(of: searchText, options: [.caseInsensitive]) != nil)})
                self.searching = true
            }
            else
            {
                self.filteredNames = self.names
            }
            self.poleListName.reloadData()
        }
        
        
    }
    
}

extension PoleListViewController:UITableViewDelegate,UITableViewDataSource {
    
    //WORK RECORD DELEGATES
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return filteredNames.count
        }else{
            return names.count
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let roadCell =   (tableView.dequeueReusableCell(withIdentifier: "road_cell") as! RoadListCell)
        var roadName:String!
        if searching{
            roadName = filteredNames[indexPath.row]
        }else{
            roadName = names[indexPath.row]
        }
        
        roadCell.roadName.text = roadName
        
        return roadCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let record = poleNames[indexPath.row]
        var roadName:String!
        if searching{
            roadName = filteredNames[indexPath.row]
        }else{
            roadName = names[indexPath.row]
        }
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "NewPoleAPIVC") as! AddNewPoleFromAPIViewController
        cameraVC.record = record
        cameraVC.poleName = roadName
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    func filterRecord(completionHandler: @escaping (_ success: Bool) -> Void){
        names.removeAll()
        for records in record!{
            if (records.installed! == 0) && ((records.status! == "\(0)" || records.status! == "\(1)")){
                if (records.road_name! == street_name){
                    let isPole = CoreDatabaseModel.checkPoleExist(name: records.name!)!
                    if isPole.count > 0{
                        
                    }else{
                        names.append(records.name!)
                        poleNames.append(records)
                    }
                    
                    
                }
            }
            
        }
        if self.names.count == 0{
            emptyRecordLabel.isHidden = false
        }else{
            emptyRecordLabel.isHidden = true
        }
        poleListName.reloadData()
        completionHandler(true)
    }
    
}

