//
//  PendingWorkRecordViewController.swift
//  IBR LED
//
//  Created by BSSADM on 25/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit

class PendingWorkRecordViewController: UIViewController {
    
    @IBOutlet var recordTableView: UITableView!
    
    @IBOutlet var clearAllButton: UIButton!
    
    @IBOutlet var emptyStatusLabel: UILabel!
    var pendingRecords = [PoleDetails]()
    fileprivate let apiConnector = RecordsAPIManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        recordTableView.delegate = self
        recordTableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        pendingRecords = CoreDatabaseModel.fetchPendingRecords()
        if pendingRecords.count > 0{
            emptyStatusLabel.isHidden = true
        }else{
            emptyStatusLabel.isHidden = false
        }
        recordTableView.reloadData()
    }
    
    
    @IBAction func sideMenuAction(_ sender: Any) {
        
        sideMenuController?.revealMenu()
    }
    
    @IBAction func clearAllRecord(_ sender: Any) {
       
        let isDelete = CoreDatabaseModel.deleteAllPendingRecords()
        if isDelete{
            DispatchQueue.main.async {
                self.pendingRecords.removeAll()
                self.emptyStatusLabel.isHidden = false
                self.recordTableView.reloadData()
                self.recordTableView.beginUpdates()
                self.recordTableView.endUpdates()
                   }
        }
       
       
    }
    
    
}
extension PendingWorkRecordViewController:UITableViewDelegate,UITableViewDataSource {
    
    //WORK RECORD DELEGATES
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pendingRecords.count
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let recordCell =   (tableView.dequeueReusableCell(withIdentifier: "work_record") as? workRecordsList){
            let record = pendingRecords[indexPath.row]
            let time = "\(record.time_stamp!.format1())".components(separatedBy: " +").first
            recordCell.timeStampLabel.text = time!.components(separatedBy: " ").last
            recordCell.lampPostName.text = record.name!
            recordCell.dateLabel.text = "\(record.time_stamp!.format())".components(separatedBy: " ").first
            recordCell.roadName.text = record.road_name!
            recordCell.submitButton.addTarget(self, action: #selector(self.submitClickAction(_:)), for: .touchUpInside)
            recordCell.submitButton.tag = indexPath.row
            return recordCell
        }
        
        
        
        
        return UITableViewCell()
    }
    
    //   func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    //     let DeleteAction = UIContextualAction(style: .destructive, title: "Submit", handler: { (action, view, success) in
    //        let record =  self.pendingRecords[indexPath.row]
    //              let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "SubmitNewPoleVC") as! SubmitNewPoleViewController
    //        cameraVC.isFromOffline = true
    //        cameraVC.pendingRecord = record
    //        self.navigationController?.pushViewController(cameraVC, animated: true)
    //     })
    //     DeleteAction.backgroundColor = #colorLiteral(red: 0.5803921569, green: 0.06666666667, blue: 0, alpha: 0.820901113)
    //     return UISwipeActionsConfiguration(actions: [DeleteAction])
    //   }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let record =  self.pendingRecords[indexPath.row]
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "SubmitNewPoleVC") as! SubmitNewPoleViewController
        cameraVC.isFromOffline = true
        cameraVC.pendingRecord = record
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    @objc func submitClickAction(_ sender:UIButton) {
        let pendingRecord =  self.pendingRecords[sender.tag ]
        TBLConstants.showActivityIndicator()
        let _ = apiConnector.checkRecordExist(location_id: nil, name: pendingRecord.name!, road_name: pendingRecord.road_name!, city_id: Int(pendingRecord.city_id), control_time_location: pendingRecord.control_time_location!, state_id: Int(pendingRecord.state_id)) { (sucess, error) in
            if sucess?.email_message == "Pole already exist!"{
                self.showAlert()
            }else{
                
                if pendingRecord.isNew == true{
                    if Reachability.isConnectedToNetwork() {
                        let _ = self.apiConnector.createNewPole(poleImage: UIImage(data: pendingRecord.photo1!)!, lampImage: UIImage(data: pendingRecord.photo2!)!, name: pendingRecord.name!, road_name: pendingRecord.road_name!, city_id: Int(pendingRecord.city_id), latitude:pendingRecord.latitude, longitude: pendingRecord.longitude, remarks: pendingRecord.remarks!, installed: Int(pendingRecord.installed), status: 1, new: 1, control_time_location: pendingRecord.control_time_location!, meter_no: "\(pendingRecord.meter_no!)", account_no: "\(pendingRecord.account_no!)",supplier: pendingRecord.supplier!) { sucess,user  in
                            let _ = CoreDatabaseModel.deletePendingRecord(name:pendingRecord.name! , roadName: pendingRecord.road_name!, control_time_location: pendingRecord.control_time_location!)
                            self.showSucessAlert()
                            
                        }
                    }else{
                        TBLConstants.showErrorPopup(message: "Please check internet connection to proceed.", storyBoardName: "Dashboard", viewController: self, title: "NO INTERNET CONNECTION")
                        
                    }
                }else{
                    if Reachability.isConnectedToNetwork() {
                        let _ = self.apiConnector.updatePoleDetails(reportID: pendingRecord.id! ,poleImage: UIImage(data: pendingRecord.photo1!)!, lampImage: UIImage(data: pendingRecord.photo2!)!, name: pendingRecord.name!, road_name: pendingRecord.road_name!, city_id: 0, latitude: pendingRecord.latitude, longitude: pendingRecord.longitude, remarks: pendingRecord.remarks!, installed: Int(pendingRecord.installed), status: 1, new: 0, control_time_location: pendingRecord.control_time_location!, meter_no: "\(pendingRecord.meter_no!)", account_no: "\(pendingRecord.account_no!)",phase : 0,supplier: pendingRecord.supplier!) { sucess,err  in
                            TBLConstants.TBL_Local_Data.set("", forKey: TBLConstants.Keys.REMARKS)
                            if sucess != nil{
                                let _ = CoreDatabaseModel.deletePendingRecord(name:pendingRecord.name! , roadName: pendingRecord.road_name!, control_time_location: pendingRecord.control_time_location!)
                                self.showSucessAlert()
                            }else{
                                 TBLConstants.hideActivityIndicator()
                                self.showFailureAlert(title: "Error", message: (err?.error?.localizedDescription)!)
                                
                            }
                            
                            
                            
                            
                        }
                    }else{
                        
                        TBLConstants.showErrorPopup(message: "Please check internet connection to proceed.", storyBoardName: "Main", viewController: self, title: "NO INTERNET CONNECTION")
                    }
                    
                }
            }
            
        }
        
        
    }
    func showSucessAlert(){
         TBLConstants.hideActivityIndicator()
        let alertController = UIAlertController(title: "", message: "Record has been submitted successfully", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let MainView = storyboard.instantiateViewController(withIdentifier: "SideMenu")
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = MainView
            TBLConstants.TBL_Local_Data.set("", forKey: TBLConstants.Keys.REMARKS)
           
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func showFailureAlert(title:String,message:String){
         TBLConstants.hideActivityIndicator()
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
            TBLConstants.TBL_Local_Data.set("", forKey: TBLConstants.Keys.REMARKS)
           
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(){
         TBLConstants.hideActivityIndicator()
        let alertController = UIAlertController(title: "Pole Duplication Issue", message: "Found same pole in the database.\nPlease rename this pole to submit.", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension Date {
    /**
     Formats a Date
     
     - parameters format: (String) for eg dd-MM-yyyy hh-mm-ss
     */
    func format(format:String = "dd-MM-yyyy HH-mm-ss") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
       // dateFormatter.timeZone = .current
        let dateString = dateFormatter.string(from: self)
        if let newDate = dateFormatter.date(from: dateString) {
            
            return newDate
        } else {
            return self
        }
    }
    func format1(format:String = "dd-MM-yyyy HH-mm-ss") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = .current
        let dateString = dateFormatter.string(from: self)
        if let newDate = dateFormatter.date(from: dateString) {
            
            return newDate.toLocalTime()
        } else {
            return self
        }
    }
    // Convert local time to UTC (or GMT)
       func toGlobalTime() -> Date {
           let timezone = TimeZone.current
           let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
           return Date(timeInterval: seconds, since: self)
       }

       // Convert UTC (or GMT) to local time
       func toLocalTime() -> Date {
           let timezone = TimeZone.current
           let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
           return Date(timeInterval: seconds, since: self)
       }
}
