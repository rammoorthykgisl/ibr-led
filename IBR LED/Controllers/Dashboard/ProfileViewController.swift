//
//  ProfileViewController.swift
//  IBR LED
//
//  Created by BSSADM on 22/10/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import SideMenuSwift
class ProfileViewController: UIViewController {

  
    @IBOutlet var nameTextField: UITextField!
    
    @IBOutlet var emailTextField: UITextField!
    
    @IBOutlet var companyTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.USER_NAME) as? String
        emailTextField.text =  TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.EMAIL) as? String
        companyTextField.text =  TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.COMPANY) as? String
    }
    

    @IBAction func changePassword(_ sender: Any) {
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
            self.navigationController?.pushViewController(cameraVC, animated: true)
        
    }
    
    
    @IBAction func swapMenuAction(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
}
