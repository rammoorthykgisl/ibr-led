//
//  RoadNameViewController.swift
//  IBR LED
//
//  Created by BSSADM on 30/08/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import Foundation
import UIKit

class RoadNameViewController: UIViewController,UISearchBarDelegate {
    
    var record:[Records]?
    var roadNames = [Records]()
    var filteredRecords = [Records]()
    var names = [String]()
    var filteredNames = [String]()
    var searching  = false
    @IBOutlet weak var roadListName: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet var emptyRecordLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        filterRecord { (sucess) in
            
        }
        // Do any additional setup after loading the view.
        let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.white]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes
    }
    
    
    @IBAction func sideMenuAction(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    @IBAction func addnewPole(_ sender: Any) {
        let reportVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "NewPoleVC") as! AddNewPoleViewController
        self.navigationController?.pushViewController(reportVC, animated: true)
    }
    
    
    
}

extension RoadNameViewController:UITableViewDelegate,UITableViewDataSource {
    
    //WORK RECORD DELEGATES
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return filteredNames.count
        }else{
            return names.count
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let roadCell =   (tableView.dequeueReusableCell(withIdentifier: "road_cell") as! RoadListCell)
        var roadName = ""
        if searching{
            roadName = filteredNames[indexPath.row]
        }else{
            roadName = names[indexPath.row]
        }
        
        roadCell.roadName.text = roadName
        
        return roadCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var roadName:String!
        searchBar.endEditing(true)
        if searching{
            roadName = filteredNames[indexPath.row]
        }else{
            roadName = names[indexPath.row]
        }
        let reportVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "PoleListVC") as! PoleListViewController
        reportVC.record = record
        reportVC.street_name = roadName
        self.navigationController?.pushViewController(reportVC, animated: true)
    }
    
    func filterRecord(completionHandler: @escaping (_ success: Bool) -> Void){
        
        for records in record!{
            if (records.installed! == 0) && ((records.status! == "\(0)" || records.status! == "\(1)")){
                let roadList = CoreDatabaseModel.checkRoadExist(roadName: records.road_name!)
                if roadList?.count == records.pole_count!{
                    
                }else{
                    names.append(records.road_name!)
                }
                
            }
        }
        names = names.uniques
        if self.names.count == 0{
            emptyRecordLabel.isHidden = false
        }else{
            emptyRecordLabel.isHidden = true
        }
        roadListName.reloadData()
        completionHandler(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.endEditing(true)
        searching = false
        roadNames.removeAll()
        filterRecord { (sucess) in
            
        }
        searchBar.isHidden = false
        
        roadListName.reloadData()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        roadNames.removeAll()
        filterRecord { (sucess) in
            if !searchBar.text!.isEmpty{
                
                self.filteredNames = self.names.filter({($0.range(of: searchText, options: [.caseInsensitive]) != nil)})
                self.searching = true
            }
            else
            {
                self.filteredNames = self.names
            }
            
        }
        self.roadListName.reloadData()
        
    }
    
    
}

class RoadListCell:UITableViewCell{
    
    
    @IBOutlet weak var roadName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
}

func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
    var buffer = [T]()
    var added = Set<T>()
    for elem in source {
        if !added.contains(elem) {
            buffer.append(elem)
            added.insert(elem)
        }
    }
    return buffer
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
