//
//  MenuViewController.swift
//  SideMenuExample
//
//  Created by kukushi on 11/02/2018.
//  Copyright © 2018 kukushi. All rights reserved.
//

import UIKit
import SideMenuSwift

class Preferences {
    static let shared = Preferences()
    var enableTransitionAnimation = false
}

class MenuViewController: UIViewController {
    var isDarkModeEnabled = false
    var isUpdateAvail = false
    fileprivate let apiConnector = RecordsAPIManager()
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
        }
    }
    
    @IBOutlet weak var selectionMenuTrailingConstraint: NSLayoutConstraint!
    private var themeColor = UIColor.white
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        versionCheck()
        SideMenuController.preferences.basic.position = .sideBySide
        SideMenuController.preferences.basic.menuWidth = 250
        isDarkModeEnabled = SideMenuController.preferences.basic.position == .under
        configureView()
        userName.text = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.USER_NAME) as? String
        versionLabel.text = appVersion!

        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "Home")
        }, with: "0")

        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "Account")
        }, with: "1")
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "Work_Record")
        }, with: "2")
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "Pending")
        }, with: "3")
        sideMenuController?.cache(viewControllerGenerator: {
            self.storyboard?.instantiateViewController(withIdentifier: "update_app")
        }, with: "4")
        
        sideMenuController?.delegate = self
    }

    func versionCheck(){
          let count = CoreDatabaseModel.fetchPendingRecords().count
        let _ = apiConnector.versionCheck(platForm: "iphone", version:appVersion!) { (version, error) in
            if version != nil{
                
                //Check if uypdate available
                if version?.status == 1{
                    TBLConstants.TBL_Local_Data.set(version?.upgrade_notes!, forKey: TBLConstants.Keys.RELESE_NOTES)
                    TBLConstants.TBL_Local_Data.set(true, forKey: TBLConstants.Keys.ISUPDATE_AVAIL)
                    TBLConstants.TBL_Local_Data.set(version?.url!, forKey: TBLConstants.Keys.UPDATE_URL)
                    self.isUpdateAvail = true
                    self.tableView.reloadData()
                    //Check if update is optional or compulsory
                    if version?.upgrade_status == true{
                       if count > 0{
                                                            
                        self.showAlert(isSignOut: false)
                                                            
                       }else{
                        let alertController = UIAlertController(title: "New Version Available", message: "New version available for IBRLED , kindly update.", preferredStyle: .alert)
                                                  
                                                    
                                                     // Create the actions
                                                     let okAction = UIAlertAction(title: "Update", style: UIAlertAction.Style.default) {
                                                         UIAlertAction in
                                                        
                                                           
                                                           let url = URL(string: (version?.url!)!)
                                                           UIApplication.shared.open(url!, options: [:])
                                                            
                                                        
                                                     }
                                                     
                                                     // Add the actions
                                                     alertController.addAction(okAction)
                                                     // Present the controller
                                                     self.present(alertController, animated: true, completion: nil)
                        }
                       
                    }else{
                        
                        if count > 0{
                            
                            self.showAlert(isSignOut: false)
                        
                        }else{
                            let alertController = UIAlertController(title: "New Version Available", message: "New version available for IBRLED , kindly update.Are you want to update?", preferredStyle: .alert)
                                                       
                                                        
                                                         // Create the actions
                                                         let okAction = UIAlertAction(title: "Update", style: UIAlertAction.Style.default) {
                                                             UIAlertAction in
                                                             
                                                                        let url = URL(string: (version?.url!)!)
                                                                        UIApplication.shared.open(url!, options: [:])
                                                                 
                                                            
                                                         }
                                                         let cancelAction = UIAlertAction(title: "Skip", style: UIAlertAction.Style.cancel) {
                                                             UIAlertAction in
                                                           self.getUserSelections()
                                                            alertController.dismiss(animated: true, completion: nil)
                                                         }
                                                         
                                                         // Add the actions
                                                         alertController.addAction(okAction)
                                                         alertController.addAction(cancelAction)
                                                         
                                                         // Present the controller
                                                         self.present(alertController, animated: true, completion: nil)
                        }
                       

                    }
                }else{
                    TBLConstants.TBL_Local_Data.set("Your app is up to date..!", forKey: TBLConstants.Keys.RELESE_NOTES)
                    TBLConstants.TBL_Local_Data.set(false, forKey: TBLConstants.Keys.ISUPDATE_AVAIL)
                     self.isUpdateAvail = true
                   // self.getUserSelections()
                }
            
            }else{
                TBLConstants.alertMessageForApiErrorTypes(viewController: self, storyBoardName: "Main", ErrorTypeResponse: error)
            }
        }
    }
    func getUserSelections(){
           
           let dispatchGroup = DispatchGroup()
       
           dispatchGroup.enter()
         let _ = apiConnector.getSuppliers { (suppliers, error) in
               if suppliers != nil{
                           TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.SUPPLIERS)
                           TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.STATES)
                           TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.CITIES)
                   if #available(iOS 12.0, *) {
                               // use iOS 12-only feature
                               do {
                                   let data = try NSKeyedArchiver.archivedData(withRootObject:  suppliers!.toJSON(), requiringSecureCoding: false)
                                  TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.SUPPLIERS)
                               } catch {
                                   return
                               }
                           } else {
                               // handle older versions
                               let data = NSKeyedArchiver.archivedData(withRootObject:  suppliers!.toJSON())
                             TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.SUPPLIERS)
                           }
                    dispatchGroup.leave()
               }else{
                   
               }
           }
            dispatchGroup.enter()
           let _ = apiConnector.getStates { (states, error) in
               if states != nil{
                   if #available(iOS 12.0, *) {
                               // use iOS 12-only feature
                               do {
                                   let data = try NSKeyedArchiver.archivedData(withRootObject:  states!.toJSON(), requiringSecureCoding: false)
                                  TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.STATES)
                               } catch {
                                   return
                               }
                           } else {
                               // handle older versions
                               let data = NSKeyedArchiver.archivedData(withRootObject:  states!.toJSON())
                             TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.STATES)
                           }
                    dispatchGroup.leave()
               }else{
                   
               }
           }
            dispatchGroup.enter()
           let _ = apiConnector.getCities { (cities, error) in
               if cities != nil{
                   if #available(iOS 12.0, *) {
                       // use iOS 12-only feature
                       do {
                           let data = try NSKeyedArchiver.archivedData(withRootObject:  cities!.toJSON(), requiringSecureCoding: false)
                          TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.CITIES)
                       } catch {
                           return
                       }
                   } else {
                       // handle older versions
                       let data = NSKeyedArchiver.archivedData(withRootObject:  cities!.toJSON())
                     TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.CITIES)
                   }
                    dispatchGroup.leave()
               }else{
                   
               }
           }
           dispatchGroup.notify(queue: .main) {
               TBLConstants.hideActivityIndicator()
                  }
       }
       
    private func configureView() {
        if isDarkModeEnabled {
            themeColor = UIColor(red: 0.03, green: 0.04, blue: 0.07, alpha: 1.00)
            
        } else {
            selectionMenuTrailingConstraint.constant = 0
            themeColor = UIColor(red: 0.98, green: 0.97, blue: 0.96, alpha: 1.00)
        }

        let sidemenuBasicConfiguration = SideMenuController.preferences.basic
        let showPlaceTableOnLeft = (sidemenuBasicConfiguration.position == .under) != (sidemenuBasicConfiguration.direction == .right)
        if showPlaceTableOnLeft {
            selectionMenuTrailingConstraint.constant = SideMenuController.preferences.basic.menuWidth - view.frame.width
        }

        //view.backgroundColor = themeColor
       // tableView.backgroundColor = themeColor
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let sidemenuBasicConfiguration = SideMenuController.preferences.basic
        let showPlaceTableOnLeft = (sidemenuBasicConfiguration.position == .under) != (sidemenuBasicConfiguration.direction == .right)
        selectionMenuTrailingConstraint.constant = showPlaceTableOnLeft ? SideMenuController.preferences.basic.menuWidth - size.width : 0
        view.layoutIfNeeded()
    }
}

extension MenuViewController: SideMenuControllerDelegate {
    func sideMenuController(_ sideMenuController: SideMenuController,
                            animationControllerFrom fromVC: UIViewController,
                            to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BasicTransitionAnimator(options: .transitionFlipFromLeft, duration: 0.6)
    }

    func sideMenuController(_ sideMenuController: SideMenuController, willShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller will show [\(viewController)]")
    }

    func sideMenuController(_ sideMenuController: SideMenuController, didShow viewController: UIViewController, animated: Bool) {
        print("[Example] View controller did show [\(viewController)]")
    }

    func sideMenuControllerWillHideMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu will hide")
    }

    func sideMenuControllerDidHideMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu did hide.")
    }

    func sideMenuControllerWillRevealMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu will reveal.")
    }

    func sideMenuControllerDidRevealMenu(_ sideMenuController: SideMenuController) {
        print("[Example] Menu did reveal.")
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }

    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SelectionCell
       // cell.contentView.backgroundColor = themeColor
        let row = indexPath.row
        if row == 0 {
            cell.titleLabel?.text = "Home"
            cell.menu_icon.image = #imageLiteral(resourceName: "home")
        } else if row == 1 {
            cell.titleLabel?.text = "Account"
            cell.menu_icon.image = #imageLiteral(resourceName: "profile")
        } else if row == 2 {
            cell.titleLabel?.text = "Work Record"
            cell.menu_icon.image = #imageLiteral(resourceName: "verified")
        }else if row == 3 {
            cell.titleLabel?.text = "Pending"
            cell.menu_icon.image = #imageLiteral(resourceName: "file")
        }else if row == 4 {
            cell.titleLabel?.text = "New Version"
            if self.isUpdateAvail{
              cell.menu_icon.image = #imageLiteral(resourceName: "update")
            }else{
              cell.menu_icon.image = #imageLiteral(resourceName: "latest")
            }
           
        }
        else if row == 5 {
            cell.titleLabel?.text = "Sign Out"
            cell.menu_icon.image = #imageLiteral(resourceName: "logout")
        }
        
       
        //cell.titleLabel?.textColor = isDarkModeEnabled ? .white : .black
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        if row == 5{
            signOut()
        }else{
            sideMenuController?.setContentViewController(with: "\(row)", animated: Preferences.shared.enableTransitionAnimation)
            sideMenuController?.hideMenu()
        }
        

        if let identifier = sideMenuController?.currentCacheIdentifier() {
            print("[Example] View Controller Cache Identifier: \(identifier)")
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
    func signOut(){
          sideMenuController?.revealMenu()
        let alertController = UIAlertController(title: "Sign Out", message: "Are you want to sign out?", preferredStyle: .alert)
        let count = CoreDatabaseModel.fetchPendingRecords().count
       
        // Create the actions
        let okAction = UIAlertAction(title: "yes", style: UIAlertAction.Style.default) {
            UIAlertAction in
            if count > 0{
                    
                self.showAlert(isSignOut: true)
                
                   }else{
                TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.ACCESS_TOKEN)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                let navController = NavigationController(rootViewController: initialViewController)
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.window?.rootViewController = navController
                appdelegate.window?.makeKeyAndVisible()
                   }
           
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
           alertController.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showAlert(isSignOut:Bool){
        var message = ""
        if isSignOut{
          message = "Please submit pending record to sign out."
        }else{
           message = "New version available for IBRLED , kindly clear your pending records and update."
        }
        let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
          
            self.sideMenuController?.setContentViewController(with: "\(3)", animated: Preferences.shared.enableTransitionAnimation)
            self.sideMenuController?.hideMenu()
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
}





class SelectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var menu_icon: UIImageView!
}
