//
//  WordRecordViewcontroller.swift
//  IBR LED
//
//  Created by BSSADM on 29/08/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit

class WordRecordViewcontroller: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var segmentControll: UISegmentedControl!
    
    @IBOutlet weak var workRecordTabelView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    fileprivate let apiConnector = RecordsAPIManager()
    
    @IBOutlet var emptyRecordLabel: UILabel!
    
    
    var verifiedRecords = [Records]()
    var completedrecords = [Records]()
    var filteredRecords = [Records]()
    var copyRecords = [Records]()
    var searching  = false
    var isCompleted = false
    var isVerified = false
    var decoded:Data?
    lazy var records =  completedrecords
   
    override func viewDidLoad() {
        super.viewDidLoad()
        workRecordTabelView.dataSource = self
        workRecordTabelView.delegate = self
        workRecordTabelView.tableFooterView = UIView()
        searchBar.delegate = self
        isCompleted = true
        
               let attributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15), NSAttributedString.Key.foregroundColor: UIColor.white]
               UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        records.removeAll()
        let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.RECORDS)
        
        if #available(iOS 12.0, *) {
            guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                else {
                    return
            }
            let dict = (unarchivedRecords as! [[String:Any]])
            for obj in dict{
                records.append(Records.init(JSON: obj)!)
            }
            self.copyRecords = self.records
        } else {
         if let unarchivedRecords = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [Records] {
                self.records = unarchivedRecords
                self.copyRecords = unarchivedRecords
            }
         }
        if self.records.count == 0{
            emptyRecordLabel.isHidden = false
        }else{
          emptyRecordLabel.isHidden = true
        }
            
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.filterRecords(records: records)
    }
    @IBAction func toggleList(_ sender: UISegmentedControl) {
            records.removeAll()
            searching = false
            searchBar.endEditing(true)
        if sender.selectedSegmentIndex == 0{
            isCompleted = true
            isVerified = false
            records = completedrecords
        }else{
            isVerified = true
            isCompleted = false
            records = verifiedRecords
        }
           workRecordTabelView.reloadData()
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    
    //Get RECORDS
    
    func getRecordsFromServer(completionHandler: @escaping (_ success: Bool) -> Void){
        TBLConstants.showActivityIndicator()
        copyRecords.removeAll()
        let _ = apiConnector.getWorkRecords { (records, error) in
            if records == nil{
                TBLConstants.hideActivityIndicator()
                TBLConstants.alertMessageForApiErrorTypes(viewController: self, storyBoardName: "Main", ErrorTypeResponse: error)
                 completionHandler(false)
                
            }else{
                TBLConstants.hideActivityIndicator()
                self.copyRecords = records!
                self.filterRecords(records: records!)
                completionHandler(true)
            }
        }
    }
    
    
    @IBAction func refreshAction(_ sender: Any) {
        
        getRecordsFromServer { (sucess) in
                   
               }
        
    }
    
    
    // FILTER RECORDS BASED ON WEATHER IT IS VERIFIED OR COMPLETED
    
    func filterRecords(records:[Records]){
        completedrecords.removeAll()
        verifiedRecords.removeAll()
        for record in records{
            if record.installed == 1 && record.status == "\(1)"{
               
                completedrecords.append(record)
            }else  if record.installed == 1 && record.status == "\(2)"{
               
                verifiedRecords.append(record)
            }
        }
        self.records.removeAll()
        if isVerified{
            self.records = verifiedRecords
        }else{
             self.records = completedrecords
        }
       
        workRecordTabelView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false;
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.endEditing(true)
        searching = false
        records.removeAll()
        filterRecords(records: copyRecords)
        searchBar.isHidden = false
        workRecordTabelView.reloadData()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        records.removeAll()
        
        filterRecords(records: copyRecords)
//        getRecordsFromServer { (sucess) in
            if !searchBar.text!.isEmpty{
                
                self.filteredRecords = self.records.filter({($0.name!.range(of: searchText, options: [.caseInsensitive]) != nil) || ($0.road_name!.range(of: searchText, options: [.caseInsensitive]) != nil)})
                self.searching = true
            }
            else
            {
                self.filteredRecords = self.records
            }
            self.workRecordTabelView.reloadData()
        //}
     

    }
    
   
}
extension WordRecordViewcontroller:UITableViewDelegate,UITableViewDataSource {
    
    //WORK RECORD DELEGATES
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
             return filteredRecords.count
        }else{
           return records.count
        }
      
    }
    
    

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let recordCell =   (tableView.dequeueReusableCell(withIdentifier: "work_record") as? workRecordsList){
            var record:Records!
            if searching{
             record = filteredRecords[indexPath.row]
            }else{
             record = records[indexPath.row]
            }
        
            recordCell.timeStampLabel.text = record.updated_at!.components(separatedBy: " ").last
            recordCell.lampPostName.text = record.name!
            recordCell.dateLabel.text = record.updated_at!.components(separatedBy: " ").first
            recordCell.roadName.text = record.road_name!
            return recordCell
        }
        
        
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let record = records[indexPath.row]
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "SubmitNewPoleVC") as! SubmitNewPoleViewController
        cameraVC.record = record
        cameraVC.isFromWorkRecord = true
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
}

class workRecordsList:UITableViewCell{
    
    @IBOutlet weak var timeStampLabel: UILabel!
    
    @IBOutlet weak var lampPostName: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var roadName: UILabel!
    
    @IBOutlet var submitButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
}
extension Encodable {

    var dict : [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else { return nil }
        return json
    }
}
