//
//  UpdateAppViewController.swift
//  IBR LED
//
//  Created by BSSADM on 29/10/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class UpdateAppViewController: UIViewController {

    @IBOutlet var releseNotes: IQTextView!
    
    @IBOutlet var updateButton: UIButton!
    
    @IBOutlet var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notes = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.RELESE_NOTES) as! String
        releseNotes.text = notes
        if TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ISUPDATE_AVAIL) as! Bool {
            updateButton.isHidden = false
             releseNotes.isEditable = true
        }else{
            releseNotes.isEditable = false
            titleLabel.isHidden = false
            titleLabel.isHidden = true
            updateButton.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
           sideMenuController?.revealMenu()
       }
    @IBAction func updateAction(_ sender: Any) {
        
        let url = URL(string: TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.UPDATE_URL) as! String)
        UIApplication.shared.open(url!, options: [:])
    }
    
    
    
   

}
