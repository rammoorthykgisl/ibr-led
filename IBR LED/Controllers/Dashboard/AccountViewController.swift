//
//  AccountViewController.swift
//  IBR LED
//
//  Created by BSSADM on 28/08/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import Toaster
import Alamofire

class AccountViewController: UIViewController {
    
    var apiConnector = LogInAPIManager()
    
    
    @IBOutlet var currentPasswordTextField: UITextField!
    
    @IBOutlet var newPasswordTextField: UITextField!
    
    @IBOutlet var confirmPasswordTextfield: UITextField!
    
    @IBOutlet var showCurrentPassword: UIButton!
    
    @IBOutlet var showNewPassword: UIButton!
    
    @IBOutlet var showConfirmPassword: UIButton!
    
    var isCurrentPSWD = false
    var isNewPSWD = false
    var isConfirmPSWD = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    @IBAction func changePasswordAction(_ sender: Any) {
        
        changePassword()
    }
    
    func changePassword(){
        
        if currentPasswordTextField.text != "" && newPasswordTextField.text != "" && confirmPasswordTextfield.text != "" {
            
            if newPasswordTextField.text == confirmPasswordTextfield.text{
                
                
                let url = BaseService.API_URL + "password/change"
                var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
                defaultHeaders["Content-Type"] = "application/x-www-form-urlencoded"
                defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
                defaultHeaders["Accept"] =  "application/json"
                let param = ["current_password":currentPasswordTextField.text!, "new_password":newPasswordTextField.text!]
                Alamofire.request(url, method: .post, parameters:param ,encoding: URLEncoding.default, headers: defaultHeaders).responseJSON {
                    response in
                    switch response.result {
                    case .success:
                        print(response)
                        let jsonResponse = response.value as! [String:Any]
                        let message = jsonResponse["message"] as? String
                        
                        
                        if message != nil{
                            // Create the alert controller
                            let alertController = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                                TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.ACCESS_TOKEN)
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                              let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                                              let navController = NavigationController(rootViewController: initialViewController)
                                              let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                              appdelegate.window?.rootViewController = navController
                                              appdelegate.window?.makeKeyAndVisible()
                            }
                            // Add the actions
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                        }else{
                            // Create the alert controller
                            let error = jsonResponse["error"] as! String
                            let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                                alertController.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(okAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    case .failure(let error):
                        
                        print(error)
                    }
                }
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                //                let _ = apiConnector.passwordChange(currentPassword: currentPasswordTextField.text!, confirmPassword: confirmPasswordTextfield.text!) { (data, response) in
                //                guard let responseJSON = response as? [String: Any] else {
                //                               return
                //                           }
                //
                //                    // Create the alert controller
                //                       let alertController = UIAlertController(title: "Sucess", message: "your Password has been updated sucessfully.", preferredStyle: .alert)
                //
                //                       // Create the actions
                //                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                //                           UIAlertAction in
                //
                //                                            TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.ACCESS_TOKEN)
                //                                                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //                                                                        let MainView = storyboard.instantiateViewController(withIdentifier: "LoginVC")
                //                                                                          let appdelegate = UIApplication.shared.delegate as! AppDelegate
                //                                                                          appdelegate.window!.rootViewController = MainView
                //                       }
                //                     // Add the actions
                //                       alertController.addAction(okAction)
                //
                //                       // Present the controller
                //                       self.present(alertController, animated: true, completion: nil)
                //
                //
                //
                //
                //
                //
                //
                //
                //                }
            }else{
                Toast(text: "New password and Confirm password is mismatched!", delay: 0, duration: 1).show()
                
            }
            
        }else{
            if currentPasswordTextField.text == ""{
                Toast(text: "Please enter current password!", delay: 0, duration: 1).show()
                
            }else if newPasswordTextField.text == ""{
                Toast(text: "Please enter new password!", delay: 0, duration: 1).show()
                
            }else if confirmPasswordTextfield.text == ""{
                Toast(text: "Please enter confirm password!", delay: 0, duration: 1).show()
                
            }
        }
        
    }
    
    
    
    
    @IBAction func hideAndShowPassword(_ sender: UIButton) {
        
        
        
        
        if sender.tag == 0{
            if !isCurrentPSWD{
                isCurrentPSWD = !isCurrentPSWD
                currentPasswordTextField.isSecureTextEntry = false
                showCurrentPassword.setImage(#imageLiteral(resourceName: "hide-password"), for: .normal)
            }else{
                showCurrentPassword.setImage(#imageLiteral(resourceName: "show-password"), for: .normal)
                currentPasswordTextField.isSecureTextEntry = true
                isCurrentPSWD = !isCurrentPSWD
            }
        }else if sender.tag == 1{
            if !isNewPSWD{
                isNewPSWD = !isNewPSWD
                newPasswordTextField.isSecureTextEntry = false
                showNewPassword.setImage(#imageLiteral(resourceName: "hide-password"), for: .normal)
            }else{
                showNewPassword.setImage(#imageLiteral(resourceName: "show-password"), for: .normal)
                newPasswordTextField.isSecureTextEntry = true
                isNewPSWD = !isNewPSWD
            }
        }else if sender.tag == 2{
            if !isConfirmPSWD{
                isConfirmPSWD = !isConfirmPSWD
                confirmPasswordTextfield.isSecureTextEntry = false
                showConfirmPassword.setImage(#imageLiteral(resourceName: "hide-password"), for: .normal)
            }else{
                showConfirmPassword.setImage(#imageLiteral(resourceName: "show-password"), for: .normal)
                confirmPasswordTextfield.isSecureTextEntry = true
                isConfirmPSWD = !isConfirmPSWD
            }
        }
        
        
        
    }
    
    
    
    
    
    
}
