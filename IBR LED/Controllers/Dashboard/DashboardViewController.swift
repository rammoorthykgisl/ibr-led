//
//  DashboardVC.swift
//  IBR LED
//
//  Created by BSSADM on 28/08/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import SideMenuSwift
import UICircularProgressRing
import Crashlytics
class DashboardViewController: UIViewController,UICircularProgressRingDelegate {
   
    

    @IBOutlet weak var progressBarView: UICircularProgressRing!
    @IBOutlet weak var dateLabel: UILabel!
    fileprivate let apiConnector = RecordsAPIManager()
    var completedrecords = [Records]()
    var record = [Records]()
    override func viewDidLoad() {
        super.viewDidLoad()
     
        progressBarView.isHidden = true
        getRecordsFromServer()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {

        if Reachability.isConnectedToNetwork() {
                       
                   } else {
            completedrecords.removeAll()
            record.removeAll()
                       TBLConstants.hideActivityIndicator()
            let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.RECORDS)
                                        if #available(iOS 12.0, *) {
                                            guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                                                else {
                                                    return
                                            }
                                            let dict = (unarchivedRecords as! [[String:Any]])
                                            for obj in dict{
                                               let jsonObj = Records.init(JSON: obj)!
                                               self.record.append(jsonObj)
                                            }
                                        } else {
                                         if let unarchivedRecords = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [Records] {
                                                self.record = unarchivedRecords
                                            }
                                         }
                                       
                           self.filterRecords(records: self.record)

        }
        navigationController?.setNavigationBarHidden(false, animated: true)
        dateLabel.text = TBLConstants.DateAndTime.getCurrentDate()
       
      
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
         sideMenuController?.revealMenu()
    }
    
    
    @IBAction func refreshAction(_ sender: Any) {
        
        getRecordsFromServer()
    }
    
    
    
    
    
    @IBAction func workRecordAction(_ sender: Any) {
        sideMenuController?.setContentViewController(with: "\(2)", animated: Preferences.shared.enableTransitionAnimation)
        sideMenuController?.hideMenu()
        
    }
    
    
    @IBAction func pendingRecord(_ sender: Any) {
        
        sideMenuController?.setContentViewController(with: "\(3)", animated: Preferences.shared.enableTransitionAnimation)
               sideMenuController?.hideMenu()
    }
    
    @IBAction func addNewReport(_ sender: Any) {

        let reportVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "RoadListVC") as! RoadNameViewController
        reportVC.record = record
        self.navigationController?.pushViewController(reportVC, animated: true)
    }
    
    //Get RECORDS
    
    func getRecordsFromServer(){
      
        TBLConstants.showActivityIndicator()
        let _ = apiConnector.getWorkRecords { (records, error) in
            if records == nil{
                TBLConstants.hideActivityIndicator()
                TBLConstants.alertMessageForApiErrorTypes(viewController: self, storyBoardName: "Main", ErrorTypeResponse: error)
                
                
            }else{
                self.record.removeAll()
                 TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.RECORDS)
                if #available(iOS 12.0, *) {
                    // use iOS 12-only feature
                    do {
                        let data = try NSKeyedArchiver.archivedData(withRootObject:  records!.toJSON(), requiringSecureCoding: false)
                       TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.RECORDS)
                    } catch {
                        return
                    }
                } else {
                    // handle older versions
                    let data = NSKeyedArchiver.archivedData(withRootObject:  records!.toJSON())
                  TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.RECORDS)
                }
               
                
                let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.RECORDS)
                 if #available(iOS 12.0, *) {
                     guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                         else {
                             return
                     }
                     let dict = (unarchivedRecords as! [[String:Any]])
                     for obj in dict{
                        let jsonObj = Records.init(JSON: obj)!
                        self.record.append(jsonObj)
                     }
                 } else {
                  if let unarchivedRecords = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [Records] {
                         self.record = unarchivedRecords
                     }
                  }
                
                self.filterRecords(records: records!)
            }
        }
    }
    
   
    
   
    
    
   
    
    // FILTER RECORDS BASED ON WEATHER IT IS VERIFIED OR COMPLETED
    
    func filterRecords(records:[Records]){
        completedrecords.removeAll()
        for record in records{
            if record.installed == 1 {
                completedrecords.append(record)
            }
        }
      
        progressBarView.isHidden = false
        progressBarView.maxValue = CGFloat(records.count)
        progressBarView.style = .ontop
        progressBarView.outerRingWidth = 20
        progressBarView.outerRingColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        progressBarView.innerRingWidth = 25
        progressBarView.innerRingColor =  #colorLiteral(red: 0.8588235294, green: 0.568627451, blue: 0.2352941176, alpha: 1)
        progressBarView.startAngle = 270
        progressBarView.delegate = self
        DispatchQueue.main.async {
            self.progressBarView.startProgress(to: CGFloat(self.completedrecords.count), duration: 3.0) {
                     print("Done animating!")
                self.getUserSelections()
                   }
        }
        TBLConstants.hideActivityIndicator()
    }
    
    
    func didFinishProgress(for ring: UICircularProgressRing) {
           
       }
       
       func didPauseProgress(for ring: UICircularProgressRing) {
           
       }
       
       func didContinueProgress(for ring: UICircularProgressRing) {
           
       }
       
       func didUpdateProgressValue(for ring: UICircularProgressRing, to newValue: CGFloat) {
           
       }
       
       func willDisplayLabel(for ring: UICircularProgressRing, _ label: UILabel) {
        label.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        label.font = UIFont(name: "Arial-BoldMT", size: 50)
        label.text = "\(completedrecords.count)/\(self.record.count)"
      
    }
    
    func getUserSelections(){
                let cities = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.CITIES)
                let states = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.STATES)
                let suppliers = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.SUPPLIERS)
        
        
        if (cities != nil) && (states != nil) && (suppliers != nil)
        {
            
        }else{
                let dispatchGroup = DispatchGroup()
            
                dispatchGroup.enter()
              let _ = apiConnector.getSuppliers { (suppliers, error) in
                    if suppliers != nil{
                                TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.SUPPLIERS)
                                TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.STATES)
                                TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.CITIES)
                        if #available(iOS 12.0, *) {
                                    // use iOS 12-only feature
                                    do {
                                        let data = try NSKeyedArchiver.archivedData(withRootObject:  suppliers!.toJSON(), requiringSecureCoding: false)
                                       TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.SUPPLIERS)
                                    } catch {
                                        return
                                    }
                                } else {
                                    // handle older versions
                                    let data = NSKeyedArchiver.archivedData(withRootObject:  suppliers!.toJSON())
                                  TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.SUPPLIERS)
                                }
                         dispatchGroup.leave()
                    }else{
                        
                    }
                }
                 dispatchGroup.enter()
                let _ = apiConnector.getStates { (states, error) in
                    if states != nil{
                        if #available(iOS 12.0, *) {
                                    // use iOS 12-only feature
                                    do {
                                        let data = try NSKeyedArchiver.archivedData(withRootObject:  states!.toJSON(), requiringSecureCoding: false)
                                       TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.STATES)
                                    } catch {
                                        return
                                    }
                                } else {
                                    // handle older versions
                                    let data = NSKeyedArchiver.archivedData(withRootObject:  states!.toJSON())
                                  TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.STATES)
                                }
                         dispatchGroup.leave()
                    }else{
                        
                    }
                }
                 dispatchGroup.enter()
                let _ = apiConnector.getCities { (cities, error) in
                    if cities != nil{
                        if #available(iOS 12.0, *) {
                            // use iOS 12-only feature
                            do {
                                let data = try NSKeyedArchiver.archivedData(withRootObject:  cities!.toJSON(), requiringSecureCoding: false)
                               TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.CITIES)
                            } catch {
                                return
                            }
                        } else {
                            // handle older versions
                            let data = NSKeyedArchiver.archivedData(withRootObject:  cities!.toJSON())
                          TBLConstants.TBL_Local_Data.set(data, forKey: TBLConstants.Keys.CITIES)
                        }
                         dispatchGroup.leave()
                    }else{
                        
                    }
                }
                dispatchGroup.notify(queue: .main) {
                    TBLConstants.hideActivityIndicator()
                       }
        }

          }
          
    
}
    

