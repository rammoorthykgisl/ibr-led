//
//  SubmitNewPoleViewController.swift
//  IBR LED
//
//  Created by BSSADM on 05/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SideMenuSwift
import Toaster
class SubmitNewPoleViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {
    
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    @IBOutlet weak var mapBottomView: UIView!
    
    @IBOutlet weak var lattitudeLabel: UILabel!
    
    @IBOutlet weak var longitudeLabel: UILabel!
    
    @IBOutlet weak var poleNumberLabel: UITextField!
    
    @IBOutlet weak var roadNameLabel: UITextField!
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var timeControlLocationLabel: UITextField!
    
    @IBOutlet weak var meterNumberLabel: UITextField!
    
    @IBOutlet weak var accountNumberLabel: UITextField!
    
    @IBOutlet weak var supplierLabel: UILabel!
    
    @IBOutlet weak var lampInstalledSwitch: UISwitch!
    
    @IBOutlet weak var remarksButton: UIButton!
    
    @IBOutlet weak var imagesStackView: UIStackView!
    
    @IBOutlet weak var imageOne: UIImageView!
    
    @IBOutlet weak var imageTwo: UIImageView!
    
    @IBOutlet weak var buttonsStack: UIStackView!
    
    @IBOutlet weak var fullScreenButton: UIButton!
    
    @IBOutlet weak var streetView: UIButton!
    
    @IBOutlet weak var panoramaView: UIButton!
    
    @IBOutlet weak var submitReportButton: UIButton!
    
    @IBOutlet weak var cityStackView: UIStackView!
    
    @IBOutlet weak var lampInstalledStatusLabel: UILabel!
    
    @IBOutlet weak var bottomOfStackView: NSLayoutConstraint!
    
    @IBOutlet var poleEditIcon: UIImageView!
    
    @IBOutlet var roadEditIcon: UIImageView!
    
    var isFullScreen = false
    var image_one:UIImage!
    var image_two:UIImage!
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    var record:Records!
    var isFromWorkRecord = false
    var isFromNewPole = false
    var isInstalled = 1
    var isStreetView = false
    var isFromOffline = false
    var pendingRecord:PoleDetails!
    fileprivate let apiConnector = RecordsAPIManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //mapView = GMSMapView(frame: mapView.frame)
        self.view.bringSubviewToFront(imagesStackView)
        //Image Roundcorner
        imageOne.layer.borderWidth = 1
        imageOne.layer.masksToBounds = false
        imageOne.layer.borderColor = UIColor.black.cgColor
        imageOne.layer.cornerRadius = imageOne.frame.height/2
        imageOne.clipsToBounds = true
        
        imageTwo.layer.borderWidth = 1
        imageTwo.layer.masksToBounds = false
        imageTwo.layer.borderColor = UIColor.black.cgColor
        imageTwo.layer.cornerRadius = imageOne.frame.height/2
        imageTwo.clipsToBounds = true
        
        //MAP INITIALIZATION
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        placesClient = GMSPlacesClient.shared()
        mapView.mapType = .satellite
        mapView.isMyLocationEnabled = true
        mapView.bringSubviewToFront(buttonsStack)
        
        
        let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(tapPhotoOne))
        imageOne.addGestureRecognizer(gestureRecognizerOne)
        
        let gestureRecognizerTwo = UITapGestureRecognizer(target: self, action: #selector(tapPhotoTwo))
        imageTwo.addGestureRecognizer(gestureRecognizerTwo)
        poleNumberLabel.isUserInteractionEnabled = false
        roadNameLabel.isUserInteractionEnabled = false
        
        
        if isFromWorkRecord{
            if Reachability.isConnectedToNetwork() {
                imageOne.image =  TBLConstants.setImageFromUrl(url: URL(string:  record.photo![0] as! String)!)
                imageTwo.image = TBLConstants.setImageFromUrl(url: URL(string:  record.photo![1] as! String)!)
            } else {
                imageOne.image = UIImage(named: "loading")
                imageTwo.image = UIImage(named: "loading")
            }
            
            poleNumberLabel.text = record.name!
            roadNameLabel.text = record.road_name!
            if let city = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.CITY) as? String){
                cityLabel.text = city
            }
            supplierLabel.text = record.supplier?.name!
            submitReportButton.isHidden = true
            lampInstalledSwitch.isHidden = true
            timeControlLocationLabel.text = record.control_time_location!
            timeControlLocationLabel.isUserInteractionEnabled = false
            if let mtr_no = record.meter_no{
                meterNumberLabel.text = mtr_no
            }
            
            meterNumberLabel.isUserInteractionEnabled = false
            accountNumberLabel.text = record.account_no!
            accountNumberLabel.isUserInteractionEnabled = false
            lattitudeLabel.text = "\(record.latitude!)"
            longitudeLabel.text = "\(record.longitude!)"
            cityStackView.isHidden = true
            if record.installed! == 0{
                lampInstalledStatusLabel.text = "No"
            }else{
                lampInstalledStatusLabel.text = "Yes"
            }
            bottomOfStackView.constant = 0
            
        }else if isFromNewPole{
            imageOne.image = image_one
            imageTwo.image = image_two
            poleNumberLabel.text = record.name!
            roadNameLabel.text = record.road_name!
            //            if let supplier = record.supplier?.name{
            //                supplierLabel.text   = supplier
            //            }else{
            supplierLabel.text   = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.SUPPLIER) as! String)
            //}
            if let timer_location = record.control_time_location{
                timeControlLocationLabel.text  = timer_location
            }else{
                timeControlLocationLabel.text  = ""
            }
            //timeControlLocationLabel.text = record.control_time_location!
            if let mtr_no = record.meter_no{
                meterNumberLabel.text = mtr_no
            }else{
                meterNumberLabel.text = ""
            }
            lampInstalledStatusLabel.isHidden = true
            if let acnt_no = record.account_no{
                accountNumberLabel.text = acnt_no
            }else{
                meterNumberLabel.text = ""
            }
            
            cityStackView.isHidden = true
            bottomOfStackView.constant = 64
            // showCurrentLocationOnMap()
        } else if isFromOffline{
            
            imageOne.image =  UIImage(data: pendingRecord.photo1!)
            imageTwo.image = UIImage(data: pendingRecord.photo2!)
            poleNumberLabel.text = pendingRecord.name!
            roadNameLabel.text = pendingRecord.road_name!
            if pendingRecord.isNew{
                poleNumberLabel.isUserInteractionEnabled = true
                roadNameLabel.isUserInteractionEnabled = true
            }else{
                poleNumberLabel.isUserInteractionEnabled = false
                roadNameLabel.isUserInteractionEnabled = false
            }
            
            poleEditIcon.isHidden = false
            roadEditIcon.isHidden = false
            cityStackView.isHidden = true
            supplierLabel.text = pendingRecord.supplier!
            submitReportButton.isHidden = false
            lampInstalledSwitch.isHidden = true
            timeControlLocationLabel.text = pendingRecord.control_time_location!
            timeControlLocationLabel.isUserInteractionEnabled = false
            meterNumberLabel.text = "\(pendingRecord.meter_no!)"
            meterNumberLabel.isUserInteractionEnabled = false
            accountNumberLabel.text = "\(String(describing: pendingRecord.account_no!))"
            accountNumberLabel.isUserInteractionEnabled = false
            lattitudeLabel.text = "\(pendingRecord.latitude)"
            longitudeLabel.text = "\(pendingRecord.longitude)"
            submitReportButton.setTitle("Update", for: .normal)
            cityStackView.isHidden = true
            if pendingRecord.installed == 0{
                lampInstalledStatusLabel.text = "No"
            }else{
                lampInstalledStatusLabel.text = "Yes"
            }
            bottomOfStackView.constant = 64
            //showCurrentLocationFromApi()
            
        }
        else {
            //Preset details
            
            imageOne.image = image_one
            imageTwo.image = image_two
            poleNumberLabel.text = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.POLE_NO) as! String)
            roadNameLabel.text = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ROAD_NAME) as! String)
            cityLabel.text = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.CITY) as! String)
            supplierLabel.text = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.SUPPLIER) as! String)
            submitReportButton.isHidden = false
            lampInstalledSwitch.isHidden = false
            lampInstalledStatusLabel.isHidden = true
            cityStackView.isHidden = false
            // showCurrentLocationOnMap()
            bottomOfStackView.constant = 64
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    @objc func tapPhotoOne(_ sender:AnyObject){
        
        viewPoleImage(image: imageOne.image!)
    }
    
    @objc func tapPhotoTwo(_ sender:AnyObject){
        
        viewPoleImage(image: imageTwo.image!)
    }
    
    func viewPoleImage(image:UIImage){
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "ViewPoleImageVC") as! ViewPoleImagesViewController
        cameraVC.poleImage = image
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    @IBAction func lampInstallStatusChange(_ sender: UISwitch) {
        if sender.isOn{
            isInstalled = 1
        }else{
            isInstalled = 0
        }
        
        
    }
    
    
    
    @IBAction func addRemarks(_ sender: Any) {
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "RemarksVC") as! RemarksViewController
        cameraVC.isFromWorkRecord = isFromWorkRecord
        cameraVC.coordinates = "\(lattitudeLabel.text!),\(String(describing: longitudeLabel.text!))"
        if isFromWorkRecord{
            if let remarks = record.remarks{
                cameraVC.remarks = remarks
            }
        }else if isFromOffline{
            cameraVC.remarks = pendingRecord.remarks!
            cameraVC.isFromOffline = true
        }
        
        self.navigationController?.pushViewController(cameraVC, animated: true)
        
    }
    
    
    @IBAction func switchFullScreen(_ sender: Any) {
        if !isFullScreen{
            isFullScreen = !isFullScreen
            submitReportButton.isHidden = true
            bottomOfStackView.constant = 0
            mapBottomView.isHidden = true
        }else{
            isFullScreen = !isFullScreen
            
            mapBottomView.isHidden = false
            if isFromWorkRecord{
                bottomOfStackView.constant = 0
                submitReportButton.isHidden = true
            }else{
                bottomOfStackView.constant = 64
                submitReportButton.isHidden = false
            }
            
        }
        
    }
    
    
    @IBAction func switchStreetView(_ sender: Any) {
        
        if !isStreetView {
            isStreetView = !isStreetView
            mapView.mapType = .normal
        }else{
            isStreetView = !isStreetView
            mapView.mapType = .satellite
        }
    }
    
    @IBAction func switchPanorama(_ sender: Any) {
        
        
    }
    
    
    func showCurrentLocationOnMap() {
        
        let camera = GMSCameraPosition.camera(withLatitude: (self.locationManager.location?.coordinate.latitude)!, longitude: (self.locationManager.location?.coordinate.longitude)!, zoom: 10.0)
        
        let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        mapView.isMyLocationEnabled = true
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "Current location"
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = mapView
        self.mapView = mapView
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        
    }
    
    
    func showCurrentLocationFromApi() {
        mapView = nil
        let myLatitute  = CLLocationDegrees(record.latitude!)
        let myLongitude  = CLLocationDegrees(record.longitude!)
        let coordinates = CLLocationCoordinate2DMake(myLatitute, myLongitude)
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 10.0)
        let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        mapView.isMyLocationEnabled = true
        lattitudeLabel.text = "\(Double(myLatitute))"
        longitudeLabel.text = "\(Double(myLongitude))"
        mapView.delegate = self
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "Current location"
        marker.appearAnimation = GMSMarkerAnimation.pop
        self.mapView = mapView
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        
        
    }
    
    @IBAction func submitReportAction(_ sender: Any) {
        
        if poleNumberLabel.text != "" && supplierLabel.text != "" && roadNameLabel.text != "" && timeControlLocationLabel.text != "" && meterNumberLabel.text != "" && accountNumberLabel.text != ""{
            
            TBLConstants.showActivityIndicator()
            var cityId:Int?
            var stateId:Int?
            var remark:String?
            if let city_id = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.CITY_ID) as? Int){
                cityId = city_id
            }
            if let state_id = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.STATE_ID) as? Int){
                stateId = state_id
            }
            if let remarks = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.REMARKS) as? String {
                remark = remarks
            }else{
                remark = ""
            }
            if isFromNewPole{
                if Reachability.isConnectedToNetwork() {
                    let _ = apiConnector.updatePoleDetails(reportID: NSNumber(value: record.id!),poleImage: image_one, lampImage: image_two, name: poleNumberLabel.text!, road_name: roadNameLabel.text!, city_id: 0, latitude: Double(lattitudeLabel.text!)!, longitude: Double(longitudeLabel.text!)!, remarks: remark!, installed: isInstalled, status: 1, new: 0, control_time_location: timeControlLocationLabel.text!, meter_no: meterNumberLabel.text!, account_no: accountNumberLabel.text!,phase : 0,supplier: supplierLabel.text!) { sucess,err  in
                        if sucess != nil{
                            self.showAlert()
                        }else{
                            TBLConstants.hideActivityIndicator()
                            self.showFailureAlert(title: "Error", message: (err?.error?.localizedDescription)!)
                            
                        }
                        
                        
                    }
                }else{
                    let records = CoreDatabaseModel.checkExistPendingRecord(name: poleNumberLabel.text!, roadName: roadNameLabel.text!, city_ID: 0, State_ID: record.state_id!)
                    if records!.count > 0{
                        showPendingRecordDuplicationAlert()
                    }else{
                        let _ = CoreDatabaseModel.savePoleDetails(poleImage: image_one, lampImage: image_two, name: poleNumberLabel.text!, road_name: roadNameLabel.text!, city_id: 0, latitude: Double(lattitudeLabel.text!)!, longitude: Double(longitudeLabel.text!)!, remarks: remark!, installed: isInstalled, status: 1, new: 0, control_time_location: timeControlLocationLabel.text!, meter_no: meterNumberLabel.text!, account_no: accountNumberLabel.text!, supplier: supplierLabel.text!, isNew: false, date: Date().format(), recordID: NSNumber(value: record.id!), state_id: record.state_id!) { (sucess) in
                            TBLConstants.hideActivityIndicator()
                            self.showPendingRecordAlert()
                        }
                        
                    }
                    
                    
                }
                
                
            }else if isFromOffline{
                
                
                let record = CoreDatabaseModel.checkExistRecord(name:pendingRecord.name!, roadName: pendingRecord.road_name!)!
                if record.count > 0{
                    record.first?.name = poleNumberLabel.text
                    record.first?.road_name = roadNameLabel.text
                    do {
                        try CoreDatabaseModel.getContext().save()
                        print("Saved")
                        TBLConstants.hideActivityIndicator()
                        self.navigationController?.popViewController(animated: true)
                    } catch {
                        
                        print("Failed saving")
                    }
                }else{
                    TBLConstants.hideActivityIndicator()
                    self.navigationController?.popViewController(animated: true)
                }
                
            }else{
                
                if Reachability.isConnectedToNetwork() {
                    let _ = apiConnector.createNewPole(poleImage: image_one, lampImage: image_two, name: poleNumberLabel.text!, road_name: roadNameLabel.text!, city_id: cityId!, latitude: Double(lattitudeLabel.text!)!, longitude: Double(longitudeLabel.text!)!, remarks: remark!, installed: isInstalled, status: 1, new: 1, control_time_location: timeControlLocationLabel.text!, meter_no: meterNumberLabel.text!, account_no: accountNumberLabel.text!,supplier: supplierLabel.text!) { sucess,user  in
                        self.showAlert()
                        
                    }
                }else{
                     let records = CoreDatabaseModel.checkExistPendingRecord(name: poleNumberLabel.text!, roadName: roadNameLabel.text!, city_ID: (cityId ?? nil)!, State_ID:stateId!)
                    if records!.count > 0{
                         showPendingRecordDuplicationAlert()
                    }else{
                        let _ = CoreDatabaseModel.savePoleDetails(poleImage: image_one, lampImage: image_two, name: poleNumberLabel.text!, road_name: roadNameLabel.text!, city_id: (cityId ?? nil)!, latitude: Double(lattitudeLabel.text!)!, longitude: Double(longitudeLabel.text!)!, remarks: remark!, installed: isInstalled, status: 1, new: 1, control_time_location: timeControlLocationLabel.text!, meter_no: (meterNumberLabel.text)!, account_no: (accountNumberLabel.text)!, supplier: supplierLabel.text!, isNew: true, date: Date().format(), recordID: 0, state_id: stateId!) { (sucess) in
                            TBLConstants.hideActivityIndicator()
                            self.showPendingRecordAlert()
                            
                            
                        }
                    }
                    
                    
                }
            }
            
            
        }else{
            if poleNumberLabel.text == ""{
                Toast(text: "Please insert pole number", delay: 0, duration: 1).show()
            }else if supplierLabel.text == ""{
                Toast(text: "Please insert supplier", delay: 0, duration: 1).show()
            }else if  roadNameLabel.text == ""{
                Toast(text: "Please insert road name", delay: 0, duration: 1).show()
            }else if timeControlLocationLabel.text == ""{
                Toast(text: "Please insert time control location", delay: 0, duration: 1).show()
            }else if meterNumberLabel.text == ""{
                Toast(text: "Please insert meter number", delay: 0, duration: 1).show()
            }else if accountNumberLabel.text == ""{
                Toast(text: "Please insert account number", delay: 0, duration: 1).show()
            }
            
        }
        
    }
    
    
    func showAlert(){
        TBLConstants.hideActivityIndicator()
        let alertController = UIAlertController(title: "", message: "Record has been submitted successfully", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let MainView = storyboard.instantiateViewController(withIdentifier: "SideMenu")
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.window!.rootViewController = MainView
            TBLConstants.TBL_Local_Data.set("", forKey: TBLConstants.Keys.REMARKS)
            
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func showPendingRecordAlert(){
        let alertController = UIAlertController(title: "NO INTERNET CONNECTION", message: "Record has been submitted in local", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            TBLConstants.TBL_Local_Data.set("", forKey: TBLConstants.Keys.REMARKS)
            self.sideMenuController?.setContentViewController(with: "\(3)", animated: Preferences.shared.enableTransitionAnimation)
            self.sideMenuController?.hideMenu()
            self.navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
                if vc.isKind(of: RoadNameViewController.self) || vc.isKind(of: PoleListViewController.self) || vc.isKind(of: AddNewPoleFromAPIViewController.self) || vc.isKind(of: PickPoleImageViewController.self) || vc.isKind(of: AddNewPoleViewController.self) || vc.isKind(of: AddNewPoleViewController.self) || vc.isKind(of: SubmitNewPoleViewController.self){
                    return true
                }
                else {
                    return false
                }
            })
            
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
}
extension SubmitNewPoleViewController {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        print("Location: \(location)")
        var camera:GMSCameraPosition!
        let marker = GMSMarker()
        mapView.clear()
        if isFromWorkRecord{
            mapView.isMyLocationEnabled = false
            let myLatitute  = CLLocationDegrees(record.latitude!)
            let myLongitude  = CLLocationDegrees(record.longitude!)
            let coordinates = CLLocationCoordinate2DMake(myLatitute, myLongitude)
            lattitudeLabel.text = "\(Double(coordinates.latitude).rounded(digits: 4))"
            longitudeLabel.text = "\(Double(coordinates.longitude).rounded(digits: 4))"
            camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude,
                                              longitude: coordinates.longitude,
                                              zoom: zoomLevel)
            marker.position = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
        }else{
            lattitudeLabel.text = "\(Double(location.coordinate.latitude).rounded(digits: 4))"
            longitudeLabel.text = "\(Double(location.coordinate.longitude).rounded(digits: 4))"
            camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
            marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        }
        
        //accurateLabel.text = "Accurate to \(Int(location.verticalAccuracy)) meters"
        
        
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        marker.icon = #imageLiteral(resourceName: "marker")
        marker.tracksViewChanges = false
        marker.map = mapView
        
    }
    func showPendingRecordDuplicationAlert(){
      TBLConstants.hideActivityIndicator()
        poleNumberLabel.isUserInteractionEnabled = true
        roadNameLabel.isUserInteractionEnabled = true
        let alertController = UIAlertController(title: "Pole Duplication Issue", message: "Found same pole in the Local database.\nPlease rename this pole to submit.", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
          alertController.dismiss(animated: true, completion: nil)
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func showFailureAlert(title:String,message:String){
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
            UIAlertAction in
            //               let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            //               let MainView = storyboard.instantiateViewController(withIdentifier: "SideMenu")
            //               let appdelegate = UIApplication.shared.delegate as! AppDelegate
            //               appdelegate.window!.rootViewController = MainView
            alertController.dismiss(animated: true, completion: nil)
            TBLConstants.TBL_Local_Data.set("", forKey: TBLConstants.Keys.REMARKS)
            TBLConstants.hideActivityIndicator()
        }
        
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
}
extension String {
    
    var numberValue:NSNumber? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)
    }
}
