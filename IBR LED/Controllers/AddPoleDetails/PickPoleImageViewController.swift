//
//  PickPoleImageViewController.swift
//  IBR LED
//
//  Created by BSSADM on 03/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import Toaster
class PickPoleImageViewController: UIViewController {

    @IBOutlet weak var poleImageView: UIImageView!
    
    @IBOutlet weak var lampImageView: UIImageView!
    
    @IBOutlet weak var poleCameraButton: UIButton!
    
    @IBOutlet weak var poleGalleryButton: UIButton!
    
    @IBOutlet weak var lampCameraButton: UIButton!
    
    @IBOutlet weak var lampGalleryButton: UIButton!
    
    var record:Records!
    var isFromNewPole = false
    var isPole = false
   
    override func viewDidLoad() {
        super.viewDidLoad()

        poleImageView.layer.borderWidth = 1.0
        poleImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lampImageView.layer.borderWidth = 1.0
        lampImageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
      
    }
    
    // Button Actions
    
    //Pole Number Photo From Camera
    
    @IBAction func pickPoleNumberPhoto(_ sender: Any) {
        isPole = true
        openCamera()
    }
    
    //Pole Number Image From Gallery
    
    @IBAction func pickPoleImageFromGallery(_ sender: Any) {
         isPole = true
        openGallery()
    }
    
    //Lamp Photo From Camera
    
    @IBAction func lampPhoto(_ sender: Any) {
         isPole = false
         openCamera()
    }
    
    //Lamp Image From Gallery
    
    @IBAction func picklampImageFromGallery(_ sender: Any) {
        isPole = false
        openGallery()
    }
    
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func doneAction(_ sender: Any) {
        if poleImageView.image != nil && lampImageView.image != nil{
            let _ = TBLConstants.saveImage(image: poleImageView.image!, name: "pole.jpg")
            let _ = TBLConstants.saveImage(image: lampImageView.image!, name: "lamp.jpg")
            let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "SubmitNewPoleVC") as! SubmitNewPoleViewController
            cameraVC.image_one = poleImageView.image
            cameraVC.image_two = lampImageView.image
            cameraVC.record = record
            cameraVC.isFromNewPole = isFromNewPole
            self.navigationController?.pushViewController(cameraVC, animated: true)
        }else{
            Toast(text: "Please pick images to proceed", delay: 0, duration: 1).show()
        }
       
        
    }
    
    
    
    
    
    

}

extension PickPoleImageViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK:-- ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imgUrl = info[.imageURL] as? URL{
            
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSTemporaryDirectory()
            let localPath = documentDirectory.appending(imgName)
            
            print(localPath)
           
        }
        if let pickedImage = info[.originalImage] as? UIImage {
             poleImageView.contentMode = .scaleToFill
            if isPole{
               poleImageView.image = pickedImage
            }else{
              lampImageView.image = pickedImage
            }
           
            //pickedImage.saveToDocuments(filename: imgName)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
