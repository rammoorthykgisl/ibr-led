//
//  AddNewPoleFromAPIViewController.swift
//  IBR LED
//
//  Created by BSSADM on 06/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Toaster
class AddNewPoleFromAPIViewController: UIViewController,GMSMapViewDelegate,UITextFieldDelegate,PopUpListViewDelegate {
    
    
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var mapBottomView: UIView!
    
    
    @IBOutlet weak var poleNameLabel: UITextField!
    @IBOutlet weak var accurateLabel: UIPaddedLabel!
    
    @IBOutlet weak var lattitudeLabel: UIPaddedLabel!
    
    @IBOutlet weak var longitudeLabel: UILabel!
    
    @IBOutlet weak var supplierLabel: UITextField!
    
    @IBOutlet weak var roadNameLabel: UITextField!
    
    @IBOutlet weak var buttonStack: UIStackView!
    
    
    var isFullScreen = false
    var record:Records!
    var poleName = ""
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    var isStreetView = false
    var popUpView:PopUpListView?
    var suppliers = [Supplier]()
    var accuracy = 0
    fileprivate let apiConnector = RecordsAPIManager()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        poleNameLabel.text = poleName
        if record.supplier?.name! == nil || record.supplier?.name! == ""{
            supplierLabel.text = "Choose Supplier"
        }else{
            supplierLabel.text = record.supplier?.name!
        }
        
        roadNameLabel.text = record.road_name!
        
        
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        mapView.isMyLocationEnabled = true
        mapView.mapType = .satellite
        mapView.bringSubviewToFront(buttonStack)
        
        supplierLabel.addTarget(self, action:  #selector(handleTaponTextField(_:)), for: .touchDown)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTaponTextField(_ sender: UITextField)
    {
        sender.isUserInteractionEnabled = false
        
        let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.SUPPLIERS)
        if data != nil{
            if #available(iOS 12.0, *) {
                guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                    else {
                        return
                }
                let dict = (unarchivedRecords as! [String:Any])
                let suppliers = dict["data"] as! [[String:Any]]
                for supplier in suppliers{
                    self.suppliers.append(Supplier.init(JSON: supplier)!)
                }
                self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                self.popUpView?.suppliers = self.suppliers
                self.popUpView?.titleLabel.text = "Choose Supplier"
                self.popUpView?.delegate = self
                self.popUpView?.type = 0
                self.view.addSubview(self.popUpView!)
                self.view.bringSubviewToFront(self.popUpView!)
                
            }
        }else{
            TBLConstants.showActivityIndicator()
            let _ = apiConnector.getSuppliers { (suppliers, error) in
                if suppliers != nil{
                               if #available(iOS 12.0, *) {
                        guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                            else {
                                return
                        }
                        let dict = (unarchivedRecords as! [String:Any])
                        let suppliers = dict["data"] as! [[String:Any]]
                        for supplier in suppliers{
                            self.suppliers.append(Supplier.init(JSON: supplier)!)
                        }
                        self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                        self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                        self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                        self.popUpView?.suppliers = self.suppliers
                        self.popUpView?.titleLabel.text = "Choose Supplier"
                        self.popUpView?.delegate = self
                        self.popUpView?.type = 0
                        self.view.addSubview(self.popUpView!)
                        self.view.bringSubviewToFront(self.popUpView!)
                        
                    }
                }else{
                    TBLConstants.hideActivityIndicator()
                }
            }
        }
        
        
        
        
        
        
        
        

        
    }
    
    func clickFromPopUpList(name: String, type: Int) {
        supplierLabel.text = name
        supplierLabel.isUserInteractionEnabled = true
        TBLConstants.TBL_Local_Data.set(name, forKey: TBLConstants.Keys.SUPPLIER)
        self.popUpView?.removeFromSuperview()
    }
   
    func viewPoleImage(image:UIImage){
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "ViewPoleImageVC") as! ViewPoleImagesViewController
        
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    @IBAction func switchFullScreen(_ sender: Any) {
        if !isFullScreen{
            isFullScreen = !isFullScreen
            mapBottomView.isHidden = true
        }else{
            isFullScreen = !isFullScreen
            mapBottomView.isHidden = false
        }
        
    }
    
    
    @IBAction func switchStreetView(_ sender: Any) {
        if !isStreetView {
            isStreetView = !isStreetView
            mapView.mapType = .normal
        }else{
            isStreetView = !isStreetView
            mapView.mapType = .satellite
        }
    }
    
    
    @IBAction func switchPanorama(_ sender: Any) {
        
        
    }
    
    
    
    @IBAction func insertPhotoAction(_ sender: Any) {
        if supplierLabel.text == "Choose Supplier"{
             Toast(text: "Please choose supplier", delay: 0, duration: 1).show()
        }else{
            if accuracy > 10{
                
                // Create the alert controller
                let alertController = UIAlertController(title: "Warning", message: "Inaccurate location.\nPlease wait until accuracy is below 10 meters.", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    
                    alertController.dismiss(animated: true, completion: nil)
                }
                // Add the actions
                alertController.addAction(okAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                savePoleDetailsInLocal()
                       if Reachability.isConnectedToNetwork(){
                           let _ = apiConnector.checkRecordExist(location_id: record.id!, name: record.name!, road_name: record.road_name!, city_id: record.city_id!, control_time_location: "", state_id: record.state_id!) { (sucess, error) in
                              if sucess?.email_message == "Pole already exist!"{
                                   self.showAlert()
                               }else{
                                     let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as! PickPoleImageViewController
                                     cameraVC.record = self.record
                                     cameraVC.isFromNewPole = true
                                     self.navigationController?.pushViewController(cameraVC, animated: true)
                                   
                               }
                               
                               
                           }
                           
                           
                           
                       }else{
                           let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as! PickPoleImageViewController
                                      cameraVC.record = self.record
                                      cameraVC.isFromNewPole = true
                                      self.navigationController?.pushViewController(cameraVC, animated: true)
                                      
                       }
                }
        }

        
       

        
}
    
   
    func textFieldDidBeginEditing(_ textField: UITextField){
        //show popup here
    }
    
    func showAlert(){
          TBLConstants.hideActivityIndicator()
             let alertController = UIAlertController(title: "Pole Duplication Issue", message: "Found same pole in the database.\nPlease rename this pole to submit.", preferredStyle: .alert)
             
             // Create the actions
             let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
                 UIAlertAction in
               alertController.dismiss(animated: true, completion: nil)
             }
             
             // Add the actions
             alertController.addAction(okAction)
             // Present the controller
             self.present(alertController, animated: true, completion: nil)
         }
       
    
    func savePoleDetailsInLocal(){
        TBLConstants.TBL_Local_Data.set(poleNameLabel.text, forKey: TBLConstants.Keys.POLE_NO)
        TBLConstants.TBL_Local_Data.set(supplierLabel.text, forKey: TBLConstants.Keys.SUPPLIER)
        TBLConstants.TBL_Local_Data.set(roadNameLabel.text, forKey: TBLConstants.Keys.ROAD_NAME)
        TBLConstants.TBL_Local_Data.synchronize()
    }
    
    
}

extension AddNewPoleFromAPIViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        self.accuracy = Int(location.verticalAccuracy)
        mapView.clear()
        lattitudeLabel.text = "\(Double(location.coordinate.latitude).rounded(digits: 4))"
        longitudeLabel.text = "\(Double(location.coordinate.longitude).rounded(digits: 4))"
        accurateLabel.text = "Accurate to \(Int(location.verticalAccuracy)) meters"
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        //marker.title = "Sydney"
        marker.tracksViewChanges = false
        marker.icon = #imageLiteral(resourceName: "marker")
       // marker.snippet = "Australia"
        marker.map = mapView
        
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    
}
