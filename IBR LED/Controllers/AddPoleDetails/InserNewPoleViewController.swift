//
//  InserNewPoleViewController.swift
//  IBR LED
//
//  Created by BSSADM on 03/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit

class InserNewPoleViewController: UIViewController {

    @IBOutlet weak var lattitudeTextLabel: UILabel!
    
    
    @IBOutlet weak var longitudeTextLabel: UILabel!
    
    
    @IBOutlet weak var supplierTextLabel: UILabel!
    
    
    @IBOutlet weak var roadNameLabel: UILabel!
    
   
    @IBOutlet weak var lampPostName: UILabel!
    
    @IBOutlet weak var accuracyLabel: UILabel!
    
     
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func insertPhoto(_ sender: Any) {
        
        let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as! PickPoleImageViewController
        self.navigationController?.pushViewController(cameraVC, animated: true)
        
    }
    
    

}
