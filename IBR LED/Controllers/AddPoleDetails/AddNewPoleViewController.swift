//
//  AddNewPoleViewController.swift
//  IBR LED
//
//  Created by BSSADM on 03/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Toaster
class AddNewPoleViewController: UIViewController,PopUpListViewDelegate,GMSMapViewDelegate {
    
    
    @IBOutlet weak var inserPoleNumberTextField: UITextField!
    
    @IBOutlet weak var AccuracyTextLabel: UILabel!
    
    @IBOutlet weak var lattitudeTextLabel: UILabel!
    
    @IBOutlet weak var longitudeTextLabel: UILabel!
    
    @IBOutlet weak var insertRoadName: UITextField!
    
    @IBOutlet weak var supplierButton: UIButton!
    
    @IBOutlet weak var chooseStateButton: UIButton!
    
    @IBOutlet weak var chooseCityButton: UIButton!
    
    @IBOutlet  var mapView: GMSMapView!
    
    @IBOutlet weak var buttonsStack: UIStackView!
    @IBOutlet weak var mapBottomView: UIView!
    @IBOutlet weak var fullScreenButton: UIButton!
    
    
    @IBOutlet weak var streetView: UIButton!
    
    @IBOutlet weak var panoramaView: UIButton!
    
    var popUpView:PopUpListView?
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    var isFullScreen = false
    var isStreetView = false
    var supplier = [Supplier]()
    var states = [State]()
    var city = [City]()
    var accuracy = 0
    fileprivate let apiConnector = RecordsAPIManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        mapView.isMyLocationEnabled = true
        mapView.mapType = .satellite
        mapView.bringSubviewToFront(buttonsStack)
        
    }
    
    
    
    @IBAction func selectSuppliers(_ sender: Any) {
        
        
        let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.SUPPLIERS)
        if data != nil{
            if #available(iOS 12.0, *) {
                guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                    else {
                        return
                }
                let dict = (unarchivedRecords as! [String:Any])
                let suppliers = dict["data"] as! [[String:Any]]
                for supplier in suppliers{
                    if self.supplier.contains(where: {$0.supplier == supplier["supplier"] as? String}){
                        
                    }else{
                         self.supplier.append(Supplier.init(JSON: supplier)!)
                    }
                  
                }
                self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                self.popUpView?.suppliers = self.supplier
                self.popUpView?.titleLabel.text = "Choose Supplier"
                self.popUpView?.delegate = self
                self.popUpView?.type = 0
                self.view.addSubview(self.popUpView!)
                self.view.bringSubviewToFront(self.popUpView!)
                
            } else {
             if let unarchivedRecords = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [Supplier] {
                    //self.supplier = unarchivedRecords
                }
             }
        }else{
            TBLConstants.showActivityIndicator()
            let _ = apiConnector.getSuppliers { (suppliers, error) in
                if suppliers != nil{
                    self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                    self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                    self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                    self.popUpView?.suppliers = (suppliers?.supplier)!
                    self.popUpView?.titleLabel.text = "Choose Supplier"
                    self.popUpView?.delegate = self
                    self.popUpView?.type = 0
                    self.view.addSubview(self.popUpView!)
                    self.view.bringSubviewToFront(self.popUpView!)
                    TBLConstants.hideActivityIndicator()
                }else{
                    TBLConstants.hideActivityIndicator()
                }
            }
        }
        
        
        
        
        
        

        
    }
    
    
    @IBAction func chooseState(_ sender: Any) {
        let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.STATES)
        chooseCityButton.setTitle("Choose City", for: .normal)
        chooseStateButton.setTitleColor(.gray, for: .normal)
        chooseCityButton.isUserInteractionEnabled = false
        if data != nil{
            if #available(iOS 12.0, *) {
                guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                    else {
                        return
                }
                let dict = (unarchivedRecords as! [String:Any])
                let states = dict["data"] as! [[String:Any]]
                for state in states{
                    if self.states.contains(where: {$0.name == state["name"] as? String}){
                        
                    }else{
                        self.states.append(State.init(JSON:state)!)
                    }
                  
                }
               self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                self.popUpView?.states = self.states
                self.popUpView?.type = 1
                self.popUpView?.delegate = self
                self.popUpView?.titleLabel.text = "Choose State"
                self.view.addSubview(self.popUpView!)
                self.view.bringSubviewToFront(self.popUpView!)
                
            }
        }else{
            TBLConstants.showActivityIndicator()
            let _ = apiConnector.getStates { (states, error) in
                if states != nil{
                    self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                    self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                    self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                    self.popUpView?.states = (states?.states)!
                    self.popUpView?.type = 1
                    self.popUpView?.delegate = self
                    self.popUpView?.titleLabel.text = "Choose State"
                    self.view.addSubview(self.popUpView!)
                    self.view.bringSubviewToFront(self.popUpView!)
                    TBLConstants.hideActivityIndicator()
                }else{
                    TBLConstants.hideActivityIndicator()
                }
            }
        }

        
    }
    
    
    
    @IBAction func chooseCity(_ sender: Any) {
        if chooseStateButton.titleLabel?.text == "Choose State"{
            Toast(text: "Please choose state first", delay: 0, duration: 1).show()
        }else{
            let state_ID =  TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.STATE_ID) as! Int
            var filtered_cities = [City]()
            let data = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.CITIES)
            if data != nil{
                if #available(iOS 12.0, *) {
                    guard let unarchivedRecords = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data as! Data)
                        else {
                            return
                    }
                    let dict = (unarchivedRecords as! [String:Any])
                    let cities = dict["data"] as! [[String:Any]]
                    for cityObj in cities{
                        self.city.append(City.init(JSON:cityObj)!)
                    }
                 self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                 self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                 self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                 
                    for city in self.city{
                     if city.state_id == state_ID{
                        if filtered_cities.contains(where: {$0.name == city.name}){
                          
                        }else{
                           filtered_cities.append(city)
                        }
                        
                     }
                 }
                 
                 
                
                 self.popUpView?.cities = filtered_cities
                 self.popUpView?.type = 2
                 self.popUpView?.delegate = self
                 self.popUpView?.titleLabel.text = "Choose City"
                 self.view.addSubview(self.popUpView!)
                 self.view.bringSubviewToFront(self.popUpView!)
                 TBLConstants.hideActivityIndicator()
                    
                }
            }else{
                 TBLConstants.showActivityIndicator()
                
                 let _ = apiConnector.getCities { (cities, error) in
                     if cities != nil{
                         self.popUpView = (Bundle.main.loadNibNamed("PopupListView", owner: self, options:nil)?.first as! PopUpListView)
                         self.popUpView?.frame = CGRect(x: 24,  y: 48, width: self.view.bounds.size.width - 48, height: self.view.bounds.size.height - 120)
                         self.popUpView?.autoresizingMask = [.flexibleWidth,.flexibleHeight]
                         
                        for city in (cities?.cities)!{
                            if city.state_id == state_ID{
                                if filtered_cities.contains(where: {$0.name == city.name}){
                                    
                                }else{
                                    filtered_cities.append(city)
                                }
                                
                            }
                        }
                         
                         
                         
                         self.popUpView?.cities = filtered_cities
                         self.popUpView?.type = 2
                         self.popUpView?.delegate = self
                         self.popUpView?.titleLabel.text = "Choose City"
                         self.view.addSubview(self.popUpView!)
                         self.view.bringSubviewToFront(self.popUpView!)
                         TBLConstants.hideActivityIndicator()
                     }else{
                         TBLConstants.hideActivityIndicator()
                     }
                 }
            }
        }
        
        
        
    }
    
    @IBAction func insertPhoto(_ sender: Any) {
        
        if accuracy > 10{
              // Create the alert controller
                       let alertController = UIAlertController(title: "Warning", message: "Inaccurate location.\nPlease wait until accuracy is below 10 meters.", preferredStyle: .alert)
                       
                       // Create the actions
                       let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                           UIAlertAction in
                           
                           alertController.dismiss(animated: true, completion: nil)
                       }
                       // Add the actions
                       alertController.addAction(okAction)
                       
                       // Present the controller
                       self.present(alertController, animated: true, completion: nil)
        }else{
            if Reachability.isConnectedToNetwork(){
                if inserPoleNumberTextField.text != "" && supplierButton.titleLabel?.text != "Supplier" && insertRoadName.text != "" && chooseCityButton.titleLabel?.text != "Choose City" && chooseStateButton.titleLabel?.text != "Choose State"{
                    savePoleDetailsInLocal()
                    var cityId = 0
                    var stateId = 0
                    if let city_id = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.CITY_ID) as? Int){
                        cityId = city_id
                    }
                    if let state_id = (TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.STATE_ID) as? Int){
                                    stateId = state_id
                                   }
                    let _ = apiConnector.checkRecordExist(location_id:nil, name: inserPoleNumberTextField.text!, road_name: insertRoadName.text!, city_id: cityId, control_time_location:"", state_id: stateId) { (sucess, error) in
                        if sucess?.email_message == "Pole already exist!"{
                            self.showAlert()
                        }else{
                          let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as! PickPoleImageViewController
                           cameraVC.isFromNewPole = false
                           self.navigationController?.pushViewController(cameraVC, animated: true)
                            
                        }
                        
                        
                    }
                    
                }else{
                    if inserPoleNumberTextField.text == ""{
                        Toast(text: "Please insert pole number", delay: 0, duration: 1).show()
                    }else if supplierButton.titleLabel?.text == "Supplier"{
                        Toast(text: "Please choose supplier", delay: 0, duration: 1).show()
                    }else if  insertRoadName.text == ""{
                        Toast(text: "Please insert road name", delay: 0, duration: 1).show()
                    }else if chooseCityButton.titleLabel?.text == "Choose City"{
                        Toast(text: "Please choose city", delay: 0, duration: 1).show()
                    }else if chooseStateButton.titleLabel?.text == "Choose State"{
                        Toast(text: "Please choose state", delay: 0, duration: 1).show()
                    }
                    
                }

            }else{
                if inserPoleNumberTextField.text != "" && supplierButton.titleLabel?.text != "Supplier" && insertRoadName.text != "" && chooseCityButton.titleLabel?.text != "Choose City" && chooseStateButton.titleLabel?.text != "Choose State"{
                    savePoleDetailsInLocal()
                    let cameraVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "CameraVC") as! PickPoleImageViewController
                    cameraVC.isFromNewPole = false
                    self.navigationController?.pushViewController(cameraVC, animated: true)
                }else{
                   if inserPoleNumberTextField.text == ""{
                        Toast(text: "Please insert pole number", delay: 0, duration: 1).show()
                    }else if supplierButton.titleLabel?.text == "Supplier"{
                        Toast(text: "Please choose supplier", delay: 0, duration: 1).show()
                    }else if  insertRoadName.text == ""{
                        Toast(text: "Please insert road name", delay: 0, duration: 1).show()
                    }else if chooseCityButton.titleLabel?.text == "Choose City"{
                        Toast(text: "Please choose city", delay: 0, duration: 1).show()
                    }else if chooseStateButton.titleLabel?.text == "Choose State"{
                        Toast(text: "Please choose state", delay: 0, duration: 1).show()
                    }
                }
            }
        }

    }
    
    func showAlert(){
        TBLConstants.hideActivityIndicator()
          let alertController = UIAlertController(title: "Pole Duplication Issue", message: "Found same pole in the database.\nPlease rename this pole to submit.", preferredStyle: .alert)
          
          // Create the actions
          let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) {
              UIAlertAction in
            alertController.dismiss(animated: true, completion: nil)
          }
          
          // Add the actions
          alertController.addAction(okAction)
          // Present the controller
          self.present(alertController, animated: true, completion: nil)
      }
    
    
    func clickFromPopUpList(name: String, type: Int) {
        if type == 0{
            supplierButton.setTitle(name, for: .normal)
            supplierButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }else if type == 1{
            chooseCityButton.isUserInteractionEnabled = true
            chooseStateButton.setTitle(name, for: .normal)
            chooseStateButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }else{
            chooseCityButton.setTitle(name, for: .normal)
            chooseCityButton.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }
        self.popUpView?.removeFromSuperview()
    }
    
    @IBAction func switchFullScreen(_ sender: Any) {
        if !isFullScreen{
            isFullScreen = !isFullScreen
            mapBottomView.isHidden = true
        }else{
            isFullScreen = !isFullScreen
            mapBottomView.isHidden = false
        }
        
    }
    
    
    @IBAction func switchStreetView(_ sender: Any) {
        if !isStreetView {
            isStreetView = !isStreetView
            mapView.mapType = .normal
        }else{
            isStreetView = !isStreetView
            mapView.mapType = .satellite
        }
        
    }
    
    @IBAction func switchPanorama(_ sender: Any) {
        
        
    }
    
    func savePoleDetailsInLocal(){
        TBLConstants.TBL_Local_Data.set(inserPoleNumberTextField.text, forKey: TBLConstants.Keys.POLE_NO)
        TBLConstants.TBL_Local_Data.set(supplierButton.titleLabel?.text, forKey: TBLConstants.Keys.SUPPLIER)
        TBLConstants.TBL_Local_Data.set(insertRoadName.text, forKey: TBLConstants.Keys.ROAD_NAME)
        TBLConstants.TBL_Local_Data.set(chooseCityButton.titleLabel?.text, forKey: TBLConstants.Keys.CITY)
        TBLConstants.TBL_Local_Data.set(chooseStateButton.titleLabel?.text, forKey: TBLConstants.Keys.STATE)
        TBLConstants.TBL_Local_Data.synchronize()
    }
    
    
    
}
extension AddNewPoleViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        mapView.clear()
        self.accuracy = Int(location.verticalAccuracy)
        lattitudeTextLabel.text = "\(Double(location.coordinate.latitude).rounded(digits: 4))"
        longitudeTextLabel.text = "\(Double(location.coordinate.longitude).rounded(digits: 4))"
        AccuracyTextLabel.text = "Accurate to \(Int(location.verticalAccuracy)) meters"
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
        // Creates a marker in the center of the map.
        
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let marker = GMSMarker()
        marker.position = center
        marker.tracksViewChanges = false
        marker.icon = #imageLiteral(resourceName: "marker")
        marker.map = mapView
        self.view.layoutIfNeeded()
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
   
    
}

extension Double {
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
}
