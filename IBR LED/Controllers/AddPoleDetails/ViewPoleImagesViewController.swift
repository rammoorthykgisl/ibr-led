//
//  ViewPoleImagesViewController.swift
//  IBR LED
//
//  Created by BSSADM on 13/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit

class ViewPoleImagesViewController: UIViewController {
   
    @IBOutlet var poleImageView: UIImageView!
    var poleImage:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       poleImageView.image = poleImage
        // Do any additional setup after loading the view.
    }
    
}
