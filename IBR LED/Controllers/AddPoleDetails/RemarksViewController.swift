//
//  RemarksViewController.swift
//  IBR LED
//
//  Created by BSSADM on 05/09/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import Toaster

class RemarksViewController: UIViewController {

    @IBOutlet weak var remarksTextView: UITextView!
    
    @IBOutlet weak var saveRemarksButton: UIButton!
    
    @IBOutlet var coOrdinatesLabel: UILabel!
    
    @IBOutlet var noteStatusLabel: UILabel!
  
    @IBOutlet var noteButton: UIButton!
    
    @IBOutlet var lainLainLsuTextLabel: UILabel!
    
    
    var isFromWorkRecord = false
    var isFromOffline = false
    var remarks = ""
    var coordinates = ""
    var isClick = true
    override func viewDidLoad() {
        super.viewDidLoad()
        coOrdinatesLabel.text = coordinates
        noteStatusLabel.layer.borderWidth = 1
        noteStatusLabel.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        
        if isFromWorkRecord || isFromOffline{
            remarksTextView.text = remarks
            noteStatusLabel.isHidden = true
            noteButton.isHidden = true
            coOrdinatesLabel.isHidden = true
            saveRemarksButton.isHidden = true
            lainLainLsuTextLabel.isHidden = true
        }else{
            if let remarks = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.REMARKS) as? String {
                remarksTextView.text = remarks
            }
        }
       
       
    }
    
    
    @IBAction func noteStatusChangeAction(_ sender: Any) {
        
        if isClick{
            isClick = !isClick
            TBLConstants.TBL_Local_Data.set(true, forKey: TBLConstants.Keys.ISNOTE)
            noteStatusLabel.text = "Nombor Tiang Salah"
            noteButton.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            if isFromWorkRecord{
               remarksTextView.text = "Issue(s): Nombor Tiang Salah\nOther : " + remarks
            }else{
                if let remarks = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.REMARKS) as? String {
                    remarksTextView.text = "Issue(s): Nombor Tiang Salah\nOther : " + remarks
                }
            }
          
        }else{
            isClick = !isClick
             TBLConstants.TBL_Local_Data.set(false, forKey: TBLConstants.Keys.ISNOTE)
            noteStatusLabel.text = "None"
            noteButton.backgroundColor = #colorLiteral(red: 0.09019607843, green: 0.2039215686, blue: 0.6431372549, alpha: 1)
            remarksTextView.text = ""
        }
        
        
    }
    
    
    
    @IBAction func saveRemarks(_ sender: Any) {
       
            if remarksTextView.text != ""{
                TBLConstants.TBL_Local_Data.set(remarksTextView.text, forKey: TBLConstants.Keys.REMARKS)
                Toast(text: "remarks Saved Sucessfully", delay: 0, duration: 1).show()
            }else{
                Toast(text: "Please type remarks/issues...", delay: 0, duration: 1).show()
            }
       
        self.navigationController?.popViewController(animated: true)
        }
    

}
