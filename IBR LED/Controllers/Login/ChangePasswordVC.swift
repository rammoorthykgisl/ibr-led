//
//  ChangePasswordVC.swift
//  IBR LED
//
//  Created by BSSADM on 28/08/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import UIKit
import Alamofire
import Toaster
class ChangePasswordVC: UIViewController {
    
    fileprivate let apiConnector = LogInAPIManager()
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var newPasswordField: UITextField!
    
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    @IBOutlet weak var changePswdButton: UIButton!
    
    @IBOutlet var newPasswordHide: UIButton!
    
    @IBOutlet var confirmPasswordHide: UIButton!
    
    
    var isNewPSWD = false
    var isConfirmPSWD = false
       
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
       // let email = TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.EMAIL) as! String
        getTokenFromEmail(email:emailTextField.text!) { (sucess, users) in
            self.changePassword(token: (users?.email_token!)!)
        }
    }
    
    
    @IBAction func hidePassword(_ sender: UIButton) {

                   if sender.tag == 0{
                   if !isNewPSWD{
                       isNewPSWD = !isNewPSWD
                       newPasswordField.isSecureTextEntry = false
                       newPasswordHide.setImage(#imageLiteral(resourceName: "hide-password"), for: .normal)
                   }else{
                       newPasswordHide.setImage(#imageLiteral(resourceName: "show-password"), for: .normal)
                       newPasswordField.isSecureTextEntry = true
                       isNewPSWD = !isNewPSWD
                   }
               }else if sender.tag == 1{
                   if !isConfirmPSWD{
                       isConfirmPSWD = !isConfirmPSWD
                       confirmPasswordField.isSecureTextEntry = false
                       confirmPasswordHide.setImage(#imageLiteral(resourceName: "hide-password"), for: .normal)
                   }else{
                       confirmPasswordHide.setImage(#imageLiteral(resourceName: "show-password"), for: .normal)
                       confirmPasswordField.isSecureTextEntry = true
                       isConfirmPSWD = !isConfirmPSWD
                   }
               }
        
    }
    
    
    
    
    
    
    //GET TOKEN FROM EMAIL FOR CHANGE PASSWORD
    func getTokenFromEmail(email:String,completion:@escaping (_ success:Bool,_ users:Users? ) -> Void){
        let _ = apiConnector.getTokenFromEmail(email: email) { (user, error) in
            if user == nil{
                TBLConstants.alertMessageForApiErrorTypes(viewController: self, storyBoardName: "Main", ErrorTypeResponse: error)
                completion(false,nil)
            }else{
                completion(true,user!)
            }
        }
    }
    
    func changePassword(token:String){
           
           if newPasswordField.text != "" && newPasswordField.text != "" {
               
               if newPasswordField.text == confirmPasswordField.text{
                   
                   
                   let url = BaseService.API_URL + "password/reset"
                   var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
                   defaultHeaders["Content-Type"] = "application/x-www-form-urlencoded"
                   defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
                   defaultHeaders["Accept"] =  "application/json"
                let param = ["password":newPasswordField.text!, "password_confirmation":confirmPasswordField.text!,"email":emailTextField.text!,"token":token]
                   Alamofire.request(url, method: .post, parameters:param ,encoding: URLEncoding.default, headers: defaultHeaders).responseJSON {
                       response in
                       switch response.result {
                       case .success:
                           print(response)
                           let jsonResponse = response.value as! [String:Any]
                           let message = jsonResponse["message"] as? String
                           
                           
                           if message != nil{
                               // Create the alert controller
                               let alertController = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
                               
                               // Create the actions
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                   UIAlertAction in
                                   
                                   TBLConstants.TBL_Local_Data.set(nil, forKey: TBLConstants.Keys.ACCESS_TOKEN)
//                                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                   let MainView = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//                                   let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                                   appdelegate.window!.rootViewController = MainView
                                self.navigationController?.popToRootViewController(animated: true)
                               }
                               // Add the actions
                               alertController.addAction(okAction)
                               
                               // Present the controller
                               self.present(alertController, animated: true, completion: nil)
                           }else{
                               // Create the alert controller
                               let error = jsonResponse["error"] as! String
                               let alertController = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
                               
                               // Create the actions
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                   UIAlertAction in
                                   
                                   alertController.dismiss(animated: true, completion: nil)
                               }
                               alertController.addAction(okAction)
                               
                               // Present the controller
                               self.present(alertController, animated: true, completion: nil)
                           }
                           
                       case .failure(let error):
                           
                           print(error)
                       }
                   }
                   
               }else{
                   Toast(text: "New password and Confirm password is mismatched!", delay: 0, duration: 1).show()
                   
               }
               
           }else{
               if newPasswordField.text == ""{
                   Toast(text: "Please enter new password!", delay: 0, duration: 1).show()
                   
               }else if confirmPasswordField.text == ""{
                   Toast(text: "Please enter confirm password!", delay: 0, duration: 1).show()
                   
               }
           }
           
       }
    
    
    
}
