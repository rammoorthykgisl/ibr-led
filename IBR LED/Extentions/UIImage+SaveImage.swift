//
//  UIImage+SaveImage.swift
//  IBR LED
//
//  Created by BSSADM on 05/11/19.
//  Copyright © 2019 KGISL. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

    func saveToDocuments(filename:String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsDirectory.appendingPathComponent(filename)
        if let data = self.jpegData(compressionQuality: 1.0) {
            do {
                try data.write(to: fileURL)
            } catch {
                print("error saving file to documents:", error)
            }
        }
    }

}
