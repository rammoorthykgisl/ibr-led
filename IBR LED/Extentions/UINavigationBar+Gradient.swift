//
//  UINavigationBar+Gradient.swift
//  boardVision
//
//  Created by Webykart Mac 2 on 27/02/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import Foundation
import UIKit
class GradientNavigationBar: UINavigationBar {
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit(){
        UINavigationBar.appearance().backgroundColor = UIColor.black
        barTintColor = UIColor.black
    }

}
