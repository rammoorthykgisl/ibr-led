//
//  Notification+Observers.swift
//  Board.Vision
//
//  Created by Webykart Mac PC 2 on 7/9/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import Foundation


extension NotificationCenter {
    func setObserver(_ observer: AnyObject, selector: Selector, name: NSNotification.Name, object: AnyObject?) {
        NotificationCenter.default.removeObserver(observer, name: name, object: object)
        NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
    }
}
