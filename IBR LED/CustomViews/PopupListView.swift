//
//  OtherDocumentsView.swift
//  Board.Vision
//
//  Created by Webykart Mac PC 2 on 5/20/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import UIKit

protocol PopUpListViewDelegate{
    func clickFromPopUpList(name:String,type:Int) -> Void

}


class PopUpListView: UIView {

    @IBOutlet  var listView: UITableView!
    
    var delegate:PopUpListViewDelegate!
    var suppliers = [Supplier]()
    var cities = [City]()
    var states = [State]()
    var searching  = false
    var type = 0
    var isJenama = false
    @IBOutlet weak var titleLabel: UILabel!
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        layoutIfNeeded()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        
    }
    
    func commonInit() {
        listView = UITableView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))

        listView.register(UINib(nibName: "popupListCell", bundle: nil), forCellReuseIdentifier: "popup_cell")
        suppliers.removeAll()
        cities.removeAll()
        states.removeAll()
       // documentTableView.register(UINib(nibName: "MeetingDocumentsHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "MeetingOtherDocumentsHeader")
     
    }

}
extension PopUpListView:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type == 0{
           return suppliers.count
        }else if type == 1{
             return states.count
        }else{
             return cities.count
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        listView.register(UINib(nibName: "popupListCell", bundle: nil), forCellReuseIdentifier: "popup_cell")

        if let roadCell =   (tableView.dequeueReusableCell(withIdentifier: "popup_cell") as? PopUpTableViewCell){
            if type == 0{
                //JENAMA
                if let index = suppliers.firstIndex(where: {$0.supplier == "JENAMA"}){
                 
                    suppliers =  rearrange(array: suppliers, fromIndex: index, toIndex: 0)
                    isJenama = true
                }
             // roadCell.name.text = "\(suppliers[indexPath.row].supplier!) (\(suppliers[indexPath.row].watt!))\n
//                var i = 0
//                for suplier in suppliers{
//                    i = i + 1
//                    if suplier.name == "JENAMA" && suplier.watt == "WATT" && suplier.brand == "JENAMA"{
//                        break
//                    }
//                }
               
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "\(suppliers[indexPath.row].supplier!) (\(suppliers[indexPath.row].watt!))\n \(suppliers[indexPath.row].brand!)")
                attributedString.setColorForText(textForAttribute: "\(suppliers[indexPath.row].supplier!) (\(suppliers[indexPath.row].watt!))", withColor: UIColor.gray)
                attributedString.setColorForText(textForAttribute: suppliers[indexPath.row].brand!, withColor: UIColor.black)
                roadCell.name.attributedText = attributedString
            }else if type == 1{
               roadCell.name.text = states[indexPath.row].name!
            }else{
              roadCell.name.text = cities[indexPath.row].name!
             
            }
            
            return roadCell
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
   
   
    
  
    @objc func btnClickAction(_ sender:UIButton) {
       
       
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if isJenama && indexPath.row == 0{
            return
        }
        
        
        let cell = tableView.cellForRow(at: indexPath) as! PopUpTableViewCell
        if type == 2{
         TBLConstants.TBL_Local_Data.set(cities[indexPath.row].id!, forKey: TBLConstants.Keys.CITY_ID)
             delegate.clickFromPopUpList(name: cell.name.text!, type:type)
        }else if type == 1{
          TBLConstants.TBL_Local_Data.set(states[indexPath.row].id!, forKey: TBLConstants.Keys.STATE_ID)
             delegate.clickFromPopUpList(name: cell.name.text!, type:type)
        }else{
           
                TBLConstants.TBL_Local_Data.set(suppliers[indexPath.row].name!, forKey: TBLConstants.Keys.SUPPLIER)
                delegate.clickFromPopUpList(name: suppliers[indexPath.row].name!, type:type)
            
        }
      
      
    }
    func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = array
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)

        return arr
    }
    
}

extension NSMutableAttributedString {

    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)

        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)

        // Swift 4.1 and below
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

}
