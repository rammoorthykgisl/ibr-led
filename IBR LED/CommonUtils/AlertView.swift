//
//  AlertView.swift
//  boardVision
//
//  Created by Webykart Mac 2 on 04/02/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import Foundation
import UIKit


class AlertErrorTypeMessageView: UIViewController {
    
    @IBOutlet weak var ErrorType_message: UILabel!
    @IBOutlet weak var PopUp_View: UIView!
    private var notification: NSObjectProtocol?
    
    var err_msg = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ErrorType_message.text = err_msg
        
        
    }
    
    
    deinit {
        // make sure to remove the observer when this view controller is dismissed/deallocated
        if let notification = notification {
            NotificationCenter.default.removeObserver(notification)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        if touch?.view == self.view {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func btnClickEvent(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
}

