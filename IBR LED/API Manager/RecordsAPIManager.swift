//
//  EventsAPIManager.swift
//  boardVision
//
//  Created by Webykart Mac 2 on 08/02/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import Foundation
import Foundation
import Alamofire


class RecordsAPIManager:BaseService {
    
    
    
    func getWorkRecords(completionHandler: @escaping (_ data: [Records]?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        
        let url = BaseService.API_URL + "V1/locations"
        // let url = "http://172.20.8.210:81/api/V1/locations"
        return BaseService.makeCallForArray(method: HTTPMethod.get, url: url, completionHandler: completionHandler)
    }
    
    func updatePoleDetails(reportID:NSNumber,poleImage:UIImage,lampImage:UIImage,name:String,road_name:String,city_id:Int,latitude:Double,longitude:Double,remarks:String,installed:Int,status:Int,new:Int,control_time_location:String,meter_no:String,account_no:String,phase:Int? = nil,supplier:String,completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) {
        // let url = "http://172.20.8.210:81/api/V1/locations/\(reportID)"
     
        let url = BaseService.API_URL + "V1/locations/\(reportID)"
        var photodata1:String?
        var photoData2:String?
        if let poleImageData = poleImage.jpegData(compressionQuality: 0.5)?.base64EncodedString() {
            photodata1 = poleImageData
        }
        if let lampImageData = lampImage.jpegData(compressionQuality: 0.5)?.base64EncodedString()  {
            photoData2 = lampImageData
        }
        
        BaseService.makeCallForObjectPut(method: HTTPMethod.put, url: url, parameter:["name":name,"road_name":road_name,"city_id":city_id,"latitude":latitude,"longitude":longitude,"remarks":remarks,"installed":installed,"status":status,"new":new,"control_time_location":control_time_location,"meter_no":meter_no,"account_no":account_no,"supplier":supplier,"photo1": "data:image/jpg;base64," + photodata1!,"photo2": "data:image/jpg;base64," + photoData2!], completionHandler: completionHandler)
    }
    
    func getSuppliers(completionHandler: @escaping (_ data: Suppliers?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        
        let url = BaseService.API_URL + "suppliers"
        return BaseService.makeCallWithoutKeyForObject(method: HTTPMethod.get, url: url, completionHandler: completionHandler)
    }
    
    func getCities(completionHandler: @escaping (_ data: Cities?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        
        let url = BaseService.API_URL + "cities"
        return BaseService.makeCallWithoutKeyForObject(method: HTTPMethod.get, url: url, completionHandler: completionHandler)
    }
    
    func getStates(completionHandler: @escaping (_ data: States?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        
        let url = BaseService.API_URL + "states"
        return BaseService.makeCallWithoutKeyForObject(method: HTTPMethod.get, url: url, completionHandler: completionHandler)
    }
    
    func createNewPole(poleImage:UIImage,lampImage:UIImage,name:String,road_name:String,city_id:Int,latitude:Double,longitude:Double,remarks:String,installed:Int,status:Int,new:Int,control_time_location:String,meter_no:String,account_no:String,phase:Int? = nil,supplier:String,completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) {
        
        let url = BaseService.API_URL + "V1/locations"
        var photodata1:String?
        var photoData2:String?
        if let poleImageData = poleImage.jpegData(compressionQuality: 0.5)?.base64EncodedString() {
            photodata1 = poleImageData
        }
        if let lampImageData = lampImage.jpegData(compressionQuality: 0.5)?.base64EncodedString()  {
            photoData2 = lampImageData
        }
        BaseService.makeCallForObjectPut( method: HTTPMethod.post, url: url, parameter: ["name":name,"road_name":road_name,"city_id":city_id,"latitude":latitude,"longitude":longitude,"remarks":remarks,"installed":installed,"status":status,"new":new,"control_time_location":control_time_location,"meter_no":meter_no,"account_no":account_no,"supplier":supplier,"photo1": "data:image/jpg;base64," + photodata1!,"photo2": "data:image/jpg;base64," + photoData2!], completionHandler: completionHandler)
    }
    
    
    func checkRecordExist(location_id:Int?,name:String,road_name:String,city_id:Int,control_time_location:String,state_id:Int,completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
       let url = BaseService.API_URL + "V2/locations/check-exist"
       //let url = "http://172.20.8.210:81/api/V2/locations/check-exist"
        var parameter:Parameters?
        if location_id != nil{
            parameter = ["location_id":location_id!,"name":name,"road_name":road_name,"city_id":city_id,"control_time_location":control_time_location,"state_id":state_id]
        }else{
            parameter = ["name":name,"road_name":road_name,"city_id":city_id,"state_id":state_id]
        }
        return BaseService.makeCallForPostObject(method: HTTPMethod.post, url: url, parameter:parameter, completionHandler: completionHandler)
    }
    
    func versionCheck(platForm:String,version:String,completionHandler: @escaping (_ data: Version?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
          let url = BaseService.API_URL + "version"
        
        return BaseService.makeCallForPostObject(method: HTTPMethod.post, url: url, parameter:["version":version,"platform":platForm], completionHandler: completionHandler)
       }
    
}
