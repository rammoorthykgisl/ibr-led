//
//  ApiResponseHandler.swift
//  Board.Vision
//
//  Created by Muthu Venkatesh on 03/09/18.
//  Copyright © 2018 Webykart Inc. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

struct ApiResponseHandler<T: Mappable> {

    static func handleObjectResponse(response: Alamofire.DataResponse<Any>, completion: (_ data: T?,_ errorResponse:DataResponse<Any>?) -> Void) {
        switch response.result {
        case .success(let value):
            guard let responseJSON = value as? [String: Any] else {
                return
            }
            if let data = Mapper<T>().map(JSONObject: responseJSON["results"]) {
                 print(responseJSON)
                completion(data, nil)
            }else if let data = Mapper<T>().map(JSONObject: responseJSON) {
                 print(responseJSON)
                completion(data, nil)
            }
            else {
                print("JSON PARSE FAILED for : \(value)")
                completion(nil,response)
            }
        case .failure(let error):
            
            if Reachability.isConnectedToNetwork() {

                completion(nil,response)
            } else {
                 TBLConstants.hideActivityIndicator()
                 TBLConstants.offlineAppAlert()
               }
            print("ErrorType: \(error.localizedDescription)")
        }
    }
   
   
    static func handleWithoutkeyObjectResponse(response: Alamofire.DataResponse<Any>, completion: (_ data: T?,_ errorResponse:DataResponse<Any>?) -> Void) {
        switch response.result {
        case .success(let value):
            guard let responseJSON = value as? [String: Any] else {
                return
            }
            if let data = Mapper<T>().map(JSONObject: responseJSON[""]) {
                 print(responseJSON)
                completion(data, nil)
            }else if let data = Mapper<T>().map(JSONObject: responseJSON) {
                 print(responseJSON)
                completion(data, nil)
            }
            else {
                print("JSON PARSE FAILED for : \(value)")
                completion(nil, response)
            }
        case .failure(let error):
            print(response.result.value as Any)
            if Reachability.isConnectedToNetwork() {
                completion(nil, response)
            } else {
                 TBLConstants.hideActivityIndicator()
                 TBLConstants.offlineAppAlert()
                
                
            }
            
            
            
            print("ErrorType: \(error.localizedDescription)")
        }
    }
    static func handleArrayResponse(response: Alamofire.DataResponse<Any>, responseKey: String?, completion: (_ data: [T]?,_ errorResponse:DataResponse<Any>?) -> Void) {
        switch response.result {
        case .success(let value):
//            guard let responseJSON = value as? [String: Any] else {
//                return
//            }
            if let data = Mapper<T>().mapArray(JSONObject: response.result.value) {
                print(data)
                completion(data, nil)
            } else {
                print("JSON PARSE FAILED for : \(value)")
                completion(nil, response)
            }
            break
        case .failure(let error):
            
            if Reachability.isConnectedToNetwork() {
                completion(nil,response)
            } else {
                TBLConstants.hideActivityIndicator()
                TBLConstants.offlineAppAlert()
              
            }
            print("ErrorType: \(error.localizedDescription)")
        }
    }
    
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
        return topViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
        if let selected = tabController.selectedViewController {
            return topViewController(controller: selected)
        }
    }
    if let presented = controller?.presentedViewController {
        return topViewController(controller: presented)
    }
    return controller
} }

public extension UIAlertController {
                  func show() {
                      let win = UIWindow(frame: UIScreen.main.bounds)
                      let vc = UIViewController()
                      vc.view.backgroundColor = .clear
                      win.rootViewController = vc
                      win.windowLevel = UIWindow.Level.alert + 1  // Swift 3-4: UIWindowLevelAlert + 1
                      win.makeKeyAndVisible()
                    win.rootViewController?.present(self, animated: true, completion: nil)
                  }
              }
              
  
