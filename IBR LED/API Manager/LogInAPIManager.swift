//
//  APIManager.swift
//  boardVision
//
//  Created by Webykart Mac 2 on 24/01/19.
//  Copyright © 2019 trustedservices. All rights reserved.
//

import Foundation
import Alamofire


class LogInAPIManager:BaseService {
    
    func LoginUserWithToken(email:String,password:String,completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        let url = BaseService.API_URL + "auth"
        return BaseService.makeCallForObject(method: HTTPMethod.post, url: url, parameter:["email":email,"password" : password], completionHandler: completionHandler)
    }
    
    
    func getUserID(completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        
        let url = BaseService.API_URL + "user"
        return BaseService.makeCallWithoutKeyForObject(method: HTTPMethod.get, url: url, completionHandler: completionHandler)
    }
    
    func getTokenFromEmail(email:String,completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        let url = BaseService.API_URL + "password/email"
        return BaseService.makeCallForObject(method: HTTPMethod.post, url: url, parameter:["email":email], completionHandler: completionHandler)
    }
    func passwordChange(currentPassword:String,confirmPassword:String,completionHandler: @escaping (_ data: Users?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        let url = BaseService.API_URL + "password/change"
        return BaseService.makeCallForPostObject(method: HTTPMethod.post, url: url, parameter: ["current_password":currentPassword, "new_password":confirmPassword],completionHandler: completionHandler)
    }
}
