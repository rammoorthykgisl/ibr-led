//
//  Base Service.swift
//  Board.Vision
//
//  Created by Muthu Venkatesh on 03/09/18.
//  Copyright © 2018 Webykart Inc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON

class BaseService {
    
   
    static var API_URL = "https://ljkled.vertikaliti.com/api/"
    //"http://103.118.172.116/api/"
    //"http://172.20.8.210:82/api/"
    //"http://172.20.8.78/api"
    //"http://172.20.8.210:81/api/"
    //"http://172.20.8.210:81/api/"
    //"https://ljkled.vertikaliti.com/api/"
      
   
    static var sessionManager: Alamofire.SessionManager = {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/json"
        defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
        defaultHeaders["Accept"] =  "application/json"
        
        let configuration = URLSessionConfiguration.default
        configuration.urlCache = nil
        configuration.httpAdditionalHeaders = defaultHeaders
        
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
        
    }()
    
    //Handles Void Responses
    static func makeCall(method: HTTPMethod ,url: String,parameter: [String: Any]? = nil, completionHandler: @escaping (_ success: Bool,_ response:DataResponse<Data> ) -> Void) -> Alamofire.Request {
        
        let requst = try? self.sessionManager.request(url.asURL(), method: method, parameters: parameter, encoding: JSONEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseData { (response) in
                switch response.result {
                case .success( _):
                    print(JSON(response.result.value!))
                    completionHandler(true,response)
                    break
                case .failure(let error):
                    if Reachability.isConnectedToNetwork() {
                        completionHandler(false,response)
                    } else {
                        completionHandler(false, response)
                    }
                    print("ErrorType: \(error.localizedDescription)")
                }
        }
        return requst!
    }
    
    //Handles Object Responses
    static func makeCallForObject<T: Mappable>(method: HTTPMethod ,url: String,parameter: Parameters? = nil, completionHandler: @escaping (_ data: T?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/json"
        defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
        defaultHeaders["Accept"] =  "application/json"
        let request = try? self.sessionManager.request(url.asURL(), method: method, parameters: parameter, encoding: JSONEncoding.default, headers: defaultHeaders)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                ApiResponseHandler<T>.handleObjectResponse(response: response, completion: completionHandler)
        }
        return request!
    }
    //Handles Object Responses
    static func makeCallForObjectPut<T: Mappable>(method: HTTPMethod ,url: String,parameter: Parameters? = nil, completionHandler: @escaping (_ data: T?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        let headers = ["Accept" :"application/json" ,
                       "Authorization" : "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")",
                        "Content-Type": "application/x-www-form-urlencoded" ]

        let request = try? self.sessionManager.request(url.asURL(), method: method, parameters: parameter, encoding: URLEncoding.default, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                ApiResponseHandler<T>.handleObjectResponse(response: response, completion: completionHandler)
        }
        return request!
    }
    //Handles Object Responses
    static func makeCallForPostObject<T: Mappable>(method: HTTPMethod ,url: String,parameter: Parameters? = nil, completionHandler: @escaping (_ data: T?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/x-www-form-urlencoded"
        defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
        defaultHeaders["Accept"] =  "application/json"
        let request = try? self.sessionManager.request(url.asURL(), method: method, parameters: parameter, encoding:URLEncoding.default, headers: defaultHeaders)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                ApiResponseHandler<T>.handleObjectResponse(response: response, completion: completionHandler)
        }
        return request!
    }
    
    static func makeCallForUpload(poleImage:UIImage,lampImage:UIImage,method: HTTPMethod ,url: String,parameter: [String: Any]? = nil, completionHandler: @escaping (_ success: Bool) -> Void) {
       
        
        
               Alamofire.upload(multipartFormData: {
                    multipartFormData in
        
                for (key, value) in parameter! {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                }, to:url,
                    method:method,
                    headers:[
                       
                        "Content-Type" : "application/x-www-form-urlencoded",
                        "Authorization" : "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")", 
                        "Accept" : "application/json",
                ],
                    encodingCompletion: {
                        encodingResult in
        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON {
                                response in
                                print(response.request ?? "")   // original URL request
                                print(response.response ?? "")  // URL response
                                print(response.data ?? "")      // server data
                                print(response.result)          // result of response serialization
        
                                switch response.result {
                                case .success( _):
                                    print(JSON(response.result.value!))
                                    completionHandler(true)
                                    break
                                case .failure(let error):
                                    if Reachability.isConnectedToNetwork() {
                                        completionHandler(false)
                                    } else {
                                        completionHandler(false)
                                    }
                                    print("ErrorType: \(error.localizedDescription)")
                                }
        
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                })
        
        
      
    }
     //Handles Object Responses Without Key
    static func makeCallWithoutKeyForObject<T: Mappable>(method: HTTPMethod ,url: String,parameter: Parameters?  = nil, completionHandler: @escaping (_ data: T?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/json"
        defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
        defaultHeaders["Accept"] =  "application/json"
        let request = try? self.sessionManager.request(url.asURL(), method: method, parameters: parameter, encoding: JSONEncoding.default, headers: defaultHeaders)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                ApiResponseHandler<T>.handleWithoutkeyObjectResponse(response: response, completion: completionHandler)
        }
        return request!
    }
    
    //Handles Array Responses
    static func makeCallForArray<T: Mappable>(method: HTTPMethod,url: String,parameter: [String: Any]? = nil, responseKey: String? = nil, completionHandler: @escaping (_ data: [T]?,_ errorResponse:DataResponse<Any>?) -> Void) -> Alamofire.Request {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["Content-Type"] = "application/json"
        defaultHeaders["Authorization"] = "Bearer \(TBLConstants.TBL_Local_Data.value(forKey: TBLConstants.Keys.ACCESS_TOKEN) as?  String ?? "")"
        defaultHeaders["Accept"] =  "application/json"
        let request = try? self.sessionManager.request(url.asURL(), method: method, parameters: parameter, encoding: JSONEncoding.default, headers: defaultHeaders )
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                ApiResponseHandler<T>.handleArrayResponse(response: response, responseKey: responseKey, completion: completionHandler)
        }
        return request!
    }
   
    
}
